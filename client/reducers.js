import {combineReducers} from 'redux';

import app from './components/App/reducers';
import nav from './components/Nav/HeaderNav/reducers';
import main from './components/Sections/Main/reducers';
import club from './components/Sections/Club/reducers';
import calendar from './components/Sections/Calendar/reducers';
import event from './components/Sections/Calendar/Event/reducers';
import teams from './components/Sections/Projects/Teams/reducers';
import team from './components/Sections/Projects/Team/reducers';
import regatta from './components/Sections/Projects/Regatta/reducers';
import news from './components/Sections/Media/News/reducers';
import newsArticle from './components/Sections/Media/NewsArticle/reducers';
import gallery from './components/Sections/Media/Gallery/reducers';
import pressCentre from './components/Sections/Media/PressCentre/reducers';
import customPage from './components/Sections/CustomPage/reducers';

export default combineReducers({
  entryPage: (page = '') => page,
  app,
  nav,
  main,
  club,
  calendar,
  event,
  projects: combineReducers({
    teams,
    team,
    regatta
  }),
  media: combineReducers({
    news,
    gallery,
    newsArticle,
    pressCentre
  }),
  customPage
});
