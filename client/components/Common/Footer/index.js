import React from 'react';
import {Link} from 'react-router-dom';

import Social from '../../UIComponents/Social';
import FooterMap from './FooterMap';

import './footer.less';

const Footer = () => (
  <footer className='footer'>
    <div className='footer__left_col'>
      <div className='footer__left_col_content'>
        <div className='footer__logo_wrapper'>
          <Link to='/'>
            <div className='footer__logo' />
          </Link>
        </div>
        <div className='footer__address'>
          <p className='text'>Адрес:</p>
          <p className='text'>
            197227 Санкт-Петербург
            <br />
            Приморский район, пос. Лахта
            <br />
            ул. Береговая, дом 19, лит. А
            <br />
            Яхтенный порт &#34;Геркулес&#34;
          </p>
          <p className='text'>Контактная информация:</p>
          <p className='text'>
            Администратор яхт-клуба: +7 (812) 999 9999
            <br />
            Капитания: +7 (812) 999 9999
            <br />
            e-mail: info@yc.ru
          </p>
        </div>
        <div className='social__wrapper'>
          <Social />
        </div>
      </div>
    </div>
    <div className='footer__right_col'>{/* <FooterMap /> */}</div>
  </footer>
);

export default Footer;
