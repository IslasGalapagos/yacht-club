import React from 'react';
import {Link} from 'react-router-dom'; // Link,

import HeaderNav from '../../Nav/HeaderNav';
// import HeaderSearch from './HeaderSearch';
import Social from '../../UIComponents/Social';

import './header.less';

if (typeof window !== 'undefined') {
  window.onscroll = () => {
    const $header = document.querySelector('.js-header');
    $header.classList.add('-compact');

    window.onscroll = null;
  };
}

const Header = () => {
  return (
    <header className="header js-header">
      <div className="header__full_block">
        <Link to={'/'}>
          <div className="header__big_logo" />
        </Link>
        <Social />
      </div>
      <div className="header__logo_wrapper">
        <Link to={'/'}>
          <div className="header__logo" />
        </Link>
      </div>
      <HeaderNav />

      {/* <ul className="header__localization">
        <li className="header__localization_item">
          <Link to={'/'} activeClassName="-active">
            рус
          </Link>
        </li>
        <li className="header__localization_item">
          <Link to={'/eng'} activeClassName="-active">
            eng
          </Link>
        </li>
      </ul>
      <HeaderSearch /> */}
    </header>
  );
};

export default Header;
