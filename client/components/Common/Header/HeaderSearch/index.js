import React from 'react';

import './header_search.less';

const HeaderSearch = () => (
    <div className="header_search">
        <form action="" className="header_search__form">
            <button type="submit" className="header_search__submit" />
            <input type="text" className="header_search__input" placeholder="Поиск" />
        </form>
    </div>
);

export default HeaderSearch;
