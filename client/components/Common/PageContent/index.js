import React from 'react';

import './page_content.less';

const PageContent = props => {
  if (typeof window !== 'undefined') {
    window.scrollTo(0, 0);
  }

  return (
    <section className={`page_content ${props.className || ''}`}>
      {props.children}
    </section>
  );
};

export default PageContent;
