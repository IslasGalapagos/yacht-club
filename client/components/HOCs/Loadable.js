import Loadable from 'react-loadable';

import Loading from '../UIComponents/Loading';

const MyLoadable = (options = {}) =>
  Loadable(
    Object.assign(
      {
        loading: Loading,
        delay: 500,
        timeout: 10000
      },
      options
    )
  );

export default MyLoadable;
