import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {StaticRouter, BrowserRouter} from 'react-router-dom';

import App from './App';

class Root extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {staticRouterData = {}, store} = this.props;

    const Router = Object.keys(staticRouterData).length
      ? StaticRouter
      : BrowserRouter;

    return (
      <Provider store={store}>
        <Router {...staticRouterData}>
          <App />
        </Router>
      </Provider>
    );
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  staticRouterData: PropTypes.shape({
    location: PropTypes.string,
    constext: PropTypes.object
  })
};

export default Root;
