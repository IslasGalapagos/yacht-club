import React from 'react';

import './symbols_pennants_list.less';

const SymbolsPennantsList = props => {
  const images = props.itemsData.images;
  const names = props.itemsData.names;
  const descs = props.itemsData.descs;

  const $images = images.map(image => (
    <li key={image} className="club_symbols__pennant_images_item">
      {image ? <img src={image} alt="" /> : null}
    </li>
  ));
  const $names = names.map(name => (
    <li key={name} className="club_symbols__pennant_name_item">
      <p className="title">{name}</p>
    </li>
  ));
  const $descs = descs.map(desc => (
    <li key={desc.substr(0, 7)} className="club_symbols__pennant_desc_item">
      <p className="text">{desc}</p>
    </li>
  ));

  return (
    <div className="club_symbols__pennant">
      <ul className="club_symbols__pennant_images">{$images}</ul>
      <ul className="club_symbols__pennant_name">{$names}</ul>
      <ul className="club_symbols__pennant_desc">{$descs}</ul>
    </div>
  );
};

SymbolsPennantsList.propTypes = {
  itemsData: PropTypes.object.isRequired
};

export default SymbolsPennantsList;
