import React from 'react';

import ContentHeader from '../../../UIComponents/ContentHeader';
import PartnersList from '../PartnersList';

import './partners.less';

const Partners = props => {
  const {type1 = [], type2 = [], type3 = []} = props.data;

  return (
    <div>
      <ContentHeader>
        <p className="text">Друзья и партнёры клуба</p>
      </ContentHeader>
      <div className="club_partners">
        {type1.length ? (
          <div>
            <h1 className="title -section_title">Друзья клуба</h1>
            <PartnersList partners={type1} />
          </div>
        ) : null}
        {type2.length ? (
          <div>
            <h1 className="title -section_title">Партнёры клуба</h1>
            <PartnersList partners={type2} />
          </div>
        ) : null}
        {type3.length ? (
          <div>
            <h1 className="title -section_title">
              Информационные партнёры клуба
            </h1>
            <PartnersList partners={type3} />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default Partners;
