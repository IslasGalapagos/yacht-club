import {GET_CLUB_DATA_SUCCESS} from './actions';

const initialState = {
  data: null
};

export default function clubMain(state = initialState, action) {
  switch (action.type) {
    case GET_CLUB_DATA_SUCCESS:
      return {...state, data: action.payload};

    default:
      return state;
  }
}
