import React from 'react';

import ContentHeader from '../../../UIComponents/ContentHeader';
import MembersList from '../MembersList';

import './members.less';

const Members = props => {
  const {type1 = [], type2 = [], type3 = []} = props.data;

  return (
    <div>
      <ContentHeader>
        <p className="text">Члены клуба</p>
      </ContentHeader>
      <div className="club_members">
        {type1.length ? (
          <div>
            <h1 className="title -section_title">Почётные члены клуба</h1>
            <MembersList
              className="club_members__honored"
              type="with_avatar"
              itemsData={type1}
            />
          </div>
        ) : null}
        {type2.length ? (
          <div>
            <h1 className="title -section_title">Действующие члены клуба</h1>
            <MembersList
              className="club_members__active"
              type="without_avatar"
              itemsData={type2}
            />
          </div>
        ) : null}
        {type3.length ? (
          <div>
            <h1 className="title -section_title">Бывшие члены клуба</h1>
            <MembersList type="without_avatar" itemsData={type3} />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default Members;
