import React from 'react';

import ContentHeader from '../../../UIComponents/ContentHeader';
import LeadershipList from '../LeadershipList';

import './leadership.less';

const Leadership = props => {
  const {type1 = [], type2 = [], type3 = []} = props.data;

  return (
    <div>
      <ContentHeader>
        <p className="text">Руководство</p>
      </ContentHeader>
      <div className="club_leadership">
        {type1.length ? (
          <LeadershipList
            className="club_leadership__commanders"
            direction="vertical"
            itemsData={type1}
          />
        ) : null}
        {type2.length ? (
          <div>
            <h1 className="title -section_title">Учредители</h1>
            <LeadershipList
              className="club_leadership__founders"
              direction="horizontal"
              itemsData={type2}
            />
          </div>
        ) : null}
        {type3.length ? (
          <div>
            <h1 className="title -section_title">Руководство</h1>
            <LeadershipList
              className="club_leadership__leaders"
              direction="horizontal"
              itemsData={type3}
            />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default Leadership;
