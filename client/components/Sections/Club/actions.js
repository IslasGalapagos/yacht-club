import axiosF from '../../../utils/axios';

export const GET_CLUB_DATA = 'GET_CLUB_DATA';
export const GET_CLUB_DATA_SUCCESS = 'GET_CLUB_DATA_SUCCESS';
export const GET_CLUB_DATA_ERROR = 'GET_CLUB_DATA_ERROR';

export const getClubDataAction = () => dispatch =>
  axiosF('/api/club')
    .then(response => {
      dispatch({
        type: GET_CLUB_DATA_SUCCESS,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
