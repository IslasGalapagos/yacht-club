import React from 'react';

import './members_list.less';

const MembersList = props => {
  const type = props.type;
  const itemsData = props.itemsData;

  const $items = itemsData.map((item, index) => {
    if (type === 'with_avatar') {
      return (
        <li key={item.name} className="members_list__item">
          <div className="img_wrapper">
            {item.avatar ? <img src={item.avatar.url} alt="" /> : null}
          </div>
          <div className="text__wrapper">
            <p className="title">{item.name}</p>
            <p className="text">{item.desc}</p>
          </div>
        </li>
      );
    }

    return (
      <li key={item.name} className="members_list__item">
        <span className="title">{item.name}, </span>
        <span className="text">{item.desc}</span>
      </li>
    );
  });

  return (
    <ul className={`members_list -${type} ${props.className}`}>{$items}</ul>
  );
};

MembersList.propTypes = {
  type: PropTypes.string.isRequired,
  itemsData: PropTypes.array.isRequired
};

export default MembersList;
