import React, {PureComponent, Fragment} from 'react';

import Slideshow from '../../../UIComponents/Slideshow';
import ThumbsNav from '../../../Nav/ThumbsNav';

class About extends PureComponent {
  render() {
    const {slideshow = []} = this.props;

    return (
      <Fragment>
        {slideshow.length ? <Slideshow data={slideshow} /> : null}

        <div className="club">
          <div className="club__content">
            <div className="club__desc_wrapper">
              <h1 className="title -section_title">О клубе</h1>
              <div className="club__desc_text">
                <p className="text -sb">
                  Санкт-Петербургская общественная спортивная организация
                  Яхт-клуб Санкт-Петербурга создана в 2007 году и является
                  преемником старейшего яхт-клуба мира – Невского флота,
                  задуманного Петром Великим в начале XVIII века.
                </p>
                <p className="text">
                  Преумножая славные традиции российского флота и активно
                  участвуя в международной парусной жизни, Яхт-клуб
                  Санкт-Петербурга сегодня является одним из крупнейших в мире.
                </p>
                <p className="text">
                  Деятельность Клуба охватывает целый ряд магистральных
                  направлений, среди которых судостроение, исторические
                  исследования и образовательные программы, спортивная
                  подготовка и морская практика для молодежи, олимпийский спорт,
                  приобщение взрослых к парусу и другие масштабные социальные
                  проекты.
                </p>
              </div>
              <h1 className="title -section_title">
                Миссия Яхт-клуба Санкт-Петербурга
              </h1>
              <div className="club__desc_text">
                <p className="text">
                  Хранение идеалов морской этики на суше и воде, развитие
                  морских видов спорта, в первую очередь, парусного.
                </p>
              </div>
              <h1 className="title -section_title">
                Философия Яхт-клуба Санкт-Петербурга
              </h1>
              <div className="club__desc_text">
                <p className="text">
                  Философия яхт-клуба основывается на необходимости сохранить и
                  на обязанности передать будущим поколениям наследие морской
                  культуры и морской истории России и всего мира.
                </p>
              </div>
              <h1 className="title -section_title">
                Приветственное слово Командора
              </h1>
              <div className="club__welcome">
                <div className="img_wrapper">
                  <img
                    src="http://res.cloudinary.com/islas-online/image/upload/v1482604003/static_pics/lyubomirov.jpg"
                    alt=""
                  />
                </div>
                <div className="club__welcome_text_block">
                  <p className="text">
                    С момента своего создания деятельность Яхт-клуба
                    Санкт-Петербурга направлена на укрепление имиджа города как
                    морской столицы России. На пути к достижению этой высокой
                    цели Яхт-клуб проводит работу сразу в нескольких
                    направлениях. Это судостроение, исторические исследования и
                    образовательные программы, спортивная подготовка и морская
                    практика для молодежи, олимпийский спорт, приобщение к
                    парусу взрослых и другие масштабные социальные проекты.
                    Заметное место среди задач Клуба занимает продвижение
                    Санкт-Петербурга как уникального места для проведения
                    парусных соревнований наивысшего уровня.
                  </p>
                  <p className="text">
                    На сегодняшний момент во всех перечисленных направлениях уже
                    удалось добиться замечательных результатов — и они
                    вдохновляют нас для дальнейшей работы.
                  </p>
                  <p className="text -sb">Любомиров В. А.</p>
                </div>
              </div>
            </div>
            <div className="club__logo">
              <div className="image" />
            </div>
          </div>
          <ThumbsNav
            title="Подразделы клуба"
            links={[{title: 'Руководство'}]}
          />
        </div>
      </Fragment>
    );
  }
}

export default About;
