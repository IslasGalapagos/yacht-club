import React, {Component} from 'react';
import {connect} from 'react-redux';

import Router from '../../Router';
import PageContent from '../../Common/PageContent';
import About from './About';
import Loading from '../../UIComponents/Loading';

import {getClubDataAction} from './actions';

import './club.less';

class Club extends Component {
  componentDidMount() {
    if (this.props.data === null) {
      this.props.getClubData();
    }
  }

  render() {
    const {data} = this.props;

    if (data === null) {
      return <Loading />;
    }

    const {
      location: {pathname},
      subRoutes
    } = this.props;

    const isRoot = pathname === '/club';

    const Routes = subRoutes.map(route =>
      Router.createRoute(route, {
        data: data[route.path.replace('/club/', '')] || {}
      })
    );

    return (
      <PageContent>
        {isRoot && <About slideshow={data.slideshow} />}
        {Routes}
      </PageContent>
    );
  }
}

Club.propTypes = {
  data: PropTypes.object,
  getClubData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  data: store.club.data
});

const mapDispatchToProps = dispatch => ({
  getClubData: () => dispatch(getClubDataAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Club);
