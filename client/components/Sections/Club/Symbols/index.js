import React from 'react';

import ContentHeader from '../../../UIComponents/ContentHeader';
import SymbolsPennantsList from '../SymbolsPennantsList';

import './symbols.less';

const Symbols = (props) => {
    const { mainLogo, pennants } = props.data;

    return (
        <div>
            <ContentHeader>
                <p className="text">Символика клуба</p>
            </ContentHeader>
            <div className="club_symbols">
                { mainLogo ? (
                    <div className="club_symbols__emblem">
                        <div className="club_symbols__emblem_image">
                            <img src={mainLogo.image.url} alt="" />
                        </div>
                        <div className="club_symbols__emblem_text_block">
                            <h1 className="title">{mainLogo.mainTitle}</h1>
                            <p className="text">{mainLogo.desc}</p>
                        </div>
                    </div>
                ) : null }
                { pennants ? (
                    <div>
                        <h1 className="title -section_title">Флаги и вымпелы</h1>
                        <SymbolsPennantsList itemsData={pennants} />
                    </div>
                ) : null }
            </div>
        </div>
    );
};

export default Symbols;
