import React from 'react';

import './club_partners_list.less';

const PartnersList = props => {
  const partners = props.partners;
  const partnersEl = partners.map(partner => (
    <li key={partner.name} className="club_partners__item">
      <div className="club_partners__logo_wrapper">
        <div className="club_partners__logo_inner">
          {partner.image ? <img src={partner.image.url} alt="" /> : null}
        </div>
      </div>
      <div className="club_partners__text_block">
        <div className="p title">{partner.name}</div>
        <p className="text">{partner.desc}</p>
      </div>
    </li>
  ));
  return <ul className="club_partners__list">{partnersEl}</ul>;
};

export default PartnersList;
