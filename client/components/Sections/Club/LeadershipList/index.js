import React from 'react';

import './leadership_list.less';

const LeadershipList = props => {
  const direction = props.direction;

  const $items = props.itemsData.map(item => {
    if (direction === 'horizontal') {
      return (
        <li key={item.name} className="leadership_list__item">
          <div className="img_wrapper">
            {item.avatar ? <img src={item.avatar.url} alt="" /> : null}
          </div>
          <p className="title">{item.name}</p>
          <p className="text">{item.position}</p>
        </li>
      );
    }

    return (
      <li key={item.name} className="leadership_list__item">
        <div className="img_wrapper">
          <div className="img_wrapper">
            {item.avatar ? <img src={item.avatar.url} alt="" /> : null}
          </div>
        </div>
        <div className="text__wrapper">
          <p className="title">{`${item.position} – ${item.name}`}</p>
          <p className="text">{item.desc}</p>
        </div>
      </li>
    );
  });

  return (
    <ul className={`leadership_list -${direction} ${props.className}`}>
      {$items}
    </ul>
  );
};

LeadershipList.propTypes = {
  direction: PropTypes.string.isRequired,
  itemsData: PropTypes.array.isRequired
};

export default LeadershipList;
