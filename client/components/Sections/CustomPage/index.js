import React, {Component} from 'react';
import {connect} from 'react-redux';

import ContentHeader from '../../UIComponents/ContentHeader';
import Slideshow from '../../UIComponents/Slideshow';
import CustomPagePeople from './CustomPagePeople';
import CustomPageEvents from './CustomPageEvents';
import NewsList from '../../Sections/Media/NewsList';
import GalleryBlock from '../Media/GalleryBlock';

import {getCustomPageDataAction} from './actions';

import './custom_page.less';

export class CustomPage extends Component {
  static loadData(store, route) {
    const slug = store
      .getState()
      .entryPage.replace(route.replace(':slug', ''), '');

    return store.dispatch(getCustomPageDataAction(slug));
  }

  componentDidMount() {
    const {pageData} = this.props;
    if (pageData === 'undefined' || !Object.keys(pageData).length) {
      this.props.getCustomPageData(this.props.params.slug);
    }
  }

  UNSAFE_componentWillReceiveProps(next) {
    if (this.props.params.slug !== next.params.slug) {
      this.props.getCustomPageData(next.params.slug);
    }
  }

  render() {
    const {
      name,
      content,
      logo,
      slideshow,
      gallery,
      people,
      events,
      news
    } = this.props.pageData;

    if (news.length) news.sort((a, b) => a.date.string < b.date.string);

    return (
      <div className="custom_page">
        {slideshow.length ? <Slideshow data={slideshow} /> : null}
        <ContentHeader>
          <p className="text">{name}</p>
        </ContentHeader>
        <div className="custom_page__content">
          <div className="custom_page__left_col">
            <div className="custom_page__desc text">
              {content.brief.indexOf('<script>') ? (
                <div
                  className="brief"
                  dangerouslySetInnerHTML={{__html: content.brief}}
                />
              ) : null}
              {content.extended.indexOf('<script>') ? (
                <div
                  className="extended"
                  dangerouslySetInnerHTML={{__html: content.extended}}
                />
              ) : null}
            </div>
            <CustomPagePeople peopleData={people} />
          </div>
          <div className="custom_page__right_col">
            {logo ? (
              <img src={logo.url} alt="" className="custom_page__logo" />
            ) : null}
            <h2 className="text -sb events_title">События команды</h2>
            <CustomPageEvents eventsData={events} />
          </div>
        </div>
        {news.length ? <NewsList newsData={[news]} /> : null}
        {gallery.photos.length || gallery.video.length ? (
          <GalleryBlock data={gallery} withLink />
        ) : null}
      </div>
    );
  }
}

CustomPage.propTypes = {
  pageData: PropTypes.object.isRequired,
  getCustomPageData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  pageData: store.customPage.pageData
});

const mapDispatchToProps = dispatch => ({
  getCustomPageData: slug => dispatch(getCustomPageDataAction(slug))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomPage);
