import axiosF from '../../../utils/axios';

export const GET_CUSTOM_PAGE_DATA = 'GET_CUSTOM_PAGE_DATA';

export const getCustomPageDataAction = slug => dispatch =>
  axiosF('/api/custom-page', {
    data: {customPageName: slug}
  })
    .then(response => {
      dispatch({
        type: GET_CUSTOM_PAGE_DATA,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
