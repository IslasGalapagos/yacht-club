import React from 'react';

import './custom_page_people.less';

const CustomPagePeople = props => {
  const $items = props.peopleData.map(item => (
    <li key={item.name} className="custom_page_people__item">
      {item.avatar.url ? (
        <div className="img_wrapper">
          <img src={item.avatar.url} alt="" />
        </div>
      ) : null}
      <h1 className="title name">{item.name}</h1>
      <p className="text -sb role">{item.role}</p>
      <p className="text">{item.desc}</p>
    </li>
  ));

  return <ul className="custom_page_people">{$items}</ul>;
};

export default CustomPagePeople;
