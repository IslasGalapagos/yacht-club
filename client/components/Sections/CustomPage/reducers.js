import {GET_CUSTOM_PAGE_DATA} from './actions.js';

const initState = {
  pageData: {}
};

export default function customPage(state = initState, action) {
  switch (action.type) {
    case GET_CUSTOM_PAGE_DATA:
      return {
        ...state,
        pageData: action.payload
      };

    default:
      return state;
  }
}
