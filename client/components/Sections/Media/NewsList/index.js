import React, {Fragment} from 'react';

import NewsMonth from '../NewsMonth';

const NewsList = props => {
  const newsList = props.newsData.map((month, index) => (
    <NewsMonth key={index} monthData={month} monthTitle={props.monthTitle} />
  ));

  return <Fragment>{newsList}</Fragment>;
};

export default NewsList;
