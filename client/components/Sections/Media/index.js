import React from 'react';

import Router from '../../Router';
import PageContent from '../../Common/PageContent';
import Main from './Main';

import './media.less';

const Media = props => {
  const {
    location: {pathname},
    subRoutes
  } = props;
  const isRoot = pathname === '/media';

  const Routes = subRoutes.map(route => Router.createRoute(route));

  return (
    <PageContent>
      {isRoot && <Main />}
      {Routes}
    </PageContent>
  );
};

export default Media;
