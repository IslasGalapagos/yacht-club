import React from 'react';
import axiosF from '../../../../utils/axios';
// import Recaptcha from 'react-gcaptcha';

import Popup from '../../../UIComponents/Popup';

import './accr_popup.less';

const AccreditationPopup = props => {
  const eventDefault = props.eventDefault;
  const events = props.events.map(item => (
    <option key={item.slug} value={item.slug}>
      {item.title}
    </option>
  ));

  // const verifyRecaptcha = () => {
  //   props.submitSwitcher(false);
  // };

  // const expiredRecaptcha = () => {
  //   props.submitSwitcher(true);
  // };

  const validate = cb => {
    return e => {
      e.preventDefault();

      const formData = new FormData(e.target);

      axiosF('/api/accreditation', {
        data: formData
      })
        .then(response => {
          if (response.code === 0) {
            closeCb();

            if (typeof cb === 'function') cb();
          } else if (response.code === 1) {
            console.log('Err');
          }
        })
        .catch(er => {
          console.log(er);
        });
    };
  };

  return (
    <Popup
      className='accr_popup'
      showed={props.showed}
      closeLink='/media/press-centre'>
      <h2 className='title'>Аккредитация на мероприятие</h2>
      <form className='form' onSubmit={validate(props.onAccreditationSubmit)}>
        <select
          defaultValue={eventDefault}
          className='form__select'
          name='event'
          required>
          {events}
        </select>
        <input
          className='form__input'
          type='text'
          name='name'
          placeholder='Введите имя'
          required
        />
        <input
          className='form__input'
          type='text'
          name='country'
          placeholder='Введите название страны'
          required
        />
        <input
          className='form__input'
          type='email'
          name='email'
          placeholder='Введите Ваш электронный адрес'
          required
        />
        <input
          className='form__input'
          type='text'
          name='phone'
          placeholder='Введите номер вашего телефона'
        />
        <textarea
          className='form__comment'
          name='comment'
          placeholder='Комментарии'
        />
        {/* <Recaptcha
          classname="form__captcha"
          sitekey="6LcG-g4UAAAAAEhy5Zj_VtuAvmzhiI9sgaf0YPUa"
          verifyCallback={verifyRecaptcha}
          expiredCallback={expiredRecaptcha}
        /> */}
        <button
          className={`form__button ${props.submitDisable ? '-disabled' : ''}`}
          type='submit'
          disabled={props.submitDisable}>
          Отправить запрос
        </button>
      </form>
      <div className='accr_popup__logo' />
    </Popup>
  );
};

export default AccreditationPopup;
