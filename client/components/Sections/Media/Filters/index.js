import React from 'react';

import ContentHeader from '../../../UIComponents/ContentHeader';
import Years from '../../../UIComponents/Filters/Years';
import Months from '../../../UIComponents/Filters/Months';
import FilterList from '../../../UIComponents/Filters/FilterList';

import './filters.less';

const Filters = props => {
  const activeYear = props.activeYear;
  const activeMonth = props.activeMonth;
  const activeFilter = props.activeFilter;
  const {
    years = [],
    months = [],
    classes = [],
    teams = []
  } = props.enabledFilters;
  return (
    <div>
      <ContentHeader className='calendar__filters_header'>
        <p className='text'>Новости клуба</p>
        <Years
          years={years}
          activeYear={activeYear}
          onYearChange={props.onYearChange}
        />
      </ContentHeader>
      <div className='calendar_filters'>
        <Months
          activeMonth={activeMonth}
          enableMonths={months}
          onMonthChange={props.onMonthChange}
        />
        {classes.length ? (
          <FilterList
            name='класс'
            inputName='classes'
            data={classes}
            activeFilter={activeFilter}
            onFilterChange={props.onFilterChange}
          />
        ) : null}
        {teams.length ? (
          <FilterList
            name='команда'
            inputName='teams'
            data={teams}
            activeFilter={activeFilter}
            onFilterChange={props.onFilterChange}
          />
        ) : null}
      </div>
    </div>
  );
};

export default Filters;
