import React from 'react';

import GalleryBlock from '../GalleryBlock';

const Galleries = props => {
  const galleryData = props.data.map(item => ({
    photos: item.photos,
    video: item.video
  }));

  const galleries = galleryData.map((data, index) => (
    <GalleryBlock key={index} data={data} />
  ));

  return <div>{galleries}</div>;
};

export default Galleries;
