import {GET_NEWS_ARTICLE_DATA} from './actions.js';

const initState = {
  newsArticleData: null
};

export default function newsArticle(state = initState, action) {
  switch (action.type) {
    case GET_NEWS_ARTICLE_DATA:
      return {
        ...state,
        newsArticleData: action.payload
      };

    default:
      return state;
  }
}
