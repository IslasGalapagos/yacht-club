import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import moment from 'moment';

import PageContent from '../../../Common/PageContent';
import NewsList from '../NewsList';
import Share from '../../../UIComponents/Share';
import Loading from '../../../UIComponents/Loading';
import ContentHeader from '../../../UIComponents/ContentHeader';

import {getNewsArticleDataAction} from './actions';

import './news_article.less';

export class NewsArticle extends Component {
  componentDidMount() {
    const {newsArticleData} = this.props;

    if (newsArticleData === null) {
      this.props.getNewsArticleData(this.props.match.params.slug);
    }
  }

  render() {
    const {newsArticleData} = this.props;

    if (newsArticleData === null) {
      return <Loading />;
    }

    const {title, content, date} = newsArticleData;
    const momentData = moment(date.string);

    return (
      <PageContent>
        <ContentHeader>
          <p className="text">
            <Link to="/media/news">← Все новости</Link>
          </p>
        </ContentHeader>

        <div className="news_article__content_wrapper">
          <div className="news_article__share_date">
            <Share />
            <p className="text -li">
              {momentData.format('DD.MM.YY')}
              <br />
              {momentData.format('hh:mm')}
            </p>
          </div>
          <div className="news_article__content">
            <h1 className="title">{title}</h1>
            {content.brief.indexOf('<script>') ? (
              <div
                className="brief"
                dangerouslySetInnerHTML={{__html: content.brief}}
              />
            ) : null}
            {content.extended.indexOf('<script>') ? (
              <div
                className="extended"
                dangerouslySetInnerHTML={{__html: content.extended}}
              />
            ) : null}
          </div>
        </div>
      </PageContent>
    );
  }
}

NewsArticle.propTypes = {
  newsArticleData: PropTypes.object,
  getNewsArticleData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  newsArticleData: store.media.newsArticle.newsArticleData,
  newsData: store.media.news.newsData
});

const mapDispatchToProps = dispatch => ({
  getNewsArticleData: name => dispatch(getNewsArticleDataAction(name))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsArticle);
