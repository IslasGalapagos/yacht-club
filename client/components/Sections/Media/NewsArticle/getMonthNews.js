const getMonthNews = (data, date) => {
    const activeYear = date.year;
    const activeMonth = date.month;

    const monthNews = [];
    data.forEach((item) => {
        if (item.date.year === activeYear && item.date.month === activeMonth) {
            monthNews.push(item);
        }
    });

    return [monthNews];
};

export default getMonthNews;
