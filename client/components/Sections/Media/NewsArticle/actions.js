import axiosF from '../../../../utils/axios';

export const GET_NEWS_ARTICLE_DATA = 'GET_NEWS_ARTICLE_DATA';

export const getNewsArticleDataAction = newsName => dispatch =>
  axiosF('/api/news-article', {
    data: {name: newsName}
  })
    .then(response => {
      dispatch({
        type: GET_NEWS_ARTICLE_DATA,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
