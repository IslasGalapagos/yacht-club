import {
  GET_ACCREDITATION_EVENTS_SUCCESS,
  SUBMIT_DISABLE,
  SUBMIT_ENABLE
} from './actions.js';

const initState = {
  accreditationEvents: null,
  submitDisable: true
};

export default function pressCentre(state = initState, action) {
  switch (action.type) {
    case GET_ACCREDITATION_EVENTS_SUCCESS:
      return {
        ...state,
        accreditationEvents: action.payload
      };

    case SUBMIT_DISABLE:
      return {
        ...state,
        submitDisable: true
      };

    case SUBMIT_ENABLE:
      return {
        ...state,
        submitDisable: false
      };

    default:
      return state;
  }
}
