import axiosF from '../../../../utils/axios';

export const GET_ACCREDITATION_EVENTS = 'GET_ACCREDITATION_EVENTS';
export const GET_ACCREDITATION_EVENTS_SUCCESS =
  'GET_ACCREDITATION_EVENTS_SUCCESS';
export const GET_ACCREDITATION_EVENTS_ERROR = 'GET_ACCREDITATION_EVENTS_ERROR';
export const SUBMIT_DISABLE = 'SUBMIT_DISABLE';
export const SUBMIT_ENABLE = 'SUBMIT_ENABLE';

export const getAccreditationEventsAction = () => dispatch =>
  axiosF('/api/accreditation-events')
    .then(response => {
      dispatch({
        type: GET_ACCREDITATION_EVENTS_SUCCESS,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });

export const submitSwitcherAction = val => {
  return {
    type: val ? SUBMIT_DISABLE : SUBMIT_ENABLE
  };
};
