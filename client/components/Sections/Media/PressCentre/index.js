import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import ContentHeader from '../../../UIComponents/ContentHeader';
import NewsList from '../NewsList';
import ContactsBlock from '../../../Sections/Contacts/ContactsBlock/';
import AccreditationPopup from '../AccreditationPopup';
import Loading from '../../../UIComponents/Loading';

import {getAccreditationEventsAction, submitSwitcherAction} from './actions';
import {getNewsDataAction} from '../News/actions';

import './press_centre.less';

export class PressCentre extends Component {
  static loadData(store) {
    const {dispatch} = store;

    return Promise.all([
      dispatch(getAccreditationEventsAction()),
      dispatch(getNewsDataAction())
    ]);
  }

  constructor(props) {
    super(props);

    this.getEventDefault = this.getEventDefault.bind(this);
  }

  componentDidMount() {
    if (this.props.newsData === null) {
      this.props.getNewsData();
    }

    if (this.props.accreditationEvents === null) {
      this.props.getAccreditationEvents();
    }

    this.accreditation();
  }

  componentDidUpdate() {
    this.accreditation();
  }

  getEventDefault() {
    const accreditationEvents = this.props.accreditationEvents;

    if (!accreditationEvents.length) return null;

    const locationState = this.props.location.state;
    let defaultEvent = accreditationEvents[0].slug;

    if (!locationState || !locationState.event) {
      return defaultEvent;
    }

    this.props.accreditationEvents.some(item => {
      if (item.slug !== locationState.event) {
        return false;
      }

      defaultEvent = locationState.event;
      return true;
    });

    return defaultEvent;
  }

  accreditation() {
    if (typeof window === 'undefined') {
      return;
    }

    if (this.props.accreditationEvents !== null) {
      const accreditationNav = document.body.querySelector(
        '.js-subnav-accreditation'
      );

      if (accreditationNav) {
        accreditationNav.classList.remove('-disabled');
      }
    }
  }

  render() {
    const {newsData, accreditationEvents} = this.props;

    if (newsData === null || accreditationEvents === null) {
      return <Loading />;
    }

    const {submitDisable, submitSwitcher} = this.props;
    const popupIsShowing =
      this.props.location.pathname === '/media/accreditation';
    const eventDefault = this.getEventDefault();

    if (newsData.length) {
      newsData.sort((a, b) => a.date.string < b.date.string);
    }

    return (
      <div className="js-press-centre">
        <ContentHeader className="press_centre__header">
          <p className="text">Пресс-центр</p>
          <form
            className="form"
            method="post"
            target="_blank"
            name="mc-embedded-subscribe-form"
            action=""
            noValidate
          >
            <input
              className="form__input -disabled"
              type="email"
              name="EMAIL"
              placeholder="Введите вашу почту"
              disabled
            />
            <input
              type="hidden"
              name="b_18206c97dd20476b117f89da9_fb20a32045"
            />
            <button
              className="form__button -disabled"
              type="button"
              value="Subscribe"
              name="subscribe"
            >
              Подписаться
            </button>
          </form>
          {accreditationEvents.length ? (
            <Link to="/media/accreditation" className="form__button -invert">
              Аккредитация на мероприятия
            </Link>
          ) : (
            <p className="form__button -invert -disabled">
              Аккредитация на мероприятия
            </p>
          )}
          <Link to="#" className="form__button -disabled">
            Фотобанк
          </Link>
        </ContentHeader>
        <ContactsBlock />
        {newsData.length ? (
          <NewsList newsData={[newsData]} monthTitle="Новости пресс-центра" />
        ) : null}
        {accreditationEvents.length ? (
          <AccreditationPopup
            events={accreditationEvents}
            eventDefault={eventDefault}
            showed={popupIsShowing}
            submitDisable={submitDisable}
            submitSwitcher={submitSwitcher}
          />
        ) : null}
      </div>
    );
  }
}

PressCentre.propTypes = {
  newsData: PropTypes.array,
  accreditationEvents: PropTypes.array,
  submitDisable: PropTypes.bool.isRequired,

  getNewsData: PropTypes.func.isRequired,
  getAccreditationEvents: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  newsData: store.media.news.newsData,
  accreditationEvents: store.media.pressCentre.accreditationEvents,
  submitDisable: store.media.pressCentre.submitDisable
});

const mapDispatchToProps = dispatch => ({
  getNewsData: () => dispatch(getNewsDataAction()),
  getAccreditationEvents: () => dispatch(getAccreditationEventsAction()),
  submitSwitcher: val => dispatch(submitSwitcherAction(val))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PressCentre);
