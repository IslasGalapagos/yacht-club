import axiosF from '../../../../utils/axios';

export const GET_GALLERY_DATA = 'GET_GALLERY_DATA';

export const getGalleryDataAction = () => dispatch =>
  axiosF('/api/gallery')
    .then(response => {
      dispatch({
        type: GET_GALLERY_DATA,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
