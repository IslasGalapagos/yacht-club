import React, {Component} from 'react';
import {connect} from 'react-redux';

import ContentHeader from '../../../UIComponents/ContentHeader';
import Galleries from '../Galleries';
import Loading from '../../../UIComponents/Loading';

import {getGalleryDataAction} from './actions';

import './gallery.less';

export class Gallery extends Component {
  static loadData(store) {
    return store.dispatch(getGalleryDataAction());
  }

  componentDidMount() {
    if (this.props.galleryData === null) {
      this.props.getGalleryData();
    }
  }

  render() {
    const {galleryData} = this.props;

    if (galleryData === null) {
      return <Loading />;
    }

    return (
      <div>
        <ContentHeader>
          <p className="text">Новости клуба</p>
        </ContentHeader>
        {galleryData.length ? (
          <Galleries data={galleryData} />
        ) : (
          <p className="text">Ни одного альбома нет</p>
        )}
      </div>
    );
  }
}

Gallery.propTypes = {
  galleryData: PropTypes.array.isRequired,

  getGalleryData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  galleryData: store.media.gallery.galleryData
});

const mapDispatchToProps = dispatch => ({
  getGalleryData: () => dispatch(getGalleryDataAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Gallery);
