import {GET_GALLERY_DATA} from './actions';

const initState = {
  galleryData: null
};

export default function gallery(state = initState, action) {
  switch (action.type) {
    case GET_GALLERY_DATA:
      return {...state, galleryData: action.payload};

    default:
      return state;
  }
}
