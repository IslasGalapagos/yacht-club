import React, {PureComponent} from 'react';
import ThumbsNav from '../../../Nav/ThumbsNav';

class Main extends PureComponent {
  render() {
    return (
      <div className="media">
        <div className="media__content">
          <div className="media__desc_wrapper">
            <h1 className="title -section_title">Медиа</h1>
          </div>
        </div>
        <ThumbsNav
          title="Подразделы проектов"
          links={[{title: 'Руководство'}]}
        />
      </div>
    );
  }
}

export default Main;
