const getNewsForShowing = (data, activeYear, enabledMonths, activeMonth) => {
  const indexOfActiveMonth = enabledMonths.indexOf(activeMonth);
  const indexOfStart = Math.max(0, indexOfActiveMonth - 2);

  // const newsForShowingObj = {}
  // for (let i = indexOfActiveMonth; i >= indexOfStart; i -= 1) {
  //   newsForShowingObj[`m${enabledMonths[i]}`] = [];
  // }

  const newsForShowingObj = {
    [`m${activeMonth}`]: []
  };

  data.forEach(item => {
    if (item.date.year !== activeYear) return;

    const month = `m${item.date.month}`;

    if (!newsForShowingObj[month]) return;

    newsForShowingObj[month].push(item);
  });

  const newsForShowing = Object.values(newsForShowingObj).sort((a, b) => {
    return a[0].date.month < b[0].date.month;
  });

  for (let i = 0; i < newsForShowing.length; i += 1) {
    newsForShowing[i].sort((a, b) => a.date.string < b.date.string);
  }

  return newsForShowing;
};

export default getNewsForShowing;
