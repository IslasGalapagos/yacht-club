const initActiveYearAndMonth = (data) => {
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth() + 1;
    const activeDate = {
        year: null,
        month: null,
    };
    let monthsOfActiveYear = null;
    // --- Set Active Year in store (begin)
    data.some((item) => {
        monthsOfActiveYear = item.months;
        activeDate.year = item._id;

        if (item._id >= currentYear) {
            return true;
        }

        return false;
    });
    // --- Set Active Year in store (end)
    // --- Set Active Month in store (begin)
    monthsOfActiveYear.some((item) => {
        activeDate.month = item;

        if (item >= currentMonth) {
            return true;
        }

        return false;
    });
    // --- Set Active Month in store (end)
    return activeDate;
};

export default initActiveYearAndMonth;
