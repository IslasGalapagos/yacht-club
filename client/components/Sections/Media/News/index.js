import React, {Component} from 'react';
import {connect} from 'react-redux';

import NewsList from '../NewsList';
import Filters from '../Filters';
import Loading from '../../../UIComponents/Loading';

import {
  dataChangingOnAction,
  dataChangingOffAction,
  getNewsDataAction,
  changeNewsForShowingAction,
  changeYearAction,
  changeMonthAction,
  changeFilterAction,
  changeEnabledFiltersAction
} from './actions';

// --- Modules for filters UI
import initActiveYearAndMonth from './initActiveYearAndMonth';
import findClosestMonth from './findClosestMonth';
import getEnabledFiltersOfYear from './getEnabledFiltersOfYear';
import getFilteredNews from './getFilteredNews';

// --- Modules for news view
import getNewsForShowing from './getNewsForShowing';

import './news.less';

export class News extends Component {
  static loadData(store) {
    return Promise.resolve(store.dispatch(getNewsDataAction())).then(result => {
      const {year, month} = initActiveYearAndMonth(result.filters);
      store.dispatch(changeYearAction(year));
      store.dispatch(changeMonthAction(month));
      store.dispatch(dataChangingOffAction());

      const enabledFIlters = getEnabledFiltersOfYear(year, result.filters);
      store.dispatch(changeEnabledFiltersAction(enabledFIlters));

      const newsForShowing = getNewsForShowing(
        result.news,
        year,
        enabledFIlters.months,
        month
      );
      store.dispatch(changeNewsForShowingAction(newsForShowing));
    });
  }

  constructor(props) {
    super(props);

    this.onYearChange = this.onYearChange.bind(this);
    this.onMonthChange = this.onMonthChange.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.changeEnabledFiltersByYear = this.changeEnabledFiltersByYear.bind(
      this
    );
    this.getNewsForShowing = this.getNewsForShowing.bind(this);
  }

  componentDidMount() {
    if (this.props.newsData === null) {
      this.props.getNewsData();
    }

    if (this.props.filtersData.length && this.props.activeYear === 0) {
      const {year, month} = initActiveYearAndMonth(this.props.filtersData);

      this.onYearChange(year);
      this.onMonthChange(month);
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // --- Set active date (year & month) to current or closest
    if (
      this.props.filtersData !== nextProps.filtersData &&
      nextProps.filtersData.length
    ) {
      const {year, month} = initActiveYearAndMonth(nextProps.filtersData);

      this.onYearChange(year);
      this.onMonthChange(month);
    }

    // --- Set filters for active year
    if (this.props.activeYear !== nextProps.activeYear) {
      const nextActiveYear = nextProps.activeYear;

      this.changeEnabledFiltersByYear(nextActiveYear);
    }

    // --- Set active month for active year – same or closest
    // (if filters is deactivated)
    if (
      !nextProps.activeFilter.filter &&
      this.props.enabledFilters.months !== nextProps.enabledFilters.months
    ) {
      const activeMonth = this.props.activeMonth;
      const nextEnabledMonths = nextProps.enabledFilters.months;

      if (nextEnabledMonths.indexOf(activeMonth) >= 0) {
        // --- Change news for showing after changing year
        const newsForShowing = this.getNewsForShowing(nextProps);
        this.props.changeNewsForShowing(newsForShowing);

        return;
      }

      const newActiveMonth = findClosestMonth(nextEnabledMonths, activeMonth);
      this.onMonthChange(newActiveMonth);
    }

    if (
      this.props.activeYear > 0 &&
      this.props.activeMonth !== nextProps.activeMonth
    ) {
      // --- Change news for showing after changing month
      const newsForShowing = this.getNewsForShowing(nextProps);
      this.props.changeNewsForShowing(newsForShowing);
    }

    if (
      nextProps.activeFilter.filter &&
      this.props.enabledFilters !== nextProps.enabledFilters
    ) {
      // --- Change news for showing after changing filter
      // (show all news by year)
      const newsData = this.props.newsData;
      const activeYear = this.props.activeYear;
      const enabledMonths = this.props.enabledFilters.months;
      const activeFilter = nextProps.activeFilter;

      const filteredNews = getFilteredNews(
        newsData,
        activeYear,
        enabledMonths,
        activeFilter
      );
      this.props.changeNewsForShowing(filteredNews);
    }
  }

  onYearChange(year) {
    this.props.changeYear(year);
    this.props.changeFilter({});
  }

  onMonthChange(month) {
    this.props.changeMonth(month);
  }

  onFilterChange(filter) {
    this.props.changeFilter(filter);

    // --- Disable/enable months
    if (filter.filter) {
      const enabledFilters = this.props.enabledFilters;
      this.props.changeEnabledFilters({...enabledFilters, months: []});
    } else {
      const activeYear = this.props.activeYear;
      this.changeEnabledFiltersByYear(activeYear);
    }
  }

  getNewsForShowing(nextProps) {
    const newsData = this.props.newsData;
    const activeYear = this.props.activeYear;
    const activeMonth = nextProps.activeMonth;
    const enabledMonths = nextProps.enabledFilters.months;

    // --- Get news-data for 3 or less (if total quantity less than 3) closest months
    // and return it
    return getNewsForShowing(newsData, activeYear, enabledMonths, activeMonth);
  }

  changeEnabledFiltersByYear(year) {
    const filtersData = this.props.filtersData;
    const enabledFilters = getEnabledFiltersOfYear(year, filtersData);
    this.props.changeEnabledFilters(enabledFilters);
  }

  render() {
    const {newsData, dataChanging} = this.props;

    if (newsData === null || dataChanging) {
      return <Loading />;
    }

    if (!newsData.length) {
      return <div className="text__section_content">Нет ни одной новости</div>;
    }

    // --- Filters UI data
    const filtersData = this.props.filtersData;
    const activeYear = this.props.activeYear;
    const activeMonth = this.props.activeMonth;
    const activeFilter = this.props.activeFilter;
    const enabledFilters = this.props.enabledFilters;

    return (
      <div>
        <Filters
          filtersData={filtersData}
          activeYear={activeYear}
          activeMonth={activeMonth}
          activeFilter={activeFilter}
          enabledFilters={enabledFilters}
          onMonthChange={this.onMonthChange}
          onYearChange={this.onYearChange}
          onFilterChange={this.onFilterChange}
        />
        {this.props.newsForShowing.length ? (
          <NewsList
            newsData={this.props.newsForShowing}
            monthTitle={`${
              activeFilter.filter ? activeFilter.filter : 'month'
            }`}
          />
        ) : null}
      </div>
    );
  }
}

News.propTypes = {
  dataChanging: PropTypes.bool.isRequired,
  newsData: PropTypes.array,
  newsForShowing: PropTypes.array.isRequired,
  filtersData: PropTypes.array.isRequired,
  activeYear: PropTypes.number.isRequired,
  activeMonth: PropTypes.number.isRequired,
  activeFilter: PropTypes.object.isRequired,
  enabledFilters: PropTypes.object.isRequired,

  getNewsData: PropTypes.func.isRequired,
  changeNewsForShowing: PropTypes.func.isRequired,
  changeYear: PropTypes.func.isRequired,
  changeMonth: PropTypes.func.isRequired,
  changeFilter: PropTypes.func.isRequired,
  changeEnabledFilters: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  dataChanging: store.media.news.dataChanging,
  newsData: store.media.news.newsData,
  newsForShowing: store.media.news.newsForShowing,
  filtersData: store.media.news.filtersData,
  activeYear: store.media.news.activeYear,
  activeMonth: store.media.news.activeMonth,
  activeFilter: store.media.news.activeFilter,
  enabledFilters: store.media.news.enabledFilters
});

const mapDispatchToProps = dispatch => ({
  dataChangingOn: () => dispatch(dataChangingOnAction()),
  dataChangingOff: () => dispatch(dataChangingOffAction()),
  getNewsData: () => dispatch(getNewsDataAction()),
  changeYear: year => dispatch(changeYearAction(year)),
  changeMonth: month => dispatch(changeMonthAction(month)),
  changeFilter: val => dispatch(changeFilterAction(val)),
  changeEnabledFilters: data => dispatch(changeEnabledFiltersAction(data)),
  changeNewsForShowing: data => dispatch(changeNewsForShowingAction(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(News);
