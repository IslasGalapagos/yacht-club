const getFilteredNews = (data, activeYear, enabledMonths, filter) => {
  const activeFilter = filter.filter;
  const activeFilterList = filter.filterList;

  const newsForShowing = [];

  data
    .sort((a, b) => a.date.month > b.date.month)
    .forEach(item => {
      if (item.date.year !== activeYear) return;

      const itemFilters = item.filters[activeFilterList];
      for (let i = 0; i < itemFilters.length; i += 1) {
        if (itemFilters[i].slug === activeFilter) {
          newsForShowing.push(item);
        }
      }
    });

  // --- [...] – Component NewsList accepts array of arrays
  return [newsForShowing];
};

export default getFilteredNews;
