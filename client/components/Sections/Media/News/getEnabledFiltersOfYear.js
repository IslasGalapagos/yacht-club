const getEnabledFiltersOfYear = (year, data) => {
  const enabledFilters = {
    years: [],
    months: [],
    classes: [],
    teams: []
  };

  data.forEach(item => {
    enabledFilters.years.push(item._id);
    if (item._id === year) {
      enabledFilters.classes = enabledFilters.classes.concat(item.classes);
      enabledFilters.teams = enabledFilters.teams.concat(item.teams);
      enabledFilters.months = enabledFilters.months.concat(item.months);
    }
  });

  return enabledFilters;
};

export default getEnabledFiltersOfYear;
