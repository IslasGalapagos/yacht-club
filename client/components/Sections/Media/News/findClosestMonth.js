const findClosestMonth = (enabledMonths, activeMonth) => {
    let closestMonth = null;

    enabledMonths.some((item) => {
        closestMonth = item;

        if (item > activeMonth) {
            return true;
        }

        return false;
    });

    return closestMonth;
};

export default findClosestMonth;
