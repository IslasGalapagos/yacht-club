import axiosF from '../../../../utils/axios';

export const DATA_CHANGING = 'DATA_CHANGING';
export const DATA_CHANGING_OFF = 'DATA_CHANGING_OFF';
export const GET_NEWS_DATA = 'GET_NEWS_DATA';
export const GET_NEWS_DATA_SUCCESS = 'GET_NEWS_DATA_SUCCESS';
export const GET_NEWS_DATA_EMPTY = 'GET_NEWS_DATA_EMPTY';
export const GET_NEWS_DATA_ERROR = 'GET_NEWS_DATA_ERROR';
export const CHANGE_NEWS_FOR_SHOWING = 'CHANGE_NEWS_FOR_SHOWING';
export const CHANGE_YEAR = 'CHANGE_YEAR';
export const CHANGE_MONTH = 'CHANGE_MONTH';
export const CHANGE_FILTER = 'CHANGE_FILTER';
export const CHANGE_FILTERS_OF_ACTIVE_YEAR = 'CHANGE_FILTERS_OF_ACTIVE_YEAR';
export const CHANGE_FILTERS_OF_ACTIVE_YEAR_SUCCESS =
  'CHANGE_FILTERS_OF_ACTIVE_YEAR_SUCCESS';

export const dataChangingOnAction = () => {
  return {
    type: DATA_CHANGING
  };
};

export const dataChangingOffAction = () => {
  return {
    type: DATA_CHANGING_OFF
  };
};

export const getNewsDataAction = () => dispatch =>
  axiosF('/api/news')
    .then(response => {
      const respType = response.data.news.length
        ? GET_NEWS_DATA_SUCCESS
        : GET_NEWS_DATA_EMPTY;

      dispatch({
        type: respType,
        payload: response.data
      });

      return response.data;
    })
    .catch(e => {
      console.log(e);
    });

export const changeYearAction = year => {
  return {
    type: CHANGE_YEAR,
    payload: year
  };
};

export const changeMonthAction = month => {
  return {
    type: CHANGE_MONTH,
    payload: month
  };
};

export const changeFilterAction = filter => {
  return {
    type: CHANGE_FILTER,
    payload: filter
  };
};

export const changeEnabledFiltersAction = data => dispatch => {
  dispatch({
    type: CHANGE_FILTERS_OF_ACTIVE_YEAR,
    payload: data
  });
  dispatch({
    type: CHANGE_FILTERS_OF_ACTIVE_YEAR_SUCCESS
  });
};

export const changeNewsForShowingAction = data => {
  return {
    type: CHANGE_NEWS_FOR_SHOWING,
    payload: data
  };
};
