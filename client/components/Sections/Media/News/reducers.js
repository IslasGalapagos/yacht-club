import {
  DATA_CHANGING,
  DATA_CHANGING_OFF,
  GET_NEWS_DATA_SUCCESS,
  GET_NEWS_DATA_EMPTY,
  CHANGE_NEWS_FOR_SHOWING,
  CHANGE_YEAR,
  CHANGE_MONTH,
  CHANGE_FILTER,
  CHANGE_FILTERS_OF_ACTIVE_YEAR
  // CHANGE_FILTERS_OF_ACTIVE_YEAR_SUCCESS,
} from './actions';

const initState = {
  dataChanging: false,
  // Filters UI data
  filtersData: [],
  activeYear: 0,
  activeMonth: 0,
  activeFilter: {},
  enabledFilters: {},
  // News data
  newsData: null,
  newsForShowing: []
};

export default function news(state = initState, action) {
  switch (action.type) {
    case DATA_CHANGING:
      return {...state, dataChanging: true};

    case DATA_CHANGING_OFF:
      return {...state, dataChanging: false};

    case GET_NEWS_DATA_SUCCESS:
      return {
        ...state,
        newsData: action.payload.news,
        filtersData: action.payload.filters
      };

    case GET_NEWS_DATA_EMPTY:
      return {
        ...state,
        dataChanging: false
      };

    case CHANGE_NEWS_FOR_SHOWING:
      return {
        ...state,
        newsForShowing: action.payload,
        dataChanging: false
      };

    case CHANGE_YEAR:
      return {...state, activeYear: action.payload, dataChanging: true};

    case CHANGE_MONTH:
      return {...state, activeMonth: action.payload, dataChanging: true};

    case CHANGE_FILTER:
      return {...state, activeFilter: action.payload, dataChanging: true};

    case CHANGE_FILTERS_OF_ACTIVE_YEAR:
      return {...state, enabledFilters: action.payload};

    default:
      return state;
  }
}
