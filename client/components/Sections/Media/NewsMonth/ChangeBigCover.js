class ChangeBigCover {
    constructor(elem) {
        this.currentElem = elem;

        this.mouseOver = this.mouseOver.bind(this);
        this.mouseOver = this.mouseOver.bind(this);
    }

    mouseOver(e) {
        if (this.currentElem) return;

        let target = e.target;
        while (target !== this) {
            if (target.tagName === 'LI') break;
            target = target.parentNode;
        }
        if (target === this) return;

        this.currentElem = target;

        const miniCover = this.currentElem.querySelector('.js-news-mini-cover');
        const imgSrc = miniCover ? miniCover.src : '';
        const titleText = this.currentElem.querySelector('.js-news-title').innerHTML;

        const newsWrapper = this.currentElem.closest('.js-month-news-wrapper');
        const bigCover = newsWrapper.querySelector('.js-news-big-cover');
        const bigTitle = bigCover.parentNode.querySelector('.js-news-big-title');

        bigCover.src = imgSrc;
        bigTitle.innerHTML = titleText;

        if (!imgSrc) {
            bigTitle.classList.add('-without_bg');
            return;
        }

        bigTitle.classList.remove('-without_bg');
    }
    mouseOut(e) {
        if (!this.currentElem) return;

        let relatedTarget = e.relatedTarget;
        if (relatedTarget) {
            while (relatedTarget) {
                if (relatedTarget === this.currentElem) return;
                relatedTarget = relatedTarget.parentNode;
            }
        }

        this.currentElem = null;
    }
}

export default ChangeBigCover;
