import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import ScrollArea from 'react-scrollbar/dist/no-css';
import moment from 'moment';

import ContentHeader from '../../../UIComponents/ContentHeader';

import cutTitle from '../../../../js/cutTitle.js';

import './news_month.less';
import './custom_scrollbar.less';

const NewsMonth = props => {
  const activeArticleData = {
    img: props.monthData[0].cover ? props.monthData[0].cover.url : null,
    title: props.monthData[0].title
  };

  const $items = props.monthData.map(item => (
    <li key={item.slug} className="news_month__item">
      <Link to={`/media/news/${item.slug}`}>
        <div className="news_month__cover">
          {item.cover ? (
            <img src={item.cover.url} alt="" className="js-news-mini-cover" />
          ) : null}
        </div>
        <div className="news_month__text_wrapper">
          <div className="news_month__title">
            <p className="text -li date">
              {moment(item.date.string).format('DD.MM.YY')}
            </p>
            <p className="text -b js-news-title">{cutTitle(item.title, 64)}</p>
          </div>
          {item.content.brief.indexOf('<script>') ? (
            <div
              className="text brief"
              dangerouslySetInnerHTML={{__html: item.content.brief}}
            />
          ) : null}
        </div>
      </Link>
    </li>
  ));

  let currentElem = null;

  function mouseOver(e) {
    if (currentElem) return;

    let target = e.target;
    while (target !== this) {
      if (target.tagName === 'LI') break;
      target = target.parentNode;
    }
    if (target === this) return;

    currentElem = target;

    const miniCover = currentElem.querySelector('.js-news-mini-cover');
    const imgSrc = miniCover ? miniCover.src : '';
    const titleText = currentElem.querySelector('.js-news-title').innerHTML;

    const newsWrapper = currentElem.closest('.js-month-news-wrapper');
    const bigCover = newsWrapper.querySelector('.js-news-big-cover');
    const bigTitle = bigCover.parentNode.querySelector('.js-news-big-title');

    bigCover.src = imgSrc;
    bigTitle.innerHTML = titleText;

    if (!imgSrc) {
      bigTitle.classList.add('-without_bg');
      return;
    }

    bigTitle.classList.remove('-without_bg');
  }

  function mouseOut(e) {
    if (!currentElem) return;

    let relatedTarget = e.relatedTarget;
    if (relatedTarget) {
      while (relatedTarget) {
        if (relatedTarget === currentElem) return;
        relatedTarget = relatedTarget.parentNode;
      }
    }

    currentElem = null;
  }

  const monthTitle =
    props.monthTitle === 'month'
      ? moment()
          .locale('ru')
          .month(props.monthData[0].date.month - 1)
          .format('MMMM')
      : props.monthTitle;

  return (
    <Fragment>
      {monthTitle ? (
        <ContentHeader>
          <p className="text -capitilize">{monthTitle}</p>
        </ContentHeader>
      ) : null}
      <div className="news_month__wrapper js-month-news-wrapper">
        <div className="news_month__big_cover">
          <h1 className="title js-news-big-title">{activeArticleData.title}</h1>
          <img
            src={activeArticleData.img ? activeArticleData.img : ''}
            alt=""
            className="js-news-big-cover"
          />
        </div>
        <ScrollArea horizontal={false} className="news_month__custom_scrollbar">
          <ul
            className="news_month"
            onMouseOver={mouseOver}
            onMouseOut={mouseOut}
          >
            {$items}
          </ul>
        </ScrollArea>
      </div>
    </Fragment>
  );
};

export default NewsMonth;
