const changingInterval = (images, media, mediaView) => {
    return () => {
        const randomImgIndx = Math.floor(Math.random() * (images.length - 1));
        const selectedImage = images[randomImgIndx];

        let randomMediaIndx = Math.floor(Math.random() * (media.length));
        let selectedMediaUrl = media[randomMediaIndx].url;

        while (selectedMediaUrl === selectedImage.src) {
            randomMediaIndx = Math.floor(Math.random() * (media.length));
            selectedMediaUrl = media[randomMediaIndx].url;
        }

        selectedImage.classList.add('-hidden');

        setTimeout(() => {
            selectedImage.src = selectedMediaUrl;
            selectedImage.classList.remove('-hidden');

            if (selectedImage.classList.contains('-active')) {
                mediaView.src = selectedMediaUrl;
            }
        }, 300);
    };
};

const randomizeData = (data, wrapper) => {
    const images = wrapper.querySelector('.js-gallery-items').querySelectorAll('img');
    const mediaView = wrapper.querySelector('.js-gallery-view');

    const allMedia = [];

    let i = 0;
    let media = 'photos';
    while (i > -1) {
        if (i < data[media].length) {
            allMedia.push(data[media][i]);
            i += 1;
        }

        if (i === data[media].length) {
            if (media === 'photos') {
                media = 'video';
                i = 0;
            }

            if (media === 'video') i = -1;
        }
    }

    allMedia.sort(() => 0.5 - Math.random());

    mediaView.src = allMedia[0].url;

    let imgIndx = 0;
    for (let m = 0; m < allMedia.length; m += 1) {
        images[imgIndx].src = allMedia[m].url;
        imgIndx += 1;

        if (m === allMedia.length - 1) m = -1;
        if (imgIndx === images.length) m = allMedia.length;
    }

    return {
        imagesArr: images,
        allMediaArr: allMedia,
        mediaViewEl: mediaView,
    };
};

const changeMedia = (data, wrapper) => {
    const {
        imagesArr: images,
        allMediaArr: allMedia,
        mediaViewEl: mediaView,
    } = randomizeData(data, wrapper);

    return setInterval(changingInterval(images, allMedia, mediaView), 1500);
};

export default changeMedia;
