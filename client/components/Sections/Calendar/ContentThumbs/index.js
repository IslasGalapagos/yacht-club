import React from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';

import './content_thumbs.less';

const thumbsOrders = {
  1: ['big'],
  2: ['big', 'big'],
  3: ['small', 'big', 'small'],
  4: ['small', 'small', 'small', 'small'],

  set concatAndSet(orders) {
    let newOrder = [];
    let num = 0;
    orders.forEach(item => {
      newOrder = newOrder.concat(this[item]);
      num += item;
    });
    this[num] = newOrder;
  }
};

const otherOrders = [
  [3, 2], // 5
  [2, 4], // 6
  [3, 4], // 7
  [2, 4, 2], // 8
  [3, 4, 2], // 9
  [3, 4, 3], // 10
  [4, 2, 3, 2], // 11
  [3, 2, 4, 3], // 12
  [4, 2, 4, 3], // 13
  [3, 4, 3, 4], // 14
  [3, 2, 4, 2, 4], // 15
  [4, 3, 2, 3, 4], // 16
  [3, 2, 3, 4, 2, 3], // 17
  [2, 3, 2, 4, 3, 4], // 18
  [4, 2, 3, 2, 4, 2, 2], // 19
  [3, 2, 4, 3, 4, 2, 2] // 20
];

for (let i = 0; i < otherOrders.length; i += 1) {
  thumbsOrders.concatAndSet = otherOrders[i];
}

const getThumb = (type, eventData, locations, eventDate) => {
  let textBlock = null;
  if (type === 'small') {
    textBlock = (
      <div
        className="events_thumbs__text_block"
        style={{color: eventData.smallPosterColor}}
      >
        <h1 className="name">{eventData.title}</h1>
        <p className="text -l -location">{locations}</p>
        <p className="text -l -date">{eventDate}</p>
      </div>
    );
  }

  if (type === 'big') {
    textBlock = (
      <div
        className="events_thumbs__text_block"
        style={{color: eventData.bigPosterColor}}
      >
        <p className="text -b -location">{locations}</p>
        <p className="text -b -date">{eventDate}</p>
        {eventData.content.brief.indexOf('<script>') ? (
          <div
            className="text -l -desc"
            dangerouslySetInnerHTML={{__html: eventData.content.brief}}
          />
        ) : null}
        <h1 className="name">{eventData.title}</h1>
      </div>
    );
  }

  return (
    <li
      key={eventData.title}
      className={`events_thumbs__item -${type} ${
        eventData.meaningful ? '-meaningful' : ''
      }`}
    >
      <Link
        to={`/calendar/event/${eventData.slug}`}
        className="events_thumbs__event_link"
      >
        {textBlock}
        <div className="events_thumbs__cover">
          {eventData.cover ? <img src={eventData.cover.url} alt="" /> : null}
        </div>
      </Link>
    </li>
  );
};

const ContentThumbs = props => {
  const $thumbs = props.content.map((item, index, arr) => {
    const locations = item.eventLocation
      .map(location => location.location)
      .join(', ');

    const eventDate =
      item.date.start.month !== item.date.start.month
        ? `${moment(item.date.start.string)
            .locale('ru')
            .format('DD MMMM')}
            - ${moment(item.date.end.string)
              .locale('ru')
              .format('DD MMMM')}`
        : `${moment(item.date.start.string)
            .locale('ru')
            .format('DD')}
            - ${moment(item.date.end.string)
              .locale('ru')
              .format('DD MMMM')}`;

    const thumbType = thumbsOrders[arr.length][index];

    return getThumb(thumbType, item, locations, eventDate);
  });

  if ($thumbs.length === 1) {
    $thumbs.push(<li key="empty" className="events_thumbs__item -empty" />);
  }

  return (
    <div {...props} className="content_posters">
      <ul className="events_thumbs">{$thumbs}</ul>
    </div>
  );
};

export default ContentThumbs;
