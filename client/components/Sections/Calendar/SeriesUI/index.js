import React from 'react';
import {Link} from 'react-router-dom';

import ContentHeader from '../../../UIComponents/ContentHeader';

import './series_ui.less';

const SeriesUI = props => {
  const seriesData = {
    years: [],
    phases: []
  };

  let currentYear = '';

  props.otherEvents.forEach(item => {
    if (item.year === props.activeYear) {
      seriesData.phases.push(
        <li key={item.slug} className="event_series__phase">
          {item.phase === props.activePhase ? (
            <p className="text -sb -active">{item.phase}</p>
          ) : (
            <Link to={`/calendar/event/${item.slug}`}>
              <p className="text -sb">{item.phase}</p>
            </Link>
          )}
        </li>
      );
    }

    if (currentYear !== item.year) {
      seriesData.years.push(
        <li key={item.slug} className="event_series__year">
          {item.year === props.activeYear ? (
            <p className="text -active">{item.year}</p>
          ) : (
            <p className="text">
              <Link to={`/calendar/event/${item.slug}`}>{item.year}</Link>
            </p>
          )}
        </li>
      );

      currentYear = item.year;
    }
  });

  return (
    <div>
      <ContentHeader>
        <p className="text">{props.name}</p>
        <ul className="event_series__years">{seriesData.years}</ul>
      </ContentHeader>
      <div className="event_series__phases_wrapper">
        <p className="text -l -title">Этапы:</p>
        <ul className="event_series__phases">{seriesData.phases}</ul>
      </div>
    </div>
  );
};

export default SeriesUI;
