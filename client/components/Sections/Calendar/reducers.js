import {
  CHANGE_DISPLAY,
  CHANGE_DATE,
  CHANGE_FILTER,
  GET_ALL_CALENDAR_UI_DATA_SUCCESS,
  GET_MONTH_EVENTS_DATA_SUCCESS
} from './actions';

const initialState = {
  display: 3,
  activeFilter: {
    filter: '',
    filterList: ''
  },
  activeDate: {
    year: new Date().getFullYear(),
    month: new Date().getMonth() + 1
  },
  UIData: null,
  content: null
};

export default function calendar(state = initialState, action) {
  switch (action.type) {
    case CHANGE_DISPLAY:
      return {...state, display: action.payload};

    case CHANGE_DATE:
      return {...state, activeDate: action.payload};

    case CHANGE_FILTER:
      return {...state, activeFilter: action.payload};

    case GET_ALL_CALENDAR_UI_DATA_SUCCESS:
      return {...state, UIData: action.payload};

    case GET_MONTH_EVENTS_DATA_SUCCESS:
      return {...state, content: action.payload};

    default:
      return state;
  }
}
