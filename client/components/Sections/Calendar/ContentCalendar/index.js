import React from 'react';
import {Link} from 'react-router-dom';

import './content_calendar.less';

import cutTitle from '../../../../js/cutTitle.js';

const getTodayMonthNum = active => {
  const now = new Date();
  return now.getMonth() === active ? now.getDate() - 1 : 0;
};

const goToAnotherMonth = cb => e => {
  e.preventDefault();

  let year;
  let monthNum = +e.target.getAttribute('href').substr(1);
  if (monthNum < 1) {
    year = -1;
    monthNum = 12;
  }
  if (monthNum > 12) {
    year = 1;
    monthNum = 1;
  }

  if (typeof cb === 'function') cb(monthNum, year);
};

const getActiveDaysContent = (content, weekDay) => {
  const activeDaysContent = {};
  content.sort((a, b) => {
    return a.date.length > b.date.length;
  });
  content.forEach(item => {
    const date = item.date.start.day + (weekDay - 1);

    if (!activeDaysContent[date]) activeDaysContent[date] = [];
    activeDaysContent[date].push({
      link: item.slug,
      title: cutTitle(item.title, 27),
      color: item.calendarColor,
      start: true,
      end: item.date.length === 1,
      dateStart: item.date.start.day,
      remainingLength: item.date.length
    });

    for (let i = 1; i < item.date.length; i += 1) {
      if (!activeDaysContent[date + i]) activeDaysContent[date + i] = [];
      activeDaysContent[date + i].push({
        link: item.slug,
        color: item.calendarColor,
        end: i + 1 === item.date.length,
        dateStart: item.date.start.day,
        remainingLength: item.date.length - i
      });
    }
  });

  return activeDaysContent;
};

/*
--- Options:
    activeMonth: bool,
    date: num – date of day,
    className: str – class for <li>
    link: str – month num / event url,
    onclick: function – for <a>,
    activeDateData: arr – event(s) data (title, line, link)
*/
const getDayContent = options => {
  if (!options.activeMonth) {
    return (
      <li
        key={`${options.date}${options.activeMonth}`}
        className="calendar__month_day -prev_next_month"
      >
        <a href={options.link} onClick={options.onClick}>
          <p className="date_num">{options.date}</p>
        </a>
      </li>
    );
  }

  if (!options.activeDateData) {
    return (
      <li
        key={`${options.date}${options.activeMonth}`}
        className={`calendar__month_day ${options.className}`}
      >
        <p className="date_num">{options.date}</p>
      </li>
    );
  }

  let titles = []; // eslint-disable-line
  let lines = []; // eslint-disable-line

  if (options.activeDateData[1]) {
    options.activeDateData
      .sort((a, b) => {
        return a.dateStart < b.dateStart;
      })
      .forEach(item => {
        if (item.title) {
          titles.push(
            <li
              key={item.title.substr(0, 7)}
              className="calendar__titles_item text -l"
            >
              <Link to={`/calendar/event/${item.link}`}>
                {item.title}
                <span className="point" style={{backgroundColor: item.color}} />
              </Link>
            </li>
          );
        }
        lines.push(
          <li
            key={item.link.substr(0, 7)}
            className={`calendar__lines_item ${item.start ? '-start' : ''} ${
              item.end ? '-end' : ''
            }`}
            data-length={item.remainingLength}
          >
            <Link to={`/calendar/event/${item.link}`}>
              <div className="line" style={{backgroundColor: item.color}} />
            </Link>
          </li>
        );
      });

    return (
      <li
        key={`${options.date}${options.activeMonth}`}
        className={`calendar__month_day -active ${options.className}`}
      >
        <div className="calendar__day_inner">
          {titles.length ? (
            <ul className="calendar__titles">{titles}</ul>
          ) : null}
          <ul className="calendar__lines">{lines}</ul>
          <p className="date_num">{options.date}</p>
        </div>
      </li>
    );
  }

  // One event on date ↓
  const dayData = options.activeDateData[0];

  return (
    <li
      key={`${options.date}${options.activeMonth}`}
      className={`calendar__month_day -active ${options.className}`}
    >
      <Link to={`/calendar/event/${dayData.link}`}>
        <div className="calendar__day_inner">
          {dayData.title ? (
            <ul className="calendar__titles">
              <li className="calendar__titles_item text -l">
                {dayData.title}
                <span
                  className="point"
                  style={{backgroundColor: dayData.color}}
                />
              </li>
            </ul>
          ) : null}
          <ul className="calendar__lines">
            <li
              className={`calendar__lines_item ${
                dayData.start ? '-start' : ''
              } ${dayData.end ? '-end' : ''}`}
              data-length={dayData.remainingLength}
            >
              <div className="line" style={{backgroundColor: dayData.color}} />
            </li>
          </ul>
          <p className="date_num">{options.date}</p>
        </div>
      </Link>
    </li>
  );
};

const getDays = props => {
  // --- Prev, Active & Next Months Days (moment.js)
  const activeDate = props.date;
  const firstDayOfActiveM = activeDate.day() || 7;
  const firstDayOfNextM =
    activeDate
      .clone()
      .endOf('month')
      .date() + firstDayOfActiveM;
  let dateCount =
    firstDayOfActiveM === 1
      ? 1
      : activeDate
          .clone()
          .date(0)
          .date() -
        (firstDayOfActiveM - 2);

  // --- props.date – moment.js date obj
  let todayMonthNum = getTodayMonthNum(props.date.month());
  todayMonthNum = todayMonthNum > 0 ? todayMonthNum + firstDayOfActiveM : 0;
  //---
  const totalNumOfDays = firstDayOfNextM > 35 ? 42 : 35;

  // --- Elements
  const activeDaysData = getActiveDaysContent(props.content, firstDayOfActiveM);
  const $monthDays = [];

  const getNumOfAnotherMonth = i => {
    let numOfMonth = activeDate.month();
    if (i >= firstDayOfNextM) numOfMonth += 2;

    return numOfMonth;
  };

  for (let i = 1; i <= totalNumOfDays; i += 1) {
    const isActiveMonth = i >= firstDayOfActiveM && i < firstDayOfNextM;

    // --- Get date content:
    // unactive month – link to it & onClick event
    // active month – className for current date & data of event
    $monthDays.push(
      getDayContent({
        activeMonth: isActiveMonth,
        date: dateCount,
        link: isActiveMonth ? null : `#${getNumOfAnotherMonth(i)}`,
        onClick: isActiveMonth ? null : goToAnotherMonth(props.onMonthChange),
        className: i === todayMonthNum ? '-today' : '',
        activeDateData: activeDaysData[i] ? activeDaysData[i] : null
      })
    );

    // --- Date count: reset to first day of month / next date
    dateCount =
      i === firstDayOfActiveM - 1 || i === firstDayOfNextM - 1
        ? 1
        : dateCount + 1;
  }

  return $monthDays;
};

const ContentCalendar = props => {
  return (
    <div className="content_calendar">
      <ul className="calendar__week">
        <li key={1} className="text -l calendar__week_day">
          понедельник
        </li>
        <li key={2} className="text -l calendar__week_day">
          вторник
        </li>
        <li key={3} className="text -l calendar__week_day">
          среда
        </li>
        <li key={4} className="text -l calendar__week_day">
          четверг
        </li>
        <li key={5} className="text -l calendar__week_day">
          пятница
        </li>
        <li key={6} className="text -l calendar__week_day">
          суббота
        </li>
        <li key={7} className="text -l calendar__week_day">
          воскресенье
        </li>
      </ul>
      <ul className="calendar__month">{getDays(props)}</ul>
    </div>
  );
};

export default ContentCalendar;
