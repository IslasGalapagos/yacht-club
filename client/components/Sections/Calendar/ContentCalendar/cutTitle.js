const cutTitle = (title, maxLength) => {
    if (title.length <= maxLength) return title;

    let cuttingTitle = title.substr(0, maxLength);

    if (title.substr(maxLength, 1) !== ' ') {
        const indexOfLastSpace = cuttingTitle.lastIndexOf(' ');
        cuttingTitle = cuttingTitle.substr(0, indexOfLastSpace);
    }

    cuttingTitle += '…';

    return cuttingTitle;
};

export default cutTitle;
