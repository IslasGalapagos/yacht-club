import axiosF from '../../../../utils/axios';

export const GET_EVENT_DATA = 'GET_EVENT_DATA';

export const getEventDataAction = slug => dispatch =>
  axiosF('/api/event', {
    data: {eventSlug: slug}
  })
    .then(response => {
      dispatch({
        type: GET_EVENT_DATA,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
