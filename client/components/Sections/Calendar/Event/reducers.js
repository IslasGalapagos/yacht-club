import {GET_EVENT_DATA} from './actions.js';

const initState = {
  eventData: null
};

export default function event(state = initState, action) {
  switch (action.type) {
    case GET_EVENT_DATA:
      return {...state, eventData: action.payload};

    default:
      return state;
  }
}
