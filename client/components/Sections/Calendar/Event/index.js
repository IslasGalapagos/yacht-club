import React, {Component} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {Link, withRouter} from 'react-router-dom';

import PageContent from '../../../Common/PageContent';
import ContentHeader from '../../../UIComponents/ContentHeader';
import Slideshow from '../../../UIComponents/Slideshow';
import Share from '../../../UIComponents/Share';
import SeriesUI from '../SeriesUI';
import NewsList from '../../../Sections/Media/NewsList';
import GalleryBlock from '../../Media/GalleryBlock';
import Loading from '../../../UIComponents/Loading';

import {getEventDataAction} from './actions';

import './event.less';

export class Event extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: this.props.eventData === null
    };
  }

  componentDidMount() {
    if (this.props.eventData === null) {
      const {slug} = this.props.match.params;

      Promise.resolve(this.props.getEventData(slug)).then(() => {
        this.setState({
          loading: false
        });
      });
    }

    this.accreditation();
  }

  componentDidUpdate() {
    const {eventData} = this.props;

    if (eventData === null) {
      return;
    }

    const {slug} = this.props.match.params;

    if (eventData.slug !== slug && !this.state.loading) {
      this.setState({
        loading: true
      });

      Promise.resolve(this.props.getEventData(slug)).then(() => {
        this.setState({
          loading: false
        });
      });
    }

    this.accreditation();
  }

  accreditation() {
    const {eventData} = this.props;

    if (typeof window === 'undefined' || eventData === null) {
      return;
    }

    if (eventData.accreditation) {
      const accreditationNav = document.body.querySelector(
        '.js-subnav-accreditation'
      );

      if (accreditationNav) {
        accreditationNav.classList.remove('-disabled');
      }
    }
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    }

    const {
      title,
      slug,
      content,
      cover,
      logo,
      siteLink,
      additional,
      accreditation,
      results,
      resultsLink,
      series,
      date,
      siblings,
      slideshow,
      gallery,
      news = []
    } = this.props.eventData;

    if (news.length) news.sort((a, b) => a.date.string < b.date.string);

    return (
      <PageContent className="-event">
        {slideshow && slideshow.length ? <Slideshow data={slideshow} /> : null}
        {series.name.length ? (
          <SeriesUI
            name={series.name[0].series}
            otherEvents={series.otherEvents}
            activeYear={date.start.year}
            activePhase={series.phase}
          />
        ) : null}
        <div className="event__info">
          <Share
            data={{
              shrTitle: title,
              shrDesc: content.brief,
              shrImage: cover ? cover.url : null
            }}
          />
          <div className="event__desc">
            <h1 className="title">{title}</h1>
            {content.brief.indexOf('<script>') ? (
              <div
                className="brief"
                dangerouslySetInnerHTML={{__html: content.brief}}
              />
            ) : null}
            {content.extended.indexOf('<script>') ? (
              <div
                className="extended"
                dangerouslySetInnerHTML={{__html: content.extended}}
              />
            ) : null}
          </div>
          <div className="event__links">
            {logo ? (
              <img src={logo.url} alt="" className="event__logo" />
            ) : null}
            {siteLink ? (
              <p className="event__site_link">
                <a href={siteLink} className="text -li">
                  Сайт мероприятия
                </a>
              </p>
            ) : null}
            <p className="event__accreditation_link">
              {results ? (
                <a
                  href={`${results === 'ext_link' ? resultsLink : results}`}
                  className="text -li"
                  target={`${results === 'ext_link' ? '_blank' : ''}`}
                  rel={`${results === 'ext_link' ? 'noopener noreferrer' : ''}`}
                >
                  Результаты
                </a>
              ) : null}
              {results && accreditation ? (
                <span className="text"> и </span>
              ) : null}
              {accreditation ? (
                <Link
                  to={{
                    pathname: '/media/accreditation',
                    state: {event: slug}
                  }}
                  className="text -li"
                >
                  Аккредитация
                </Link>
              ) : null}
            </p>
            <div className="event__team" />
          </div>
        </div>
        {news.length ? (
          <div className="event__news">
            <NewsList newsData={[news]} />
          </div>
        ) : null}
        {gallery.photos.length || gallery.video.length ? (
          <GalleryBlock data={gallery} withLink />
        ) : null}
        {additional && additional.indexOf('<script>') < 0 ? (
          <div className="event__additional">
            {results ? <div id="results" /> : null}
            <div
              className="event__additional_content"
              dangerouslySetInnerHTML={{__html: additional}}
            />
          </div>
        ) : null}
        <ContentHeader className="-event">
          {siblings[0] ? (
            <p className="text">
              <Link to={`/calendar/event/${siblings[0]}`}>
                Предыдущее мероприятие
              </Link>
            </p>
          ) : (
            <p className="text -disable">Предыдущее мероприятие</p>
          )}
          <p className="text">
            <Link to="/calendar">Перейти к календарю событий</Link>
          </p>
          {siblings[1] ? (
            <p className="text">
              <Link to={`/calendar/event/${siblings[1]}`}>
                Следующее мероприятие
              </Link>
            </p>
          ) : (
            <p className="text -disable">Следующее мероприятие</p>
          )}
        </ContentHeader>
      </PageContent>
    );
  }
}

Event.propTypes = {
  eventData: PropTypes.object,
  getEventData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  eventData: store.event.eventData
});

const mapDispatchToProps = dispatch => ({
  getEventData: slug => dispatch(getEventDataAction(slug))
});

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Event);
