import axiosF from '../../../utils/axios';

export const CHANGE_DISPLAY = 'CHANGE_DISPLAY';
export const CHANGE_DATE = 'CHANGE_DATE';
export const CHANGE_FILTER = 'CHANGE_FILTER';
export const GET_ALL_CALENDAR_UI_DATA = 'GET_ALL_CALENDAR_UI_DATA';
export const GET_ALL_CALENDAR_UI_DATA_SUCCESS =
  'GET_ALL_CALENDAR_UI_DATA_SUCCESS';
export const GET_ALL_CALENDAR_UI_DATA_ERROR = 'GET_ALL_CALENDAR_UI_DATA_ERROR';
export const GET_MONTH_EVENTS_DATA = 'GET_MONTH_EVENTS_DATA';
export const GET_MONTH_EVENTS_DATA_SUCCESS = 'GET_MONTH_EVENTS_DATA_SUCCESS';
export const GET_MONTH_EVENTS_DATA_ERROR = 'GET_MONTH_EVENTS_DATA_ERROR';

export const changeDisplayAction = val => {
  return {
    type: CHANGE_DISPLAY,
    payload: val
  };
};

export const changeDateAction = val => {
  return {
    type: CHANGE_DATE,
    payload: val
  };
};

export const changeFilterAction = val => {
  return {
    type: CHANGE_FILTER,
    payload: val
  };
};

export const getCalendarUIDataAction = () => dispatch =>
  axiosF('/api/events/ui')
    .then(response => {
      dispatch({
        type: GET_ALL_CALENDAR_UI_DATA_SUCCESS,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });

export const getMonthEventsAction = date => dispatch =>
  axiosF('/api/events', {
    data: date
  })
    .then(response => {
      dispatch({
        type: GET_MONTH_EVENTS_DATA_SUCCESS,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
