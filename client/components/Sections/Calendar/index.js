import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import PageContent from '../../Common/PageContent';
import ContentHeader from '../../UIComponents/ContentHeader';
import Filters from './Filters';
import ContentThumbs from './ContentThumbs';
import ContentList from './ContentList';
import ContentCalendar from './ContentCalendar';
import Loading from '../../UIComponents/Loading';

import {
  changeDisplayAction,
  changeDateAction,
  changeFilterAction,
  getCalendarUIDataAction,
  getMonthEventsAction
} from './actions';

export class Calendar extends Component {
  constructor(props) {
    super(props);

    this.onDisplayChange = this.onDisplayChange.bind(this);
    this.onYearChange = this.onYearChange.bind(this);
    this.onMonthChange = this.onMonthChange.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
  }

  componentDidMount() {
    const {UIData, content} = this.props;

    if (UIData === null) {
      this.props.getCalendarUIData();
    }

    if (content === null) {
      this.props.getMonthEvents(this.props.activeDate);
    }
  }

  shouldComponentUpdate(nextProps) {
    if (!Object.is(nextProps.activeDate, this.props.activeDate)) {
      return false;
    }

    return true;
  }

  onDisplayChange(type) {
    this.props.changeDisplay(type);
  }

  onYearChange(year) {
    this.changeDate(year);
  }

  onMonthChange(month, year) {
    this.changeDate(year, month);
  }

  onFilterChange(val) {
    if (val === this.props.activeFilter) {
      this.props.changeFilter('');
      return;
    }

    this.props.changeFilter(val);
  }

  getSiblingYear(sibling) {
    const activeYear = `${this.props.activeDate.year}`;
    const allYears = this.props.UIData.filters.years;
    if (allYears.length <= 1) return false;

    const activeIndex = allYears.indexOf(activeYear);

    if (sibling < 0) {
      if (activeIndex === 0) return false;
      return +allYears[activeIndex - 1];
    }
    if (activeIndex === allYears.length - 1) return false;
    return +allYears[activeIndex + 1];
  }

  changeDate(
    paramYear = this.props.activeDate.year,
    paramMonth = this.props.activeDate.month
  ) {
    if (paramYear === -1) paramYear = this.getSiblingYear(-1);
    if (paramYear === 1) paramYear = this.getSiblingYear(1);
    if (!paramYear || !paramMonth) return;

    Promise.resolve(
      this.props.changeDate({
        year: paramYear,
        month: paramMonth
      })
    ).then(() => {
      this.props.getMonthEvents(this.props.activeDate);
    });
  }

  render() {
    const {UIData} = this.props;
    let {content} = this.props;

    if (UIData === null || content === null) {
      return <Loading />;
    }

    const {
      children,
      display: displayType,
      activeDate,
      activeFilter
    } = this.props;

    if (activeFilter.filter !== '') {
      content = content.filter(list => {
        const isIt = list[activeFilter.filterList].some(item => {
          return item.slug === activeFilter.filter;
        });

        return isIt;
      });
    }

    const activeDateMoment = moment({
      y: activeDate.year,
      M: activeDate.month - 1
    }).locale('ru');

    return (
      <PageContent>
        {!children ? (
          <div>
            <Filters
              display={displayType}
              UIData={UIData}
              date={activeDate}
              activeFilter={activeFilter}
              onDisplayChange={this.onDisplayChange}
              onYearChange={this.onYearChange}
              onMonthChange={this.onMonthChange}
              onFilterChange={this.onFilterChange}
            />
            <ContentHeader>
              <p className="text">
                События, {activeDateMoment.format('MMMM YYYY')}
              </p>
            </ContentHeader>
            {(() => {
              switch (displayType) {
                case 1:
                  return <ContentThumbs content={content} />;
                case 2:
                  return (
                    <ContentList
                      content={content}
                      activeFilter={activeFilter}
                      onFilterChange={this.onFilterChange}
                    />
                  );
                case 3:
                  return (
                    <ContentCalendar
                      date={activeDateMoment}
                      content={content}
                      onMonthChange={this.onMonthChange}
                    />
                  );
                default:
                  return null;
              }
            })()}
          </div>
        ) : null}
        {children}
      </PageContent>
    );
  }
}

Calendar.propTypes = {
  display: PropTypes.number.isRequired,
  activeDate: PropTypes.object.isRequired,
  activeFilter: PropTypes.object.isRequired,
  UIData: PropTypes.object,
  content: PropTypes.array,

  changeDisplay: PropTypes.func.isRequired,
  changeDate: PropTypes.func.isRequired,
  changeFilter: PropTypes.func.isRequired,
  getCalendarUIData: PropTypes.func.isRequired,
  getMonthEvents: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  display: store.calendar.display,
  activeFilter: store.calendar.activeFilter,
  activeDate: store.calendar.activeDate,
  UIData: store.calendar.UIData,
  content: store.calendar.content
});

const mapDispatchToProps = dispatch => ({
  changeDisplay: type => dispatch(changeDisplayAction(type)),
  changeDate: (year, month) => dispatch(changeDateAction(year, month)),
  changeFilter: val => dispatch(changeFilterAction(val)),
  getCalendarUIData: () => dispatch(getCalendarUIDataAction()),
  getMonthEvents: date => dispatch(getMonthEventsAction(date))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Calendar);
