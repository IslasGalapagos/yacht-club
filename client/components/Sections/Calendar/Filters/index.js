import React from 'react';

import ContentHeader from '../../../UIComponents/ContentHeader';
import Years from '../../../UIComponents/Filters/Years';
import Months from '../../../UIComponents/Filters/Months';
import FilterList from '../../../UIComponents/Filters/FilterList';

import './filters.less';

const Filters = props => {
  const displayType = props.display;
  const activeFilter = props.activeFilter;
  const title = props.UIData.title || '';
  const {
    years = [],
    types = [],
    categories = [],
    statuses = [],
    classes = [],
    teams = []
  } = props.UIData.filters;

  function onDisplayChange(e) {
    const value = +e.target.value;

    props.onDisplayChange(value);
  }

  return (
    <div>
      <ContentHeader className="calendar__filters_header">
        <p className="text">{title}</p>
        <ul className="calendar__display_switchers">
          <li className="item">
            <input
              type="radio"
              name="display_switcher"
              id="display_switcher_poster"
              value="1"
              checked={displayType === 1}
              onChange={onDisplayChange}
            />
            <label
              className="calendar__display_switcher -posters"
              htmlFor="display_switcher_poster"
            />
          </li>
          <li className="item">
            <input
              type="radio"
              name="display_switcher"
              id="display_switcher_list"
              value="2"
              checked={displayType === 2}
              onChange={onDisplayChange}
            />
            <label
              className="calendar__display_switcher -list"
              htmlFor="display_switcher_list"
            />
          </li>
          <li className="item">
            <input
              type="radio"
              name="display_switcher"
              id="display_switcher_calendar"
              value="3"
              checked={displayType === 3}
              onChange={onDisplayChange}
            />
            <label
              className="calendar__display_switcher -calendar"
              htmlFor="display_switcher_calendar"
            >
              {new Date().getDate()}
            </label>
          </li>
        </ul>
        <Years
          years={years}
          activeYear={props.date.year}
          onYearChange={props.onYearChange}
        />
      </ContentHeader>
      <div className="calendar_filters">
        <Months
          activeMonth={props.date.month}
          onMonthChange={props.onMonthChange}
        />
        {types.length ? (
          <FilterList
            name="тип"
            inputName="types"
            data={types}
            activeFilter={activeFilter}
            onFilterChange={props.onFilterChange}
          />
        ) : null}
        {categories.length ? (
          <FilterList
            name="категория"
            inputName="categories"
            data={categories}
            activeFilter={activeFilter}
            onFilterChange={props.onFilterChange}
          />
        ) : null}
        {statuses.length ? (
          <FilterList
            name="статус"
            inputName="statuses"
            data={statuses}
            activeFilter={activeFilter}
            onFilterChange={props.onFilterChange}
          />
        ) : null}
        {classes.length ? (
          <FilterList
            name="класс"
            inputName="classes"
            data={classes}
            activeFilter={activeFilter}
            onFilterChange={props.onFilterChange}
          />
        ) : null}
        {teams.length ? (
          <FilterList
            name="команда"
            inputName="teams"
            data={teams}
            activeFilter={activeFilter}
            onFilterChange={props.onFilterChange}
          />
        ) : null}
      </div>
    </div>
  );
};

export default Filters;
