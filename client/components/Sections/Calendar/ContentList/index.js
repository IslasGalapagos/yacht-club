import React from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';

import Share from '../../../UIComponents/Share';

import './content_list.less';

const ContentList = props => {
  const onFilterChange = e => {
    e.preventDefault();

    const newVal = e.target
      .getAttribute('href')
      .substr(1)
      .split('___');
    props.onFilterChange({
      filter: newVal[0],
      filterList: newVal[1]
    });
  };

  const activeFilter = props.activeFilter.filter;

  const events = props.content.map(item => {
    let tags = [];
    const filterListNames = [
      'types',
      'categories',
      'statuses',
      'classes',
      'teams'
    ];

    filterListNames.forEach((filterListName, index) => {
      item[filterListName].forEach(tag => {
        tags.push(
          <li key={tag.slug} className='text -li'>
            {tag.slug !== activeFilter ? (
              <a
                href={`#${tag.slug}___${filterListNames[index]}`}
                onClick={onFilterChange}>
                {tag.name}
              </a>
            ) : (
              <p className='text -li'>{tag.name}</p>
            )}
          </li>
        );
      });
    });

    return (
      <li key={item.title} className='events_list__event'>
        <div className='events_list__share_date'>
          <Share
            data={{
              shrTitle: item.title,
              shrDesc: item.content.brief,
              shrImage: item.cover ? item.cover.url : null
            }}
          />
          <p className='text -li'>
            {moment(item.date.start.string).format('DD.MM.YY')}
          </p>
          <p className='text -li'>
            {moment(item.date.end.string).format('DD.MM.YY')}
          </p>
        </div>
        <div className='events_list__content'>
          <Link to={`/calendar/event/${item.slug}`}>
            <h1 className='title'>{item.title}</h1>
            {item.content.brief.indexOf('<script>') ? (
              <div
                className='text -sb desc'
                dangerouslySetInnerHTML={{__html: item.content.brief}}
              />
            ) : null}
          </Link>
          <ul className='events_list__tag_list'>{tags}</ul>
        </div>
        <Link
          to={`/calendar/event/${item.slug}`}
          className='events_list__cover'>
          {item.cover ? <img src={item.cover.url} alt='' /> : null}
        </Link>
      </li>
    );
  });
  return <div className='events_list'>{events}</div>;
};

export default ContentList;
