import {GET_MAIN_DATA_SUCCESS} from './actions';

const initialState = {
  data: null
};

export default function main(state = initialState, action) {
  switch (action.type) {
    case GET_MAIN_DATA_SUCCESS:
      return {...state, data: action.payload};

    default:
      return state;
  }
}
