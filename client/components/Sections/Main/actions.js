import axiosF from '../../../utils/axios';

export const GET_MAIN_DATA = 'GET_MAIN_DATA';
export const GET_MAIN_DATA_SUCCESS = 'GET_MAIN_DATA_SUCCESS';
export const GET_MAIN_DATA_ERROR = 'GET_MAIN_DATA_ERROR';

export const getMainDataAction = () => dispatch =>
  axiosF('/api/main')
    .then(response => {
      dispatch({
        type: GET_MAIN_DATA_SUCCESS,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
