import React from 'react';
import {Link} from 'react-router-dom';

import './partners_list.less';

const PartnersList = props => {
  const items = props.items.map(item => (
    <li key={item.name} className="main__partners_item">
      <Link to="/club/partners">
        {item.image ? <img src={item.image.url} alt="" /> : null}
      </Link>
    </li>
  ));

  return (
    <div
      className={`main__partners_wrapper ${
        props.className ? props.className : ''
      }`}
    >
      {props.title ? (
        <h1 className="title -section_title">{props.title}</h1>
      ) : null}
      <ul className="main__partners">{items}</ul>
    </div>
  );
};

export default PartnersList;
