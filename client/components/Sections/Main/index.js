import React, {Component} from 'react';
import {connect} from 'react-redux';

import PageContent from '../../Common/PageContent';
import Slideshow from '../../UIComponents/Slideshow';
import NewsList from '../../Sections/Media/NewsList';
import PartnersList from './PartnersList';
import GalleryBlock from '../Media/GalleryBlock';
import Loading from '../../UIComponents/Loading';

import {getMainDataAction} from './actions';
import {getNewsDataAction} from '../../Sections/Media/News/actions';
import {getClubDataAction} from '../Club/actions';

import './main.less';

class Main extends Component {
  componentDidMount() {
    const {mainData, newsData, clubData} = this.props;
    if (mainData === null) {
      this.props.getMainData();
    }

    if (newsData === null) {
      this.props.getNewsData();
    }

    if (clubData === null) {
      this.props.getClubData();
    }
  }

  render() {
    const {mainData, newsData, clubData} = this.props;

    if (mainData === null || newsData === null || clubData === null) {
      return <Loading />;
    }

    const {slideshow = [], gallery} = mainData;
    const {partners} = clubData;

    if (newsData.length) {
      newsData.sort((a, b) => a.date.string < b.date.string);
    }

    return (
      <PageContent className="main">
        {slideshow.length && <Slideshow data={slideshow} />}
        {newsData.length ? <NewsList newsData={[newsData]} /> : null}
        {gallery && (gallery.photos.length || gallery.video.length) ? (
          <GalleryBlock
            data={gallery}
            title="Последние фото и видео"
            withLink
          />
        ) : null}
        {partners.type2.length ? (
          <PartnersList
            className="-partners"
            items={partners.type2}
            title="Партнёры клуба"
          />
        ) : null}
        {partners.type1.length ? (
          <PartnersList items={partners.type1} title="Друзья клуба" />
        ) : null}
      </PageContent>
    );
  }
}

Main.propTypes = {
  mainData: PropTypes.object,
  newsData: PropTypes.array,
  clubData: PropTypes.object,

  getMainData: PropTypes.func.isRequired,
  getNewsData: PropTypes.func.isRequired,
  getClubData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  mainData: store.main.data,
  newsData: store.media.news.newsData,
  clubData: store.club.data
});

const mapDispatchToProps = dispatch => ({
  getMainData: () => dispatch(getMainDataAction()),
  getNewsData: () => dispatch(getNewsDataAction()),
  getClubData: () => dispatch(getClubDataAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
