import React from 'react';

import './team_people.less';

const TeamPeople = props => {
  const $items = props.peopleData.map(item => (
    <li key={item.name} className="team_people__item">
      {item.avatar.url ? (
        <div className="img_wrapper">
          <img src={item.avatar.url} alt="" />
        </div>
      ) : null}
      <h1 className="title name">{item.name}</h1>
      <p className="text -sb role">{item.role}</p>
      <p className="text">{item.desc}</p>
    </li>
  ));

  return <ul className="team_people">{$items}</ul>;
};

export default TeamPeople;
