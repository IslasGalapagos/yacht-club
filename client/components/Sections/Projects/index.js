import React from 'react';

import Router from '../../Router';
import PageContent from '../../Common/PageContent';
import About from './About';

import './projects.less';

const Projects = props => {
  const {
    location: {pathname},
    subRoutes
  } = props;
  const isRoot = pathname === '/projects';

  const Routes = subRoutes.map(route => Router.createRoute(route));

  return (
    <PageContent>
      {isRoot && <About />}
      {Routes}
    </PageContent>
  );
};

export default Projects;
