import React, {Component} from 'react';
import {connect} from 'react-redux';

import ContentHeader from '../../../UIComponents/ContentHeader';
import TeamsList from '../TeamsList';
import NewsList from '../../../Sections/Media/NewsList';
import GalleryBlock from '../../Media/GalleryBlock';
import Loading from '../../../UIComponents/Loading';

import {getTeamsDataAction} from './actions';

import './teams.less';

export class Teams extends Component {
  static loadData(store) {
    return store.dispatch(getTeamsDataAction());
  }

  componentDidMount() {
    if (this.props.data === null) {
      this.props.getTeamsData();
    }
  }

  render() {
    const {data} = this.props;

    if (data === null) {
      return <Loading />;
    }

    const {teams = [], news = [], gallery} = data;

    if (news.length) news.sort((a, b) => a.date.string < b.date.string);

    return (
      <div>
        <ContentHeader>
          <p className="text">Команды Яхт-клуба Санкт-Петербурга</p>
        </ContentHeader>
        <p className="teams__desc">&nbsp;</p>
        {teams.length ? (
          <TeamsList teamsData={teams} />
        ) : (
          <p className="teams__desc">Ни одной команды нет</p>
        )}
        {news.length ? <NewsList newsData={[news]} /> : null}
        {gallery && (gallery.photos.length || gallery.video.length) ? (
          <GalleryBlock data={gallery} withLink />
        ) : null}
      </div>
    );
  }
}

Teams.propTypes = {
  data: PropTypes.object,
  getTeamsData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  data: store.projects.teams.data
});

const mapDispatchToProps = dispatch => ({
  getTeamsData: () => dispatch(getTeamsDataAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Teams);
