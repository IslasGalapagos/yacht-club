import {GET_TEAMS_DATA} from './actions.js';

const initState = {
  data: null
};

export default function teams(state = initState, action) {
  switch (action.type) {
    case GET_TEAMS_DATA:
      return {
        ...state,
        data: action.payload
      };

    default:
      return state;
  }
}
