import axiosF from '../../../../utils/axios';

export const GET_TEAMS_DATA = 'GET_TEAMS_DATA';

export const getTeamsDataAction = () => dispatch =>
  axiosF('/api/teams')
    .then(response => {
      dispatch({
        type: GET_TEAMS_DATA,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
