import React from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';

import Share from '../../../UIComponents/Share';

import cutTitle from '../../../../js/cutTitle.js';

import './regatta_list.less';

const getLocations = locations =>
  locations.map(location => location.location).join(', ');

const getDate = date => {
  if (date.start.month !== date.end.month) {
    return `${moment(date.start.string)
      .locale('ru')
      .format('D MMMM')}-${moment(date.end.string)
      .locale('ru')
      .format('D MMMM')}`;
  }
  return `${moment(date.start.string)
    .locale('ru')
    .format('D')}-${moment(date.end.string)
    .locale('ru')
    .format('D MMMM')}`;
};

const getSeriesDate = dates => {
  let dateIndex = null;
  const now = moment();

  dates.some((item, index) => {
    const start = moment(item.start);
    const end = moment(item.end).add(1, 'days');

    if (now > start && now < end) {
      dateIndex = index;
      return true;
    }

    if (start.diff(start) > 0) {
      dateIndex = index;
      return true;
    }

    dateIndex = index;
    return false;
  });

  return dateIndex;
};

const RegattaList = props => {
  const $items = props.regattaData.map(item => {
    if (!item.events.length && !item.event) {
      return null;
    }

    if (!item.event) {
      const eventsDates = [];
      let events = null;
      events = item.events
        .sort((a, b) => a.date.start.string > b.date.start.string)
        .map(event => {
          if (!event.series.name) {
            return null;
          }

          eventsDates.push({
            start: event.date.start.string,
            end: event.date.end.string
          });

          return (
            <li
              key={event.slug}
              className={`regatta_list__cover ${
                event.meaningful ? '-meaningful' : ''
              }`}
            >
              <Link to={`/calendar/event/${event.slug}`}>
                <div className="img_wrapper">
                  {event.cover ? (
                    <img
                      src={event.cover.url}
                      alt=""
                      className="regatta_list__cover"
                    />
                  ) : null}
                </div>
                <div className="regatta_list__cover_text_block">
                  <h1 className="title">{cutTitle(event.title, 64)}</h1>
                  <p className="text -l">{event.series.phase}</p>
                  {/* <p className="text -l">{getLocations(event.location)}</p> */}
                  <p className="text -l">{getDate(event.date)}</p>
                </div>
              </Link>
            </li>
          );
        });

      if (!eventsDates.length) return null;

      const seriesDateIndex = getSeriesDate(eventsDates);
      const seriesDate = getDate(item.events[seriesDateIndex].date);

      return (
        <li
          key={item.events[seriesDateIndex].title.substr(0, 7)}
          className="regatta_list__item -series"
        >
          <div className="regatta_list__desc_block">
            <div className="left_col">
              <h1 className="title">
                {cutTitle(item.events[seriesDateIndex].title, 64)}
              </h1>
              <p className="date">{seriesDate}</p>
              {/*
                                <p className="text -l tags">{getLocations(item.events[seriesDateIndex].location)} теги</p>
                            */}
            </div>
            <div className="right_col">
              <p className="text -sb desc">{item.text}</p>
              <Share
                data={{
                  shrTitle: item.events[seriesDateIndex].title,
                  shrDesc: item.text,
                  shrImage: item.events[seriesDateIndex].cover
                    ? item.events[seriesDateIndex].cover.url
                    : null
                }}
              />
            </div>
          </div>
          <ul className="regatta_list__series">{events}</ul>
        </li>
      );
    }

    const event = item.event;

    return (
      <li key={event.slug} className="regatta_list__item -event">
        <div
          className={`regatta_list__cover ${
            event.meaningful ? '-meaningful' : ''
          }`}
        >
          <Link to={`/calendar/event/${event.slug}`}>
            <div className="img_wrapper">
              {event.cover ? (
                <img
                  src={event.cover.url}
                  alt=""
                  className="regatta_list__cover"
                />
              ) : null}
            </div>
          </Link>
        </div>
        <div className="regatta_list__desc_block">
          <h1 className="title">{cutTitle(event.title, 64)}</h1>
          <p className="text -sb date">{getDate(event.date)}</p>
          <p className="text -l tags">{getLocations(event.location)} теги</p>
          <p className="text -sb desc">{item.text}</p>
          <div className="share_wrapper">
            <Share
              data={{
                shrTitle: event.title,
                shrDesc: item.text,
                shrImage: event.cover ? event.cover.url : null
              }}
            />
          </div>
        </div>
      </li>
    );
  });

  return <ul className="regatta_list">{$items}</ul>;
};

export default RegattaList;
