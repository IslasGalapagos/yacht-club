import React, {Component} from 'react';
import {connect} from 'react-redux';

import ContentHeader from '../../../UIComponents/ContentHeader';
import Slideshow from '../../../UIComponents/Slideshow';
import TeamPeople from '../TeamPeople';
import TeamEvents from '../TeamEvents';
import NewsList from '../../../Sections/Media/NewsList';
import GalleryBlock from '../../Media/GalleryBlock';
import Loading from '../../../UIComponents/Loading';

import {getTeamDataAction} from './actions';
import {getTeamsDataAction} from '../Teams/actions';

import './team.less';

export class Team extends Component {
  static loadData(store, route) {
    const {dispatch} = store;
    const slug = store
      .getState()
      .entryPage.replace(route.replace(':slug', ''), '');

    return Promise.all([
      dispatch(getTeamsDataAction()),
      dispatch(getTeamDataAction(slug))
    ]);
  }

  componentDidMount() {
    if (this.props.news === null) {
      this.props.getTeamsData();
    }

    const {team} = this.props;
    const {slug} = this.props.match.params;
    if (team === null || team.slug !== slug) {
      this.props.getTeamData(slug);
    }
  }

  render() {
    const {team} = this.props;

    if (team === null || team.slug !== this.props.match.params.slug) {
      return <Loading />;
    }

    const {name, content, logo, slideshow, gallery, people, events} = team;

    const {news = []} = this.props;
    if (news.length) news.sort((a, b) => a.date.string < b.date.string);

    return (
      <div className="team">
        {slideshow.length ? <Slideshow data={slideshow} /> : null}
        <ContentHeader>
          <p className="text">Страница команды {name}</p>
        </ContentHeader>
        <div className="team__content">
          <div className="team__left_col">
            <div className="team__desc text">
              {content.brief.indexOf('<script>') ? (
                <div
                  className="brief"
                  dangerouslySetInnerHTML={{__html: content.brief}}
                />
              ) : null}
              {content.extended.indexOf('<script>') ? (
                <div
                  className="extended"
                  dangerouslySetInnerHTML={{__html: content.extended}}
                />
              ) : null}
            </div>
            <TeamPeople peopleData={people} />
          </div>
          <div className="team__right_col">
            {logo ? <img src={logo.url} alt="" className="team__logo" /> : null}
            <h2 className="text -sb events_title">События команды</h2>
            <TeamEvents eventsData={events} />
          </div>
        </div>
        {news.length ? <NewsList newsData={[news]} /> : null}
        {gallery.photos.length || gallery.video.length ? (
          <GalleryBlock data={gallery} withLink />
        ) : null}
      </div>
    );
  }
}

Team.propTypes = {
  team: PropTypes.object,
  getTeamData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  team: store.projects.team.team,
  news: store.projects.teams.data.news
});

const mapDispatchToProps = dispatch => ({
  getTeamData: slug => dispatch(getTeamDataAction(slug)),
  getTeamsData: () => dispatch(getTeamsDataAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Team);
