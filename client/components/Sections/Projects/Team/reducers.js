import {GET_TEAM_DATA} from './actions.js';

const initState = {
  team: null
};

export default function team(state = initState, action) {
  switch (action.type) {
    case GET_TEAM_DATA:
      return {
        ...state,
        team: action.payload
      };

    default:
      return state;
  }
}
