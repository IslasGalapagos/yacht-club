import axiosF from '../../../../utils/axios';

export const GET_TEAM_DATA = 'GET_TEAM_DATA';

export const getTeamDataAction = slug => dispatch =>
  axiosF('/api/team', {
    data: {teamName: slug}
  })
    .then(response => {
      dispatch({
        type: GET_TEAM_DATA,
        payload: {...response.data, slug}
      });
    })
    .catch(e => {
      console.log(e);
    });
