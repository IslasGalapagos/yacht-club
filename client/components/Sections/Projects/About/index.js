import React, {PureComponent} from 'react';

import ThumbsNav from '../../../Nav/ThumbsNav';

class About extends PureComponent {
  render() {
    return (
      <div className="projects">
        <div className="projects__content">
          <div className="projects__desc_wrapper">
            <h1 className="title -section_title">Проекты</h1>
            <div className="projects__desc_text">
              <p className="text">
                На сегодняшний день деятельность Яхт-клуба Санкт-Петербурга
                охватывает целый ряд магистральных направлений, среди которых
                судостроение, исторические исследования и образовательные
                программы, спортивная подготовка и морская практика для
                молодежи, олимпийский и профессиональный спорт, приобщение
                взрослых к парусу и другие масштабные социальные проекты. Объем
                и масштаб реализуемых проектов позволяет выдвинуть Яхт-клуб
                Санкт-Петербурга в ряд крупнейших клубов в мире.
              </p>
            </div>
          </div>
        </div>
        <ThumbsNav
          title="Подразделы проектов"
          links={[{title: 'Руководство'}]}
        />
      </div>
    );
  }
}

export default About;
