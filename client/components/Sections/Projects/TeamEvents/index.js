import React from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';

import './team_events.less';

// const getLocations = locations => (
//     locations.map(location => (
//         location.location
//     )).join(', ')
// );

const getDate = date => {
  if (date.start.month !== date.end.month) {
    return `${moment(date.start.string)
      .locale('ru')
      .format('D MMMM')}-${moment(date.end.string)
      .locale('ru')
      .format('D MMMM')}`;
  }
  return `${moment(date.start.string)
    .locale('ru')
    .format('D')}-${moment(date.end.string)
    .locale('ru')
    .format('D MMMM')}`;
};

const TeamEvents = props => {
  const $items = props.eventsData.map(item => (
    <li key={item.slug} className="team_events__item">
      <Link to={`/calendar/event/${item.slug}`}>
        <div className="team_events__text_block">
          <h1 className="title name">{item.title}</h1>
          <p className="text">
            {/*
                        {`${getLocations(item.location)} ${getDate(item.date)}`}
                        */}
            {getDate(item.date)}
          </p>
        </div>
        <div className="img_wrapper">
          {item.cover ? <img src={item.cover.url} alt="" /> : null}
        </div>
      </Link>
    </li>
  ));

  return <ul className="team_events">{$items}</ul>;
};

export default TeamEvents;
