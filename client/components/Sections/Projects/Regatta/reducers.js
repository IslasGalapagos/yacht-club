import {GET_REGATTA_DATA} from './actions.js';

const initState = {
  regattaData: null
};

export default function regatta(state = initState, action) {
  switch (action.type) {
    case GET_REGATTA_DATA:
      return {...state, regattaData: action.payload};

    default:
      return state;
  }
}
