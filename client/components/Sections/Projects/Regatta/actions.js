import axiosF from '../../../../utils/axios';

export const GET_REGATTA_DATA = 'GET_REGATTA_DATA';

export const getRegattaDataAction = () => dispatch =>
  axiosF('/api/regatta')
    .then(response => {
      dispatch({
        type: GET_REGATTA_DATA,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
