import React, {Component} from 'react';
import {connect} from 'react-redux';

import ContentHeader from '../../../UIComponents/ContentHeader';
import RegattaList from '../RegattaList';
import Loading from '../../../UIComponents/Loading';

import {getRegattaDataAction} from './actions';

import './regatta.less';

export class Regatta extends Component {
  static loadData(store) {
    return store.dispatch(getRegattaDataAction());
  }

  componentDidMount() {
    if (this.props.regattaData === null) {
      this.props.getRegattaData();
    }
  }

  render() {
    const {regattaData} = this.props;

    if (regattaData === null) {
      return <Loading />;
    }

    return (
      <div>
        <ContentHeader>
          <p className="text">Регаты Яхт-клуба Санкт-Петербурга</p>
          <p className="text">Мероприятия</p>
        </ContentHeader>
        {regattaData.length ? (
          <RegattaList regattaData={regattaData} />
        ) : (
          <p className="text">Ни одной регаты нет</p>
        )}
      </div>
    );
  }
}

Regatta.propTypes = {
  regattaData: PropTypes.array,
  getRegattaData: PropTypes.func.isRequired
};

const mapStateToProps = store => ({
  regattaData: store.projects.regatta.regattaData
});

const mapDispatchToProps = dispatch => ({
  getRegattaData: () => dispatch(getRegattaDataAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Regatta);
