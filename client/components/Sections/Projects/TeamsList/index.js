import React from 'react';
import {Link} from 'react-router-dom';

import './teams_list.less';

const TeamsList = props => {
  const $items = props.teamsData.map(item => (
    <li key={item.slug} className="teams_list__item">
      <Link
        to={`/projects/team/${item.slug}`}
        className="teams_list__logo_link"
      >
        <div className="img_wrapper">
          {item.logo ? <img src={item.logo.url} alt="" /> : null}
        </div>
      </Link>
      <div className="teams_list__desc">
        <h1 className="title">{item.name}</h1>
        <p className="text -li class">
          {item.class.reduce(
            (classes, teamClass, index) =>
              `${classes}${index ? ', ' : ''}${teamClass.name}`,
            ''
          )}
        </p>
        {item.content.brief.indexOf('<script>') ? (
          <Link to={`/projects/team/${item.slug}`}>
            <div
              className="text -sb desc"
              dangerouslySetInnerHTML={{__html: item.content.brief}}
            />
          </Link>
        ) : null}
      </div>
      <Link
        to={`/projects/team/${item.slug}`}
        className="teams_list__cover_link"
      >
        {item.cover ? <img src={item.cover.url} alt="" /> : null}
      </Link>
    </li>
  ));

  return <ul className="teams_list">{$items}</ul>;
};

export default TeamsList;
