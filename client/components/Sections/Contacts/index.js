import React from 'react';

import PageContent from '../../Common/PageContent';
import ContactsMap from './ContactsMap';
import ContactsBlock from './ContactsBlock';

import './contacts.less';

const Contacts = () => (
  <PageContent>
    {/* <ContactsMap /> */}
    <div className='contacts'>
      <div className='contacts__address'>
        <h1 className='title'>Полный адрес:</h1>
        <p className='text -li'>
          197227 Санкт-Петербург
          <br />
          Приморский район, пос. Лахта
          <br />
          ул. Береговая, дом 19, лит. А
        </p>
      </div>
      <div className='contacts__route'>
        <h1 className='title'>Как проехать:</h1>
        <p className='text -li'>
          По Приморскому шоссе после развилки проехать вперед 300 м и повернуть
          налево следуя указателям жёлтого цвета «Яхтенный порт «Геркулес» (см.
          схему)
        </p>
      </div>
    </div>
    <ContactsBlock />
  </PageContent>
);

export default Contacts;
