import React from 'react';
import {Link} from 'react-router-dom';

import './contacts_block.less';

const ContactsBlock = () => {
  return (
    <div className="contacts_block">
      <h1 className="title">Контакты</h1>
      <div className="contacts_block__col">
        <h4 className="text -title">Администратор клуба</h4>
        <p className="text -li -desc">&nbsp;</p>
        <p className="text -li">
          тел.: <span className="text -b">+7 (812) 999 9999</span>
        </p>
        <p className="text -li">
          e-mail: <Link to="mailto:info@yc.ru">info@yc.ru</Link>
        </p>
      </div>
      <div className="contacts_block__col">
        <h4 className="text -title">Капитания</h4>
        <p className="text -li -desc">По вопросам стоянки яхт</p>
        <p className="text -li">
          тел.: <span className="text -b">+7 (812) 999 9999</span>
        </p>
        <p className="text -li">
          e-mail: <Link to="mailto:capitania@yc.ru">capitania@yc.ru</Link>
        </p>
      </div>
      <div className="contacts_block__col">
        <h4 className="text -title">Пресс-служба клуба</h4>
        <p className="text -li -desc">&nbsp;</p>
        <p className="text -li">
          тел.: <span className="text -b">+7 (812) 999 9999</span>
        </p>
        <p className="text -li">
          e-mail: <Link to="mailto:press@yc.ru">press@yc.ru</Link>
        </p>
      </div>
    </div>
  );
};

export default ContactsBlock;
