import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

import mapStyles from './mapStyles.js';

const GoogleMapWrapper = withGoogleMap((props) => {
    return (
        <GoogleMap
            defaultOptions={{
                styles: mapStyles,
                scrollwheel: false,
            }}
            mapTypeControl={false}
            defaultZoom={15}
            defaultCenter={props.center}
        >
            {props.markers.map(marker => (
                <Marker{...marker} />
            ))}
        </GoogleMap>
    );
});

export class ContactsMap extends Component {
    constructor(props) {
        super(props);

        this.state = {
            markers: [{
                key: 'Яхтенный порт "Геркулес"',
                defaultAnimation: 2,
                icon: require('../../../../images/footer/map_icon.svg'),
            }],
        };
    }

    render() {
        const { addressClub } = this.props;

        if (!addressClub.length) {
            return null;
        }

        const geo = {
            lat: addressClub[1],
            lng: addressClub[0],
        };

        this.state.markers[0].position = geo;

        return (
            <div style={{ height: 580 }}>
                <GoogleMapWrapper
                    containerElement={
                        <div style={{ height: 580, width: '100%' }} />
                    }
                    mapElement={
                        <div style={{ height: '100%' }} />
                    }
                    center={geo}
                    markers={this.state.markers}
                />
            </div>
        );
    }
}

ContactsMap.propTypes = {
    addressClub: PropTypes.array.isRequired,
};

function mapStateToProps(store) {
    return {
        addressClub: store.app.addressClub,
    };
}

export default connect(mapStateToProps)(ContactsMap);
