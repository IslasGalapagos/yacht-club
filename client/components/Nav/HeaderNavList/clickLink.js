export default function clickLink(e) {
    const link = e.target;

    const nav = link.closest('.js-header-nav');
    nav.classList.remove('-opened_subnav');
}
