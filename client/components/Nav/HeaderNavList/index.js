import React from 'react';
import {NavLink} from 'react-router-dom';

import {mouseOver, mouseOut} from './subnavSwitcher';
import clickLink from './clickLink';

import './header_nav.less';

const HeaderNavList = props => {
  const itemsData = props.data;
  let maxSubnavItems = 0;
  const items = props.urls.map((navUrl, navUrlIndex) => {
    const itemData = itemsData[navUrlIndex];

    let subnavItems = null;
    if (itemData.subnav) {
      const subnavUrls = itemData.subnav.urls;

      if (subnavUrls.length > maxSubnavItems) {
        maxSubnavItems = subnavUrls.length;
      }

      subnavItems = subnavUrls.map((subnavUrl, subnavIndex) => {
        const subnavName = itemData.subnav.data[subnavIndex].name;
        const isDisabled =
          itemData.subnav.disabled && itemData.subnav.disabled === subnavIndex;
        const subnavSlug = subnavUrl.replace('/', '');

        return (
          <li key={subnavUrl} className="header_nav__subnav_item">
            <NavLink
              className={`${
                isDisabled ? `-disabled js-subnav-${subnavSlug}` : ''
              }`}
              to={`${navUrl}${subnavUrl}`}
              title={subnavName}
              onClick={clickLink}
              activeClassName="-active"
            >
              {subnavName}
            </NavLink>
          </li>
        );
      });
    }

    return (
      <div key={navUrl} className="header_nav__item">
        <NavLink
          to={navUrl}
          title={itemData.name}
          onClick={clickLink}
          activeClassName="-active"
        >
          {itemData.name}
        </NavLink>
        {subnavItems ? (
          <ul className={`header_nav__subnav -align_${itemData.align}`}>
            {subnavItems}
          </ul>
        ) : null}
      </div>
    );
  });

  return (
    <nav
      className={`header_nav js-header-nav -subnavitems_${maxSubnavItems}`}
      onMouseOver={mouseOver}
      onMouseOut={mouseOut}
    >
      {items}
    </nav>
  );
};

export default HeaderNavList;
