let currentElem = null;

export function mouseOver(e) {
    if (currentElem) return;

    let target = e.target;
    while (target !== this) {
        if (target.tagName === 'NAV') break;
        target = target.parentNode;
    }
    if (target === this) return;

    currentElem = target;

    target.classList.add('-opened_subnav');
}

export function mouseOut(e) {
    if (!currentElem) return;

    let relatedTarget = e.relatedTarget;
    if (relatedTarget) {
        while (relatedTarget) {
            if (relatedTarget === currentElem) return;
            relatedTarget = relatedTarget.parentNode;
        }
    }

    currentElem.classList.remove('-opened_subnav');

    currentElem = null;
}
