import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import './thumbs_nav.less';
import subnav from '../HeaderNav/reducers';

class ThumbsNav extends Component {
  constructor(props) {
    super(props);

    this.getThumbsData = this.getThumbsData.bind(this);
  }

  getThumbsData(currPathname) {
    const thumbsData = this.props.thumbsData;

    if (!thumbsData) return {};

    const currSectionSubnav = thumbsData[currPathname];

    if (!currSectionSubnav) return {};

    return currSectionSubnav;
  }

  render() {
    const currPathname =
      typeof window !== 'undefined'
        ? window.location.pathname
        : this.props.entryPage;
    const {urls, data, disabled} = this.getThumbsData(currPathname);

    if (!urls) return null;

    const thumbs = urls.map((url, urlIndex) => (
      <li
        key={url}
        className={`thumbs_nav__item ${
          urlIndex === disabled ? '-disabled' : ''
        }`}
        title={`${urlIndex === disabled ? 'Ссылка недоступна' : ''}`}
      >
        {urlIndex !== disabled ? (
          <Link to={`${currPathname}${url}`}>
            <div className="img_wrapper">
              <img src={data[urlIndex].cover} alt="" />
            </div>
            <p className="cover-title">{data[urlIndex].name}</p>
          </Link>
        ) : (
          <div className="">
            <div className="img_wrapper">
              <img src={data[urlIndex].cover} alt="" />
            </div>
            <p className="cover-title">{data[urlIndex].name}</p>
          </div>
        )}
      </li>
    ));

    return <ul className="thumbs_nav">{thumbs}</ul>;
  }
}

ThumbsNav.propTypes = {
  thumbsData: PropTypes.object.isRequired
};

function mapStateToProps(store) {
  return {
    entryPage: store.entryPage,
    thumbsData: store.nav.subnavData
  };
}

export default connect(mapStateToProps)(ThumbsNav);
