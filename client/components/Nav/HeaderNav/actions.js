import axiosF from '../../../utils/axios';

export const GET_NAV_DATA = 'GET_NAV_DATA';
export const GET_NAV_DATA_SUCCESS = 'GET_NAV_DATA_SUCCESS';
export const GET_NAV_DATA_ERROR = 'GET_NAV_DATA_ERROR';

export const getNavDataAction = () => dispatch =>
  axiosF('/api/nav')
    .then(response => {
      dispatch({
        type: GET_NAV_DATA_SUCCESS,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
