import React, {Component} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';

import HeaderNavList from '../HeaderNavList';

import getAllNavData from './getAllNavData';

const navLinks = {
  urls: ['/club', '/calendar', '/projects', '/media', '/contacts'],
  data: [
    {
      name: 'Клуб',
      align: 'left'
    },
    {
      name: 'Календарь',
      align: null
    },
    {
      name: 'Проекты',
      align: 'right'
    },
    {
      name: 'Медиа',
      align: 'left'
    },
    {
      name: 'Контакты',
      align: null
    }
  ]
};

class HeaderNav extends Component {
  render() {
    const {subnavData, customPagesData} = this.props;

    if (!subnavData) return null;

    const {urls, data} = getAllNavData(navLinks, subnavData, customPagesData);

    return <HeaderNavList urls={urls} data={data} />;
  }
}

HeaderNav.propTypes = {
  subnavData: PropTypes.object.isRequired,
  customPagesData: PropTypes.object.isRequired
};

function mapStateToProps(store) {
  return {
    subnavData: store.nav.subnavData,
    customPagesData: store.nav.customPagesData
  };
}

export default compose(
  withRouter,
  connect(mapStateToProps)
)(HeaderNav);
