const getSubnavLinks = (navData, subnavData, customPagesData) => {
  for (let n = 0; n < navData.urls.length; n += 1) {
    const navUrl = navData.urls[n];

    if (subnavData[navUrl]) {
      navData.data[n].subnav = subnavData[navUrl];
    }

    if (customPagesData.subnav[navUrl]) {
      if (!navData.data[n].subnav) {
        navData.data[n].subnav = {
          urls: [],
          data: [],
          disabled: null
        };
      }

      const navSubnav = navData.data[n].subnav;
      const cpSubnav = customPagesData.subnav[navUrl];
      cpSubnav.urls.forEach((cpSubnavItem, cpSubnavIndx) => {
        navSubnav.urls.push(cpSubnavItem);
        navSubnav.data.push(cpSubnav.data[cpSubnavIndx]);
      });
    }

    if (customPagesData.nav[navUrl]) {
      const cpNavs = customPagesData.nav[navUrl];

      cpNavs.urls.forEach((cpNav, cpNavIndx) => {
        navData.urls.splice(n + cpNavIndx, 0, cpNav);
        navData.data.splice(n + cpNavIndx, 0, cpNavs.data[cpNavIndx]);
      });

      n += cpNavs.urls.length;
    }
  }

  return navData;
};

export default getSubnavLinks;
