import {
    GET_NAV_DATA_SUCCESS,
} from './actions';

const initialState = {
    subnavData: {},
    customPagesData: {
        nav: {},
        subnav: {},
    },
};

export default function subnav(state = initialState, action) {
    switch (action.type) {
    case GET_NAV_DATA_SUCCESS:
        return {
            ...state,
            subnavData: action.payload.subnav,
            customPagesData: action.payload.customPages,
        };

    default:
        return state;
    }
}
