import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import NotFound from './404';

import routes from '../routes';

class Router extends Component {
  static createRoute(route, componentProps = {}) {
    const props = route.props || {};
    const RouteComponent = route.component;

    return (
      <Route
        key={route.path}
        path={route.path}
        {...props}
        render={props => (
          <RouteComponent
            subRoutes={route.children}
            {...props}
            {...componentProps}
          />
        )}
      />
    );
  }

  render() {
    return (
      <Switch>
        {routes.map(route => Router.createRoute(route))}
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default Router;
