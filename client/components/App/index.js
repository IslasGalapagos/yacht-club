import React, {Component, StrictMode} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';

import Head from './Head';
import Header from '../Common/Header';
import Footer from '../Common/Footer';
import Router from '../Router';

import {getClubAddressAction, getSocialAction} from './actions';
import {getNavDataAction} from '../Nav/HeaderNav/actions';

export class App extends Component {
  static loadData(store) {
    const {dispatch} = store;

    return Promise.all([
      dispatch(getClubAddressAction()),
      dispatch(getSocialAction()),
      dispatch(getNavDataAction())
    ]);
  }

  render() {
    return (
      <StrictMode>
        <div className='common_wrapper'>
          <Head />
          <Header />
          <Router />
          <Footer />
        </div>
      </StrictMode>
    );
  }
}

const mapStateToProps = store => ({
  store
});

export default compose(
  withRouter,
  connect(mapStateToProps)
)(App);
