import React, {PureComponent} from 'react';
import Helmet from 'react-helmet';

class Head extends PureComponent {
  render() {
    return (
      <Helmet>
        <title>Яхт-клуб</title>
        <meta charset='utf-8' />
        <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        {/* <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRdZ_EdpIVmkRHXZ5zNv60PRuFzk6ecrU" />
        <script src="https://www.google.com/recaptcha/api.js?render=explicit" /> */}
      </Helmet>
    );
  }
}

export default Head;
