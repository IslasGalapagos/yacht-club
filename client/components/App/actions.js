import axiosF from '../../utils/axios';

export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
export const GET_CLUB_ADDRESS_DATA = 'GET_CLUB_ADDRESS_DATA';
export const GET_CLUB_ADDRESS_DATA_SUCCESS = 'GET_CLUB_ADDRESS_DATA_SUCCESS';
export const GET_CLUB_ADDRESS_DATA_ERROR = 'GET_CLUB_ADDRESS_DATA_ERROR';
export const GET_SOCIAL_DATA = 'GET_SOCIAL_DATA';
export const GET_SOCIAL_DATA_SUCCESS = 'GET_SOCIAL_DATA_SUCCESS';
export const GET_SOCIAL_DATA_ERROR = 'GET_SOCIAL_DATA_ERROR';

export const loaderSwitcherAction = switcher => {
  return {
    type: switcher ? SHOW_LOADER : HIDE_LOADER
  };
};

export const getClubAddressAction = () => dispatch =>
  axiosF('/api/club-address')
    .then(response => {
      dispatch({
        type: GET_CLUB_ADDRESS_DATA_SUCCESS,
        payload: response.data || []
      });
    })
    .catch(e => {
      console.log(e);
    });

export const getSocialAction = () => dispatch =>
  axiosF('/api/social')
    .then(response => {
      dispatch({
        type: GET_SOCIAL_DATA_SUCCESS,
        payload: response.data
      });
    })
    .catch(e => {
      console.log(e);
    });
