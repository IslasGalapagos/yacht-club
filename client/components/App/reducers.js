import {
    SHOW_LOADER,
    HIDE_LOADER,
    GET_CLUB_ADDRESS_DATA_SUCCESS,
    GET_SOCIAL_DATA_SUCCESS,
} from './actions';

const initialState = {
    loader: true,
    addressClub: [],
    social: [],
};

export default function subnav(state = initialState, action) {
    switch (action.type) {
    case GET_CLUB_ADDRESS_DATA_SUCCESS:
        return { ...state, addressClub: action.payload };

    case GET_SOCIAL_DATA_SUCCESS:
        return { ...state, social: action.payload };

    case SHOW_LOADER:
        return { ...state, loader: true };

    case HIDE_LOADER:
        return { ...state, loader: false };

    default:
        return state;
    }
}
