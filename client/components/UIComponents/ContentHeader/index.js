import React from 'react';

import './content_header.less';

const ContentHeader = props => {
  if (props.children.length > 1) {
    const wrappedItems = props.children.map((item, index) => {
      return (
        <div key={index} className="content_header__item">
          {item}
        </div>
      );
    });
    return (
      <div className={`content_header ${props.className || ''}`}>
        {wrappedItems}
      </div>
    );
  }

  return (
    <div className={`content_header ${props.className || ''}`}>
      <div className="content_header__item">{props.children}</div>
    </div>
  );
};

export default ContentHeader;
