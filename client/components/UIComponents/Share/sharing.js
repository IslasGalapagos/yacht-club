const shareMethod = {
  fb: params => {
    const apiUrl = `http://www.facebook.com/sharer.php?u=${params.url}&title=${
      params.title
    }&description=${params.desc}&picture=${params.image}`;
    shareMethod.openShareWindow(apiUrl);
  },
  vk: params => {
    const apiUrl = `http://vkontakte.ru/share.php?url=${params.url}&title=${
      params.title
    }&description=${params.desc}&image=${params.image}`;
    shareMethod.openShareWindow(apiUrl);
  },
  twitter: params => {
    const descLength = 140 - params.url.length - 2;

    params.desc = `${params.title} ${params.desc}`;
    if (params.desc.length > descLength) {
      params.desc = `${params.desc.substr(0, descLength)}…`;
    }

    const apiUrl = `https://twitter.com/intent/tweet?url=${params.url}&text=${
      params.desc
    }&image=${params.image}`;
    shareMethod.openShareWindow(apiUrl);
  },

  openShareWindow: url => {
    if (typeof window !== 'undefined') {
      window.open(url);
    }
  }
};

const sharing = data => {
  if (!data) return null;

  let {shrUrl, shrTitle, shrDesc, shrImage} = data;

  if (!shrUrl && typeof window !== 'undefined') {
    shrUrl = window.location.href;
  }
  if (!shrTitle) shrTitle = 'Яхт-клуб';
  if (!shrDesc)
    shrDesc =
      'Санкт-Петербургская общественная спортивная организация Яхт-клуб Санкт-Петербурга создана в 2007 году и является преемником старейшего яхт-клуба мира – Невского флота, задуманного Петром Великим в начале XVIII века.\nПреумножая славные традиции российского флота и активно участвуя в международной парусной жизни, Яхт-клуб Санкт-Петербурга сегодня является одним из крупнейших в мире.';
  if (!shrImage)
    shrImage =
      'http://res.cloudinary.com/islas-online/image/upload/v1478758751/club_symbols_pennant/bvbeumjtnt7fyee4qpe0.png';

  return e => {
    const social = e.target.dataset.social;

    if (shareMethod[social]) {
      shareMethod[social]({
        url: shrUrl,
        title: shrTitle,
        desc: shrDesc,
        image: shrImage
      });
    }
  };
};

export default sharing;
