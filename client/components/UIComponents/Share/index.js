import React from 'react';

import sharing from './sharing';

import './share.less';

const Share = props => {
  const sharingData = props.data;

  return (
    <ul className="share">
      <li className="share__item -fb">
        <button
          className="share__button"
          type="button"
          onClick={sharing(sharingData)}
          data-social="fb"
        />
      </li>
      <li className="share__item -vk">
        <button
          className="share__button"
          type="button"
          onClick={sharing(sharingData)}
          data-social="vk"
        />
      </li>
      <li className="share__item -twitter">
        <button
          className="share__button"
          type="button"
          onClick={sharing(sharingData)}
          data-social="twitter"
        />
      </li>
    </ul>
  );
};

export default Share;
