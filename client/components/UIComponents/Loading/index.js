import React from 'react';

import PageContent from '../../Common/PageContent';

import './loading.less';

const Loading = props => {
  let content;

  if (props.error) {
    content = <button onClick={props.retry}>Retry</button>;
  } else if (props.pastDelay) {
    constent = <p>...Загрузка</p>;
  } else if (props.timedOut) {
    content = <button onClick={props.retry}>Retry</button>;
  }

  return <PageContent className="-loading">{content}</PageContent>;
};

export default Loading;
