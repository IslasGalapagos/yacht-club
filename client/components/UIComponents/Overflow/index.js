import React from 'react';

import './overflow.less';

const Overflow = (props) => {
    return (
        <div className={`overflow js-overflow ${props.showed ? '-visible' : ''}`}>
            <div className="overflow__inner">
                <div className="overflow__content">
                    {props.children}
                </div>
            </div>
        </div>
    );
};

export default Overflow;
