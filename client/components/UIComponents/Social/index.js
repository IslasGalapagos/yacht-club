import React, {Component} from 'react';
import {connect} from 'react-redux';

import './social.less';

class Social extends Component {
  render() {
    const {social} = this.props;

    if (!social) return null;

    const items = social.map(item => {
      if (!item.url) return null;

      return (
        <li key={item.name} className="social__item">
          <a href={item.url} target="_blank">
            <div className={`icon -${item.name}`} />
          </a>
        </li>
      );
    });

    return <ul className="social">{items}</ul>;
  }
}

Social.propTypes = {
  social: PropTypes.array.isRequired
};

function mapStateToProps(store) {
  return {
    social: store.app.social
  };
}

export default connect(mapStateToProps)(Social);
