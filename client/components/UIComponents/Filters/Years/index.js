import React from 'react';

import './years.less';

const Years = props => {
  const onYearChange = e => {
    const targetValue = +e.target.value;

    if (targetValue !== props.activeYear) {
      props.onYearChange(targetValue);
    }
  };

  const items = props.years.map(item => {
    const isActiveYear = +item === props.activeYear;

    return (
      <li key={item} className="item calendar_years__item">
        <input
          type="radio"
          name="calendar_year"
          id={`year_${item}`}
          value={item}
          checked={isActiveYear}
          onChange={onYearChange}
        />
        <label htmlFor={`year_${item}`}>{item}</label>
      </li>
    );
  });

  return <ul className="text_list">{items}</ul>;
};

export default Years;
