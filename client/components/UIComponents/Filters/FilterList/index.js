import React from 'react';

import './filter_list.less';

const FilterList = props => {
  const activeFilter = props.activeFilter.filter;

  const onFilterChange = e => {
    const newVal =
      e.target.value !== activeFilter
        ? {
            filter: e.target.value,
            filterList: e.target.name
          }
        : {
            filter: '',
            filterList: ''
          };

    props.onFilterChange(newVal);
  };

  let isActiveFilterList = '';

  const items = props.data.map(item => {
    const isActiveFilter = item.slug === activeFilter;
    if (isActiveFilter && !isActiveFilterList) {
      isActiveFilterList = true;
    }

    return (
      <li key={item.slug} className="calendar_filters__item">
        <input
          type="radio"
          name={props.inputName}
          id={item.slug}
          value={item.slug}
          checked={isActiveFilter}
          onChange={onFilterChange}
        />
        <label htmlFor={item.slug}>{item.name.toLowerCase()}</label>
      </li>
    );
  });
  return (
    <ul className="calendar_filters__list" title={`${props.name}:`}>
      {isActiveFilterList ? (
        <li className="calendar_filters__item -reset">
          <input
            type="radio"
            name={props.inputName}
            id={`${props.inputName}_reset`}
            value=""
            onChange={onFilterChange}
          />
          <label htmlFor={`${props.inputName}_reset`} />
        </li>
      ) : (
        ''
      )}
      {items}
    </ul>
  );
};

export default FilterList;
