import React from 'react';

const Months = props => {
  const items = [];
  const months = [
    'январь',
    'февраль',
    'март',
    'апрель',
    'май',
    'июнь',
    'июль',
    'август',
    'сентябрь',
    'октябрь',
    'ноябрь',
    'декабрь'
  ];
  const enableMonths = props.enableMonths;

  const onMonthChange = e => {
    const targetValue = +e.target.value;

    if (targetValue !== props.activeMonth) {
      // props.onMonthChange(undefined, targetValue); // First param - year
      props.onMonthChange(targetValue);
    }
  };

  for (let i = 0; i < 12; i += 1) {
    const isActiveMonth = i + 1 === props.activeMonth;
    const isDisabledMonth = enableMonths && !(enableMonths.indexOf(i + 1) >= 0);

    items.push(
      <li key={`month_${i}`} className="calendar_filters__item">
        <input
          type="radio"
          name="calendar_month"
          id={`month_${i}`}
          value={i + 1}
          checked={!isDisabledMonth && isActiveMonth}
          onChange={onMonthChange}
          disabled={isDisabledMonth}
        />
        <label htmlFor={`month_${i}`}>{months[i]}</label>
      </li>
    );
  }

  return (
    <ul className="calendar_filters__list" title="месяц:">
      {items}
    </ul>
  );
};

export default Months;
