import React from 'react';
import {Link} from 'react-router-dom';

import Overflow from '../Overflow';

import './popup.less';

const hidePopup = e => {
  const overflow = e.target.closest('.js-overflow');

  overflow.classList.remove('-visible');
};

const Popup = props => {
  const {closeLink, closeCb} = props;
  return (
    <Overflow showed={props.showed}>
      <div className={`popup ${props.className}`}>
        {closeLink ? (
          <Link to={closeLink} className="popup__close" />
        ) : (
          <button className="popup__close" onClick={closeCb || hidePopup} />
        )}
        {props.children}
      </div>
    </Overflow>
  );
};

export default Popup;
