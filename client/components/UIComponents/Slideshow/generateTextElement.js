import React from "react";
import { Link } from "react-router-dom";

const generateTextElement = textObj => {
  if (!textObj) return null;

  let textEl = (
    <h1
      className={`slideshow__title -${textObj.align}`}
      style={{ color: textObj.color }}
    >
      {textObj.text}
    </h1>
  );

  if (!textObj.url) return textEl;

  if (textObj.url && typeof window !== "undefined".canUseDOM) {
    const appHost = window.location.host;
    const indexOfHost = textObj.url.indexOf(appHost) > 0;

    textEl = (
      <Link to={textObj.url} target={`${indexOfHost ? "" : "_blank"}`}>
        {textEl}
      </Link>
    );
  }

  return textEl;
};

export default generateTextElement;
