import React from 'react';
import Slider from 'react-slick';

import generateTextElement from './generateTextElement';

import './slideshow.less';
import 'slick-carousel/slick/slick.less';

const Slideshow = props => {
  const slides = props.data.map(slide => {
    const text = generateTextElement(slide.text);

    return (
      <div
        key={slide.image.substr(0, 7)}
        className="slideshow__slide img_wrapper"
      >
        {text}
        <img src={slide.image} alt="slide" />
      </div>
    );
  });

  const settings = {
    arrows: false,
    autoplay: true,
    className: 'slideshow',
    dots: true,
    dotsClass: 'slideshow__bullets',
    infinite: true,
    pauseOnHover: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipe: true,
    touchMove: true,
    useCSS: true
  };

  return <Slider {...settings}>{slides}</Slider>;
};

export default Slideshow;
