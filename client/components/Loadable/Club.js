import React, {Component} from 'react';

import Loadable from '../HOCs/Loadable';

import {getClubDataAction} from '../Sections/Club/actions';

const LoadableComponent = Loadable({
  loader: () => import('../Sections/Club'),
  webpack: () => [require.resolveWeak('../Sections/Club')],
  modules: ['../Sections/Club']
});

export default class LoadableClub extends Component {
  static loadData(store) {
    return store.dispatch(getClubDataAction());
  }

  render() {
    return <LoadableComponent {...this.props} />;
  }
}
