import React, {Component} from 'react';

import Loadable from '../HOCs/Loadable';

import {getEventDataAction} from '../Sections/Calendar/Event/actions';

const LoadableComponent = Loadable({
  loader: () => import('../Sections/Calendar/Event'),
  webpack: () => [require.resolveWeak('../Sections/Calendar/Event')],
  modules: ['../Sections/Calendar/Event']
});

export default class LoadableEvent extends Component {
  static loadData(store, route) {
    const slug = store
      .getState()
      .entryPage.replace(route.replace(':slug', ''), '');

    return store.dispatch(getEventDataAction(slug));
  }

  render() {
    return <LoadableComponent {...this.props} />;
  }
}
