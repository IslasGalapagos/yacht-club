import React, {Component} from 'react';

import Loadable from '../HOCs/Loadable';

import {getMainDataAction} from '../Sections/Main/actions';
import {getNewsDataAction} from '../Sections/Media/News/actions';
import {getClubDataAction} from '../Sections/Club/actions';

const LoadableComponent = Loadable({
  loader: () => import('../Sections/Main'),
  webpack: () => [require.resolveWeak('../Sections/Main')],
  modules: ['../Sections/Main']
});

export default class LoadableMain extends Component {
  static loadData(store) {
    const {dispatch} = store;

    return Promise.all([
      dispatch(getMainDataAction()),
      dispatch(getNewsDataAction()),
      dispatch(getClubDataAction())
    ]);
  }

  render() {
    return <LoadableComponent {...this.props} />;
  }
}
