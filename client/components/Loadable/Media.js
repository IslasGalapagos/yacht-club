import React, {Component} from 'react';

import Loadable from '../HOCs/Loadable';

const LoadableComponent = Loadable({
  loader: () => import('../Sections/Media'),
  webpack: () => [require.resolveWeak('../Sections/Media')],
  modules: ['../Sections/Media']
});

export default class LoadableMedia extends Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
