import React, {Component} from 'react';

import Loadable from '../HOCs/Loadable';

const LoadableComponent = Loadable({
  loader: () => import('../Sections/Projects'),
  webpack: () => [require.resolveWeak('../Sections/Projects')],
  modules: ['../Sections/Projects']
});

export default class LoadableProjects extends Component {
  render() {
    return <LoadableComponent {...this.props} />;
  }
}
