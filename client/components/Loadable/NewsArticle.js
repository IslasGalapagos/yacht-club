import React, {Component} from 'react';

import Loadable from '../HOCs/Loadable';

import {getNewsArticleDataAction} from '../Sections/Media/NewsArticle/actions';

const LoadableComponent = Loadable({
  loader: () => import('../Sections/Media/NewsArticle'),
  webpack: () => [require.resolveWeak('../Sections/Media/NewsArticle')],
  modules: ['../Sections/Media/NewsArticle']
});

export default class LoadableNewsArticle extends Component {
  static loadData(store, route) {
    const {dispatch} = store;
    const slug = store
      .getState()
      .entryPage.replace(route.replace(':slug', ''), '');

    return Promise.all([dispatch(getNewsArticleDataAction(slug))]);
  }

  render() {
    return <LoadableComponent {...this.props} />;
  }
}
