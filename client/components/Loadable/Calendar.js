import React, {Component} from 'react';

import Loadable from '../HOCs/Loadable';

import {
  getCalendarUIDataAction,
  getMonthEventsAction
} from '../Sections/Calendar/actions';

const LoadableComponent = Loadable({
  loader: () => import('../Sections/Calendar'),
  webpack: () => [require.resolveWeak('../Sections/Calendar')],
  modules: ['../Sections/Calendar']
});

export default class LoadableCalendar extends Component {
  static loadData(store) {
    const {dispatch} = store;
    const {activeDate} = store.getState().calendar;

    return Promise.all([
      dispatch(getCalendarUIDataAction()),
      dispatch(getMonthEventsAction(activeDate))
    ]);
  }

  render() {
    return <LoadableComponent {...this.props} />;
  }
}
