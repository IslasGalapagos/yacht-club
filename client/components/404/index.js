import React from 'react';

import PageContent from '../Common/PageContent';

import './404.less';

const NotFound = () => (
  <div>
    <PageContent>
      <h1 className="fourohfour__title">Page Not Found</h1>
    </PageContent>
  </div>
);

export default NotFound;
