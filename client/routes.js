import LoadableMain from './components/Loadable/Main';
import LoadableClub from './components/Loadable/Club';
import Leadership from './components/Sections/Club/Leadership';
import Members from './components/Sections/Club/Members';
import Statute from './components/Sections/Club/Statute';
import Symbols from './components/Sections/Club/Symbols';
import Partners from './components/Sections/Club/Partners';
import LoadableCalendar from './components/Loadable/Calendar';
import LoadableEvent from './components/Loadable/Event';
import LoadableProjects from './components/Loadable/Projects';
import Team from './components/Sections/Projects/Team';
import Teams from './components/Sections/Projects/Teams';
import Regatta from './components/Sections/Projects/Regatta';
import LoadableMedia from './components/Loadable/Media';
import News from './components/Sections/Media/News';
import LoadableNewsArticle from './components/Loadable/NewsArticle';
import Gallery from './components/Sections/Media/Gallery';
import PressCentre from './components/Sections/Media/PressCentre';
import Contacts from './components/Sections/Contacts';
import CustomPage from './components/Sections/CustomPage';

export default [
  {
    path: '/',
    component: LoadableMain,
    props: {
      exact: true
    }
  },
  {
    path: '/club',
    component: LoadableClub,
    children: [
      {
        path: '/club/leadership',
        component: Leadership
      },
      {
        path: '/club/members',
        component: Members
      },
      {
        path: '/club/statute',
        component: Statute
      },
      {
        path: '/club/symbols',
        component: Symbols
      },
      {
        path: '/club/partners',
        component: Partners
      }
    ]
  },
  {
    path: '/calendar',
    component: LoadableCalendar,
    props: {
      exact: true
    }
  },
  {
    path: '/calendar/event/:slug',
    component: LoadableEvent
  },
  {
    path: '/projects',
    component: LoadableProjects,
    children: [
      {
        path: '/projects/teams',
        component: Teams
      },
      {
        path: '/projects/team/:slug',
        component: Team
      },
      {
        path: '/projects/regatta',
        component: Regatta
      }
    ]
  },
  {
    path: '/media/news/:slug',
    component: LoadableNewsArticle,
    props: {
      exact: true
    }
  },
  {
    path: '/media',
    component: LoadableMedia,
    children: [
      {
        path: '/media/news',
        component: News
      },
      {
        path: '/media/gallery',
        component: Gallery
      },
      {
        path: '/media/press-centre',
        component: PressCentre
      },
      {
        path: '/media/accreditation',
        component: PressCentre
      }
    ]
  },
  {
    path: '/contacts',
    component: Contacts
  },
  {
    path: '/pages/:slug',
    component: CustomPage
  }
];
