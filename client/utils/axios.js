import axios from 'axios';

export default (url, options = {}) => {
  const {method = 'POST', data = {}} = options;

  return axios({
    method,
    baseURL: APP_ENV === 'Node' ? DOMAIN : '',
    url,
    data,
    params: method === 'GET' ? data : ''
  });
};
