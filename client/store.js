import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
// import createLogger from 'redux-logger';
import reducers from './reducers';

export function configureStore(initialState = {}) {
  const enhancers = [
    applyMiddleware(thunk)
    // applyMiddleware(createLogger()),
  ];

  return createStore(reducers, initialState, compose(...enhancers));
}
