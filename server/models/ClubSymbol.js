const keystone = require('keystone'),
  Types = keystone.Field.Types;

const ClubSymbol = new keystone.List('ClubSymbol', {
  label: 'Символика',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true},
  sortable: true
});

ClubSymbol.add({
  type: {
    type: Types.Select,
    numeric: true,
    options: [
      {
        value: 1,
        label: 'Основной логотип'
      },
      {
        value: 2,
        label: 'Вымпел'
      }
    ],
    default: 2
    // hidden: true
  },
  image: {
    type: Types.CloudinaryImage,
    label: 'Изображение',
    folder: '/club_symbols_pennant',
    autoCleanup: true
  },
  mainTitle: {type: Types.Text, label: 'Заголовок', dependsOn: {type: 1}},
  name: {type: Types.Text, label: 'Название', default: 'Название'},
  desc: {type: Types.Textarea, label: 'Описание'}
});

ClubSymbol.defaultColumns = 'name, type';
ClubSymbol.register();
