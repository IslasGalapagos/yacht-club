const keystone = require('keystone'),
  Types = keystone.Field.Types;

const ClubLeadership = new keystone.List('ClubLeadership', {
  label: '👥 Руководство',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true},
  sortable: true
});

ClubLeadership.add({
  type: {
    type: Types.Select,
    label: 'Тип',
    numeric: true,
    options: [
      {
        value: 1,
        label: 'командор / вице-командор'
      },
      {
        value: 2,
        label: 'учредители'
      },
      {
        value: 3,
        label: 'руководство'
      }
    ],
    emptyOption: false
  },
  avatar: {
    type: Types.CloudinaryImage,
    label: 'Фото',
    folder: '/club_leadership',
    autoCleanup: true
  },
  name: {type: Types.Text, label: 'Имя', default: 'Имя'},
  position: {type: Types.Text, label: 'Должность'},
  desc: {type: Types.Textarea, label: 'Описание', dependsOn: {type: 1}}
});

ClubLeadership.defaultColumns = 'name, type';
ClubLeadership.register();
