const keystone = require('keystone'),
  Types = keystone.Field.Types;

const CustomPage = new keystone.List('CustomPage', {
  label: '🔧 Типовая страница',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

CustomPage.add(
  {
    name: {
      type: Types.Text,
      label: 'Название',
      default: 'Название',
      required: true
    },
    state: {
      type: Types.Select,
      label: 'Состояние',
      options: [
        {
          value: 'draft',
          label: 'черновик'
        },
        {
          value: 'published',
          label: 'опубликовано'
        },
        {
          value: 'archive',
          label: 'архив'
        }
      ],
      default: 'draft'
    }
  },
  'Меню',
  {
    url: {type: Types.Text, label: '/projects/', note: '(латиница)'},
    section: {
      type: Types.Select,
      label: 'Раздел',
      options: [
        {
          value: '/club',
          label: 'Клуб'
        },
        {
          value: '/calendar',
          label: 'Календарь'
        },
        {
          value: '/projects',
          label: 'Проекты'
        },
        {
          value: '/media',
          label: 'Медиа'
        },
        {
          value: '/contacts',
          label: 'Контакты'
        }
      ],
      default: '/club',
      required: true
    },
    menu: {
      type: Types.Select,
      label: 'Расположение ссылки',
      options: [
        {
          value: 'nav',
          label: 'меню (слева от выбранного пункта)'
        },
        {
          value: 'subnav',
          label: 'подменю'
        }
      ],
      default: 'subnav',
      required: true
    },
    cover: {
      type: Types.CloudinaryImage,
      label: 'Обложка',
      dependsOn: {menu: 'subnav'}
    }
  },
  'Контент',
  {
    content: {
      brief: {
        type: Types.Html,
        label: 'Первый абзац',
        wysiwyg: true,
        height: 100
      },
      extended: {
        type: Types.Html,
        label: 'Остальной текст описания',
        wysiwyg: true,
        height: 300
      }
    },
    logo: {
      type: Types.CloudinaryImage,
      label: 'Логотип',
      folder: '/team_logo',
      autoCleanup: true
    },
    cover: {type: Types.CloudinaryImage, label: 'Обложка'},
    slideshow: {
      type: Types.Relationship,
      label: 'Слайдшоу',
      ref: 'Slideshow',
      filters: {state: 'published'}
    },
    gallery: {
      type: Types.Relationship,
      label: 'Фото и видео',
      ref: 'Gallery',
      filters: {state: 'published'},
      many: true
    }
  }
);

CustomPage.defaultColumns = 'name, state';
CustomPage.register();
