const keystone = require('keystone');
const SubSection = require('./SubSection');

const ProjectsSubSection = new keystone.List('ProjectsSubSection', {
  inherits: SubSection,
  hidden: false
});

ProjectsSubSection.add({
  section: {type: String, default: '/projects', hidden: true}
});

ProjectsSubSection.register();
