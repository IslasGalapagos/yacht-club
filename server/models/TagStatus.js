const keystone = require('keystone');

const TagStatus = new keystone.List('TagStatus', {
  label: 'Статусы',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

TagStatus.add({
  name: {
    type: String,
    label: 'Статус',
    default: 'Название',
    required: true
  }
});

TagStatus.register();
