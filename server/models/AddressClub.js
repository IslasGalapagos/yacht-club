const keystone = require('keystone');
const Types = keystone.Field.Types;

const AddressClub = new keystone.List('AddressClub', {
  label: 'Адреса',
  singular: 'Адрес',
  map: {name: 'name'},
  nodelete: true,
  nocreate: true
});

AddressClub.add({
  name: {type: String},
  location: {type: Types.Location}
});

AddressClub.register();
