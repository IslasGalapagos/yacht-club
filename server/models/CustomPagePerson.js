const keystone = require('keystone'),
  Types = keystone.Field.Types;

const CustomPagePerson = new keystone.List('CustomPagePerson', {
  label: '👥 Персоны',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

CustomPagePerson.add({
  name: {type: Types.Text, label: 'Имя', default: 'Имя', required: true},
  avatar: {
    type: Types.CloudinaryImage,
    label: 'Фото',
    folder: '/teams_persons',
    autoCleanup: true
  },
  position: {type: Types.Text, label: 'Должность'},
  desc: {type: Types.Textarea, label: 'Описание'},
  page: {type: Types.Relationship, label: 'Страница', ref: 'CustomPage'}
});

CustomPagePerson.register();
