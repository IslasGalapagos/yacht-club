const keystone = require('keystone');
const SubSection = require('./SubSection');

const ClubSubSection = new keystone.List('ClubSubSection', {
  inherits: SubSection,
  hidden: false
});

ClubSubSection.add({
  section: {type: String, default: '/club', hidden: true}
});

ClubSubSection.register();
