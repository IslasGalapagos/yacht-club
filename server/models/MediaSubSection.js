const keystone = require('keystone');
const SubSection = require('./SubSection');

const MediaSubSection = new keystone.List('MediaSubSection', {
  inherits: SubSection,
  hidden: false
});

MediaSubSection.add({
  section: {type: String, default: '/media', hidden: true}
});

MediaSubSection.register();
