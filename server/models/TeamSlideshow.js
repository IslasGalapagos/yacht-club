const keystone = require('keystone');
const Slideshow = require('./Slideshow');

const TeamSlideshow = new keystone.List('TeamSlideshow', {
  inherits: Slideshow,
  hidden: false
});

TeamSlideshow.add({
  section: {type: String, default: 'team', hidden: true}
});

TeamSlideshow.register();
