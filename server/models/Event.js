const keystone = require('keystone'),
  Types = keystone.Field.Types;

const Event = new keystone.List('Event', {
  label: 'События',
  map: {name: 'title'},
  autokey: {path: 'slug', from: 'title', unique: true}
});

Event.add({
  title: {type: Types.Text, label: 'Название', default: 'Название'},
  state: {
    type: Types.Select,
    label: 'Состояние',
    options: [
      {
        value: 'draft',
        label: 'черновик'
      },
      {
        value: 'published',
        label: 'опубликовано'
      },
      {
        value: 'archive',
        label: 'архив'
      }
    ],
    default: 'draft'
  },
  date: {
    start: {
      string: {type: Types.Date, label: 'Дата начала'},
      day: {type: Types.Number, hidden: true},
      month: {type: Types.Number, hidden: true},
      year: {type: Types.Number, hidden: true}
    },
    end: {
      string: {type: Types.Date, label: 'Дата завершения'},
      day: {type: Types.Number, hidden: true},
      month: {type: Types.Number, hidden: true},
      year: {type: Types.Number, hidden: true}
    },
    length: {type: Types.Number, hidden: true}
  },
  content: {
    brief: {
      type: Types.Html,
      label: 'Первый абзац',
      wysiwyg: true,
      height: 100
    },
    extended: {
      type: Types.Html,
      label: 'Остальной текст описания',
      wysiwyg: true,
      height: 300
    }
  },
  additional: {type: Types.Html, wysiwyg: true, label: 'Доп. информация'},
  logo: {
    type: Types.CloudinaryImage,
    label: 'Логотип события',
    folder: '/event_logo',
    autoCleanup: true
  },
  cover: {
    type: Types.CloudinaryImage,
    label: 'Обложка',
    note: 'для списка и двух афиш, размер: … Х …',
    folder: '/event_covers',
    autoCleanup: true
  },
  slideshow: {
    type: Types.Relationship,
    label: 'Слайдшоу',
    ref: 'EventSlideshow',
    filters: {state: 'published'}
  },
  gallery: {
    type: Types.Relationship,
    label: 'Фото и видео',
    ref: 'Gallery',
    filters: {state: 'published'},
    many: true
  },
  bigPosterColor: {
    type: Types.Color,
    label: 'Цвет в большой афише',
    default: '#FFFFFF'
  },
  smallPosterColor: {
    type: Types.Color,
    label: 'Цвет в маленькой афише',
    default: '#FFFFFF'
  },
  meaningful: {type: Types.Boolean, label: 'Значимое'},
  promote: {type: Types.Boolean, label: 'Промо'},
  calendarColor: {
    type: Types.Color,
    label: 'Цвет линии в календаре',
    default: '#EF9C00'
  },
  eventLocation: {
    type: Types.Relationship,
    label: 'Место проведения',
    ref: 'EventLocation',
    many: true,
    index: true
  },
  types: {
    type: Types.Relationship,
    label: 'Тип',
    ref: 'TagType',
    many: true,
    index: true
  },
  categories: {
    type: Types.Relationship,
    label: 'Категории',
    ref: 'TagCategory',
    many: true,
    index: true
  },
  statuses: {
    type: Types.Relationship,
    label: 'Статус',
    ref: 'TagStatus',
    many: true,
    index: true
  },
  classes: {
    type: Types.Relationship,
    label: 'Класс',
    ref: 'TagClass',
    many: true,
    index: true
  },
  teams: {
    type: Types.Relationship,
    label: 'Команды',
    ref: 'Team',
    many: true,
    index: true
  },
  customPages: {
    type: Types.Relationship,
    label: 'Типовые страницы',
    ref: 'CustomPage',
    many: true,
    index: true
  },
  series: {
    name: {
      type: Types.Relationship,
      label: 'Серии',
      ref: 'EventSeries'
    },
    phase: {type: Types.Text, label: 'Этап в серии'},
    otherEvents: {type: Types.TextArray, hidden: true}
  },
  siteLink: {type: Types.Url, label: 'Ссылка на сайт события'},
  accreditation: {type: Types.Boolean, label: 'Аккредитация'},
  results: {
    type: Types.Select,
    label: 'Результаты',
    options: [
      {
        value: 'ext_link',
        label: 'внешняя ссылка'
      },
      {
        value: '#results',
        label: 'якорь'
      }
    ]
  },
  resultsLink: {
    type: Types.Url,
    label: 'Ссылка на результаты',
    dependsOn: {results: 'ext_link'}
  },
  siblings: {type: Types.TextArray, hidden: true}
});

Event.schema.pre('save', function(next) {
  const dateStart = new Date(this.date.start.string);
  this.date.start.day = dateStart.getDate();
  this.date.start.month = dateStart.getMonth() + 1;
  this.date.start.year = dateStart.getFullYear();

  const dateEnd = new Date(this.date.end.string);
  this.date.end.day = dateEnd.getDate();
  this.date.end.month = dateEnd.getMonth() + 1;
  this.date.end.year = dateEnd.getFullYear();

  const diffTime = Math.abs(dateEnd.getTime() - dateStart.getTime());
  const diffDays = Math.ceil(diffTime / (1000 * 3600 * 24)) + 1;
  this.date.length = diffDays;

  next();
});

Event.defaultColumns =
  'title, state, date.start.string, eventLocation, meaningful, promote, series.name';
Event.register();
