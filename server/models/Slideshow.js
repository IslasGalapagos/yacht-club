const keystone = require('keystone');
const Types = keystone.Field.Types;

const Slideshow = new keystone.List('Slideshow', {
  label: '📷 Слайдшоу',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true},
  hidden: true
});

Slideshow.add(
  {
    name: {type: Types.Text, label: 'Имя'},
    state: {
      type: Types.Select,
      label: 'Состояние',
      options: [
        {
          value: 'draft',
          label: 'черновик'
        },
        {
          value: 'published',
          label: 'опубликовано'
        },
        {
          value: 'archive',
          label: 'архив'
        }
      ],
      default: 'draft'
    }
  },
  'Фото 1',
  {
    slide1: {
      textEnable: {type: Boolean, label: 'Текст'},
      text: {
        type: Types.Text,
        label: 'Текст',
        dependsOn: {'slide1.textEnable': true}
      },
      textColor: {
        type: Types.Select,
        label: 'Цвет текста',
        dependsOn: {'slide1.textEnable': true},
        options: [
          {
            value: '#FFFFFF',
            label: 'Белый'
          },
          {
            value: '#000000',
            label: 'Черный'
          }
        ],
        emptyOption: false,
        default: '#FFFFFF'
      },
      textAlign: {
        type: Types.Select,
        label: 'Выравнивание',
        dependsOn: {'slide1.textEnable': true},
        options: [
          {
            value: 'top_right',
            label: '↗'
          },
          {
            value: 'bottom_right',
            label: '↘'
          },
          {
            value: 'bottom_left',
            label: '↙'
          },
          {
            value: 'top_left',
            label: '↖'
          }
        ],
        emptyOption: false,
        default: 'bottom_left'
      },
      url: {
        type: Types.Url,
        label: 'Ссылка',
        dependsOn: {'slide1.textEnable': true}
      },
      image: {
        type: Types.CloudinaryImage,
        folder: 'slideshow',
        label: 'Изображение'
      }
    }
  },
  'Фото 2',
  {
    slide2: {
      textEnable: {type: Boolean, label: 'Текст'},
      text: {
        type: Types.Text,
        label: 'Текст',
        dependsOn: {'slide2.textEnable': true}
      },
      textColor: {
        type: Types.Select,
        label: 'Цвет текста',
        dependsOn: {'slide2.textEnable': true},
        options: [
          {
            value: '#FFFFFF',
            label: 'Белый'
          },
          {
            value: '#000000',
            label: 'Черный'
          }
        ],
        emptyOption: false,
        default: '#FFFFFF'
      },
      textAlign: {
        type: Types.Select,
        label: 'Выравнивание',
        dependsOn: {'slide2.textEnable': true},
        options: [
          {
            value: 'top_right',
            label: '↗'
          },
          {
            value: 'bottom_right',
            label: '↘'
          },
          {
            value: 'bottom_left',
            label: '↙'
          },
          {
            value: 'top_left',
            label: '↖'
          }
        ],
        emptyOption: false,
        default: 'bottom_left'
      },
      url: {
        type: Types.Url,
        label: 'Ссылка',
        dependsOn: {'slide2.textEnable': true}
      },
      image: {
        type: Types.CloudinaryImage,
        folder: 'slideshow',
        label: 'Изображение'
      }
    }
  },
  'Фото 3',
  {
    slide3: {
      textEnable: {type: Boolean, label: 'Текст'},
      text: {
        type: Types.Text,
        label: 'Текст',
        dependsOn: {'slide3.textEnable': true}
      },
      textColor: {
        type: Types.Select,
        label: 'Цвет текста',
        dependsOn: {'slide3.textEnable': true},
        options: [
          {
            value: '#FFFFFF',
            label: 'Белый'
          },
          {
            value: '#000000',
            label: 'Черный'
          }
        ],
        emptyOption: false,
        default: '#FFFFFF'
      },
      textAlign: {
        type: Types.Select,
        label: 'Выравнивание',
        dependsOn: {'slide3.textEnable': true},
        options: [
          {
            value: 'top_right',
            label: '↗'
          },
          {
            value: 'bottom_right',
            label: '↘'
          },
          {
            value: 'bottom_left',
            label: '↙'
          },
          {
            value: 'top_left',
            label: '↖'
          }
        ],
        emptyOption: false,
        default: 'bottom_left'
      },
      url: {
        type: Types.Url,
        label: 'Ссылка',
        dependsOn: {'slide3.textEnable': true}
      },
      image: {
        type: Types.CloudinaryImage,
        folder: 'slideshow',
        label: 'Изображение'
      }
    }
  },
  'Фото 4',
  {
    slide4: {
      textEnable: {type: Boolean, label: 'Текст'},
      text: {
        type: Types.Text,
        label: 'Текст',
        dependsOn: {'slide4.textEnable': true}
      },
      textColor: {
        type: Types.Select,
        label: 'Цвет текста',
        dependsOn: {'slide4.textEnable': true},
        options: [
          {
            value: '#FFFFFF',
            label: 'Белый'
          },
          {
            value: '#000000',
            label: 'Черный'
          }
        ],
        emptyOption: false,
        default: '#FFFFFF'
      },
      textAlign: {
        type: Types.Select,
        label: 'Выравнивание',
        dependsOn: {'slide4.textEnable': true},
        options: [
          {
            value: 'top_right',
            label: '↗'
          },
          {
            value: 'bottom_right',
            label: '↘'
          },
          {
            value: 'bottom_left',
            label: '↙'
          },
          {
            value: 'top_left',
            label: '↖'
          }
        ],
        emptyOption: false,
        default: 'bottom_left'
      },
      url: {
        type: Types.Url,
        label: 'Ссылка',
        dependsOn: {'slide4.textEnable': true}
      },
      image: {
        type: Types.CloudinaryImage,
        folder: 'slideshow',
        label: 'Изображение'
      }
    }
  },
  'Фото 5',
  {
    slide5: {
      textEnable: {type: Boolean, label: 'Текст'},
      text: {
        type: Types.Text,
        label: 'Текст',
        dependsOn: {'slide5.textEnable': true}
      },
      textColor: {
        type: Types.Select,
        label: 'Цвет текста',
        dependsOn: {'slide5.textEnable': true},
        options: [
          {
            value: '#FFFFFF',
            label: 'Белый'
          },
          {
            value: '#000000',
            label: 'Черный'
          }
        ],
        emptyOption: false,
        default: '#FFFFFF'
      },
      textAlign: {
        type: Types.Select,
        label: 'Выравнивание',
        dependsOn: {'slide5.textEnable': true},
        options: [
          {
            value: 'top_right',
            label: '↗'
          },
          {
            value: 'bottom_right',
            label: '↘'
          },
          {
            value: 'bottom_left',
            label: '↙'
          },
          {
            value: 'top_left',
            label: '↖'
          }
        ],
        emptyOption: false,
        default: 'bottom_left'
      },
      url: {
        type: Types.Url,
        label: 'Ссылка',
        dependsOn: {'slide5.textEnable': true}
      },
      image: {
        type: Types.CloudinaryImage,
        folder: 'slideshow',
        label: 'Изображение'
      }
    }
  },
  'Фото 6',
  {
    slide6: {
      textEnable: {type: Boolean, label: 'Текст'},
      text: {
        type: Types.Text,
        label: 'Текст',
        dependsOn: {'slide6.textEnable': true}
      },
      textColor: {
        type: Types.Select,
        label: 'Цвет текста',
        dependsOn: {'slide6.textEnable': true},
        options: [
          {
            value: '#FFFFFF',
            label: 'Белый'
          },
          {
            value: '#000000',
            label: 'Черный'
          }
        ],
        emptyOption: false,
        default: '#FFFFFF'
      },
      textAlign: {
        type: Types.Select,
        label: 'Выравнивание',
        dependsOn: {'slide6.textEnable': true},
        options: [
          {
            value: 'top_right',
            label: '↗'
          },
          {
            value: 'bottom_right',
            label: '↘'
          },
          {
            value: 'bottom_left',
            label: '↙'
          },
          {
            value: 'top_left',
            label: '↖'
          }
        ],
        emptyOption: false,
        default: 'bottom_left'
      },
      url: {
        type: Types.Url,
        label: 'Ссылка',
        dependsOn: {'slide6.textEnable': true}
      },
      image: {
        type: Types.CloudinaryImage,
        folder: 'slideshow',
        label: 'Изображение'
      }
    }
  },
  'Фото 7',
  {
    slide7: {
      textEnable: {type: Boolean, label: 'Текст'},
      text: {
        type: Types.Text,
        label: 'Текст',
        dependsOn: {'slide7.textEnable': true}
      },
      textColor: {
        type: Types.Select,
        label: 'Цвет текста',
        dependsOn: {'slide7.textEnable': true},
        options: [
          {
            value: '#FFFFFF',
            label: 'Белый'
          },
          {
            value: '#000000',
            label: 'Черный'
          }
        ],
        emptyOption: false,
        default: '#FFFFFF'
      },
      textAlign: {
        type: Types.Select,
        label: 'Выравнивание',
        dependsOn: {'slide7.textEnable': true},
        options: [
          {
            value: 'top_right',
            label: '↗'
          },
          {
            value: 'bottom_right',
            label: '↘'
          },
          {
            value: 'bottom_left',
            label: '↙'
          },
          {
            value: 'top_left',
            label: '↖'
          }
        ],
        emptyOption: false,
        default: 'bottom_left'
      },
      url: {
        type: Types.Url,
        label: 'Ссылка',
        dependsOn: {'slide7.textEnable': true}
      },
      image: {
        type: Types.CloudinaryImage,
        folder: 'slideshow',
        label: 'Изображение'
      }
    }
  }
);

Slideshow.defaultColumns = 'name, state';

Slideshow.register();

exports = module.exports = Slideshow;
