const keystone = require('keystone');

const TagClass = new keystone.List('TagClass', {
  label: 'Классы',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

TagClass.add({
  name: {
    type: String,
    label: 'Класс',
    default: 'Название',
    required: true
  }
});

TagClass.register();
