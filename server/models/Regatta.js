const keystone = require('keystone'),
  Types = keystone.Field.Types;

const Regatta = new keystone.List('Regatta', {
  label: '⛵ Регаты',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

Regatta.add({
  name: {
    type: Types.Text,
    label: 'Название',
    default: 'Название',
    required: true
  },
  state: {
    type: Types.Select,
    label: 'Состояние',
    options: [
      {
        value: 'draft',
        label: 'черновик'
      },
      {
        value: 'published',
        label: 'опубликовано'
      },
      {
        value: 'archive',
        label: 'архив'
      }
    ],
    default: 'draft'
  },
  type: {
    type: Types.Select,
    label: 'Тип',
    note: 'событие или серия',
    numeric: true,
    options: [
      {
        value: 1,
        label: 'событие'
      },
      {
        value: 2,
        label: 'серии'
      }
    ],
    emptyOption: false
  },
  event: {
    type: Types.Relationship,
    label: 'Событие',
    ref: 'Event',
    filters: {state: 'published'},
    dependsOn: {type: 1}
  },
  series: {
    type: Types.Relationship,
    label: 'Серия',
    ref: 'EventSeries',
    dependsOn: {type: 2},
    initial: true,
    index: true
  },
  text: {type: Types.Textarea, label: 'Текст', height: 200}
});

Regatta.defaultColumns = 'name, state, type';
Regatta.register();
