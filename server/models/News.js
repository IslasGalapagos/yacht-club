const keystone = require('keystone');
const Types = keystone.Field.Types;

const News = new keystone.List('News', {
  label: 'Новости',
  map: {name: 'title'},
  autokey: {path: 'slug', from: 'title', unique: true}
});

News.add({
  title: {
    type: Types.Text,
    label: 'Заголовок',
    default: 'Заголовок',
    required: true
  },
  state: {
    type: Types.Select,
    label: 'Состояние',
    options: [
      {
        value: 'draft',
        label: 'черновик'
      },
      {
        value: 'published',
        label: 'опубликовано'
      },
      {
        value: 'archive',
        label: 'архив'
      }
    ],
    default: 'draft'
  },
  content: {
    brief: {
      type: Types.Html,
      label: 'Первый абзац',
      wysiwyg: true,
      height: 100
    },
    extended: {
      type: Types.Html,
      label: 'Остальной текст описания',
      wysiwyg: true,
      height: 300
    }
  },
  date: {
    string: {type: Types.Datetime, label: 'Дата', default: Date.now},
    month: {type: Number, hidden: true},
    year: {type: Number, hidden: true}
  },
  filters: {
    eventType: {
      type: Types.Select,
      label: 'Событие / серия',
      options: [
        {
          value: 'event',
          label: 'событие'
        },
        {
          value: 'series',
          label: 'серия'
        }
      ]
    },
    event: {
      type: Types.Relationship,
      ref: 'Event',
      label: '↳',
      many: true,
      dependsOn: {'filters.eventType': 'event'}
    },
    series: {
      type: Types.Relationship,
      ref: 'EventSeries',
      label: '↳',
      many: true,
      dependsOn: {'filters.eventType': 'series'}
    },
    teams: {
      type: Types.Relationship,
      ref: 'Team',
      label: 'Команды',
      many: true
    },
    classes: {
      type: Types.Relationship,
      ref: 'TagClass',
      label: 'Классы',
      many: true
    },
    customPages: {
      type: Types.Relationship,
      ref: 'CustomPage',
      label: 'Типовая страница',
      many: true
    }
  },
  cover: {type: Types.CloudinaryImage, label: 'Обложка'},
  album: {type: Types.Relationship, ref: 'Gallery', label: 'Фото-альбом'},
  docUrl: {type: Types.Url, label: 'Ссылка на документ'}
});

News.schema.pre('save', function(next) {
  const dateObj = new Date(this.date.string);
  this.date.month = dateObj.getMonth() + 1;
  this.date.year = dateObj.getFullYear();

  next();
});

News.defaultColumns = 'title, state, date.string';
News.register();
