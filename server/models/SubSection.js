const keystone = require('keystone'),
  Types = keystone.Field.Types;

const SubSection = new keystone.List('SubSection', {
  label: '🔗 Ссылки на подраздел',
  map: {name: 'link'},
  autokey: {path: 'slug', from: 'link', unique: true},
  hidden: true,
  sortable: true,
  nodelete: true,
  nocreate: true
});

SubSection.add({
  active: {
    type: Types.Boolean,
    label: 'Показана'
  },
  link: {type: Types.Text, label: 'Ссылка', required: true, noedit: true},
  name: {type: Types.Text, label: 'Название'},
  cover: {type: Types.CloudinaryImage, label: 'Обложка'},
  check: {type: Types.Boolean, hidden: true}
});

SubSection.defaultColumns = 'link, name, active';

SubSection.register();

exports = module.exports = SubSection;
