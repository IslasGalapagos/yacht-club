const keystone = require('keystone');
const Slideshow = require('./Slideshow');

const EventSlideshow = new keystone.List('EventSlideshow', {
  inherits: Slideshow,
  hidden: false
});

EventSlideshow.add({
  section: {type: String, default: 'event', hidden: true}
});

EventSlideshow.register();
