const keystone = require('keystone');
const Slideshow = require('./Slideshow');

const ClubSlideshow = new keystone.List('ClubSlideshow', {
  inherits: Slideshow,
  hidden: false,
  nocreate: true,
  nodelete: true
});

ClubSlideshow.add({
  section: {type: String, default: 'club', hidden: true}
});

ClubSlideshow.register();
