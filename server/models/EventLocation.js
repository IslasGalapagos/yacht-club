const keystone = require('keystone');

const EventLocation = new keystone.List('EventLocation', {
  label: '📍 Места проведения',
  map: {name: 'location'},
  autokey: {path: 'slug', from: 'location', unique: true}
});

EventLocation.add({
  location: {
    type: String,
    label: 'Место проведения',
    required: true
  }
});

EventLocation.relationship({ref: 'Event', path: 'location'});
EventLocation.register();
