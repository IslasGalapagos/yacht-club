const keystone = require('keystone'),
  Types = keystone.Field.Types;

const Calendar = new keystone.List('Calendar', {
  label: '📆 Календарь',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true},
  nodelete: true,
  nocreate: true
});

Calendar.add({
  name: {type: Types.Text, noedit: true},
  title: {type: Types.Text, label: 'Заголовок'},
  filters: {
    types: {
      type: Types.Relationship,
      label: 'Типы',
      ref: 'TagType',
      many: true
    },
    categories: {
      type: Types.Relationship,
      label: 'Категории',
      ref: 'TagCategory',
      many: true
    },
    statuses: {
      type: Types.Relationship,
      label: 'Статусы',
      ref: 'TagStatus',
      many: true
    },
    classes: {
      type: Types.Relationship,
      label: 'Классы',
      ref: 'TagClass',
      many: true
    },
    teams: {
      type: Types.Relationship,
      label: 'Команды',
      ref: 'Team',
      many: true
    },
    years: {type: Types.TextArray, hidden: true}
  }
});

Calendar.register();
