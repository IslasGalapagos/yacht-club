const keystone = require('keystone');

const EventSeries = new keystone.List('EventSeries', {
  label: 'Серии',
  map: {name: 'series'},
  autokey: {path: 'slug', from: 'series', unique: true}
});

EventSeries.add({
  series: {
    type: String,
    label: 'Серия событий',
    required: true
  }
});

// EventSeries.relationship({ ref: 'Event', path: 'series.name' });
EventSeries.register();
