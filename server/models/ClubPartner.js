const keystone = require('keystone'),
  Types = keystone.Field.Types;

const ClubPartner = new keystone.List('ClubPartner', {
  label: 'Партнёры',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true},
  sortable: true
});

ClubPartner.add({
  type: {
    type: Types.Select,
    label: 'Тип',
    numeric: true,
    options: [
      {
        value: 1,
        label: 'друзья'
      },
      {
        value: 2,
        label: 'партнёры'
      },
      {
        value: 3,
        label: 'информационные партнёры'
      }
    ],
    default: 1,
    emptyOption: false
  },
  image: {
    type: Types.CloudinaryImage,
    label: 'Изображение',
    note: 'Не больше 194Х194',
    folder: '/club_partners',
    autoCleanup: true
  },
  name: {type: Types.Text, label: 'Название', default: 'Название'},
  desc: {type: Types.Textarea, label: 'Описание'}
});

ClubPartner.defaultColumns = 'name, type';
ClubPartner.register();
