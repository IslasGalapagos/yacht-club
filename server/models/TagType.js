const keystone = require('keystone');

const TagType = new keystone.List('TagType', {
  label: 'Типы',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

TagType.add({
  name: {
    type: String,
    label: 'Тип',
    default: 'Название',
    required: true
  }
});

TagType.register();
