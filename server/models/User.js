const keystone = require('keystone');
const Types = keystone.Field.Types;

const User = new keystone.List('User', {
  label: 'Пользователи'
});

User.add(
  {
    name: {type: Types.Name, label: 'Имя', required: true, index: true},
    email: {
      type: Types.Email,
      label: 'Емейл',
      initial: true,
      required: true,
      index: true
    },
    password: {
      type: Types.Password,
      label: 'Пароль',
      initial: true,
      required: true
    }
  },
  'Права',
  {
    isAdmin: {type: Boolean, label: 'Админ', index: true}
  }
);

User.schema.virtual('canAccessKeystone').get(function() {
  return this.isAdmin;
});

User.defaultColumns = 'name, email, isAdmin';
User.register();
