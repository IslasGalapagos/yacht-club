const keystone = require('keystone'),
  Types = keystone.Field.Types;

const TeamPerson = new keystone.List('TeamPerson', {
  label: '👥 Участники команды',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

TeamPerson.add({
  name: {type: Types.Text, label: 'Имя', default: 'Имя', required: true},
  avatar: {
    type: Types.CloudinaryImage,
    label: 'Фото',
    folder: '/teams_persons',
    autoCleanup: true
  },
  role: {type: Types.Text, label: 'Роль'},
  desc: {type: Types.Textarea, label: 'Описание'},
  team: {type: Types.Relationship, label: 'Команда', ref: 'Team'}
});

TeamPerson.register();
