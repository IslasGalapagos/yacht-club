const keystone = require('keystone'),
  Types = keystone.Field.Types;

const ClubMember = new keystone.List('ClubMember', {
  label: '👥 Члены клуба',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true},
  sortable: true
});

ClubMember.add({
  type: {
    type: Types.Select,
    label: 'Тип',
    numeric: true,
    options: [
      {
        value: 1,
        label: 'почётные'
      },
      {
        value: 2,
        label: 'действующие'
      },
      {
        value: 3,
        label: 'бывшие'
      }
    ],
    emptyOption: false
  },
  avatar: {
    type: Types.CloudinaryImage,
    label: 'Фото',
    dependsOn: {
      type: [1]
    },
    folder: '/club_members',
    autoCleanup: true
  },
  name: {type: Types.Text, label: 'Имя', default: 'Имя'},
  desc: {type: Types.Textarea, label: 'Описание'}
});

ClubMember.defaultColumns = 'name, type';
ClubMember.register();
