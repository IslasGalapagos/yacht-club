const keystone = require('keystone');

const TagCategory = new keystone.List('TagCategory', {
  label: 'Категории',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

TagCategory.add({
  name: {
    type: String,
    label: 'Категория',
    default: 'Название',
    required: true
  }
});

TagCategory.register();
