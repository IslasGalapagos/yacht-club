const keystone = require('keystone');
const Types = keystone.Field.Types;

const Gallery = new keystone.List('Gallery', {
  label: 'Фото и видео',
  map: {name: 'name'},
  autokey: {path: 'slug', from: 'name', unique: true}
});

Gallery.add({
  name: {
    type: Types.Text,
    label: 'Название альбома',
    default: 'Название альбома',
    required: true
  },
  state: {
    type: Types.Select,
    label: 'Состояние',
    options: [
      {
        value: 'draft',
        label: 'черновик'
      },
      {
        value: 'published',
        label: 'опубликовано'
      },
      {
        value: 'archive',
        label: 'архив'
      }
    ],
    default: 'draft'
  },
  dateType: {type: Types.Boolean, label: 'Синтетическая дата', default: false},
  date: {type: Types.Date, label: 'Дата', dependsOn: {dateType: false}},
  dateEvent: {
    type: Types.Relationship,
    label: 'Дата события',
    ref: 'Event',
    dependsOn: {dateType: true}
  },
  photos: {type: Types.CloudinaryImages, label: 'Фото'},
  video: {type: Types.TextArray, label: 'Видео', note: 'URL'}
});

Gallery.defaultColumns = 'name, state';
Gallery.register();
