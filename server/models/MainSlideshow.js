const keystone = require('keystone');
const Slideshow = require('./Slideshow');

const MainSlideshow = new keystone.List('MainSlideshow', {
  inherits: Slideshow,
  hidden: false,
  nocreate: true,
  nodelete: true
});

MainSlideshow.add({
  section: {type: String, default: 'club', hidden: true}
});

MainSlideshow.register();
