const keystone = require('keystone');
const Types = keystone.Field.Types;

const Social = new keystone.List('Social', {
  label: 'Соцсети',
  map: {name: 'name'},
  nodelete: true,
  nocreate: true
});

Social.add({
  name: {type: String, label: 'Название', required: true, noedit: true},
  url: {type: Types.Url, label: 'Адрес'}
});

Social.defaultColumns = 'name, url';

Social.register();
