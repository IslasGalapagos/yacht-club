const keystone = require('keystone');
const Types = keystone.Field.Types;

const Accreditation = new keystone.List('Accreditation', {
  label: 'Заявки на аккредитацию',
  map: {name: 'name'},
  defaultSort: 'done',
  nocreate: true
});

Accreditation.add({
  done: {type: Types.Boolean, label: 'Просмотрено'},
  event: {
    type: Types.Relationship,
    ref: 'Event',
    label: 'Мероприятие',
    noedit: true
  },
  name: {type: Types.Text, label: 'Имя', noedit: true},
  country: {type: Types.Text, label: 'Страна', noedit: true},
  email: {type: Types.Email, label: 'E-mail', noedit: true},
  phone: {type: Types.Text, label: 'Номер телефона', noedit: true},
  comment: {
    type: Types.Textarea,
    label: 'Комментарий',
    height: 200,
    noedit: true
  }
});

Accreditation.defaultColumns = 'name, event, email, phone, done';

Accreditation.register();
