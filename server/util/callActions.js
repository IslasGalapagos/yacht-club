import App from '../../client/components/App';
import {sequence} from './promiseUtils';

export default (store, routes, url) => {
  const getUpdaters = (routes, acc) =>
    routes.reduce((updatersAcc, route) => {
      const {path} = route;
      const areTheSame = ~path.indexOf(':slug')
        ? new RegExp(path.replace(':slug', '[A-Za-z0-9-]+$')).test(url)
        : url === path;

      if (areTheSame || (url.indexOf(path) === 0 && path !== '/')) {
        const {loadData} = route.component;

        if (typeof loadData === 'function') {
          updatersAcc.push(loadData.bind(null, store, route.path));
        }
      }

      if (typeof route.children !== 'undefined') {
        updatersAcc.push(...getUpdaters(route.children, []));
      }

      return updatersAcc;
    }, acc);

  return sequence(getUpdaters(routes, [App.loadData.bind(null, store)]), f =>
    f()
  );
};
