const keystone = require('keystone');

const Team = keystone.list('Team');
const TeamPerson = keystone.list('TeamPerson');
const Regatta = keystone.list('Regatta');
const CustomPage = keystone.list('CustomPage');
const Event = keystone.list('Event');
const News = keystone.list('News');

const getSlideshowData = require('./getSlideshowData');

export function getAllTeams(req, res) {
  Team.model
    .find({}, 'name slug content.brief class logo cover')
    .where('state', 'published')
    .populate('class')
    .exec()
    .then(data => {
      const allData = {
        teams: data
      };
      return News.model
        .find()
        .where('state', 'published')
        .where('filters.teams')
        .ne([])
        .exec()
        .then(newsData => {
          allData.news = newsData;
          return allData;
        });
    })
    .then(allData => {
      return Team.model
        .aggregate([
          {
            $project: {
              gallery: 1
            }
          },
          {
            $unwind: {
              path: '$gallery',
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: 'galleries',
              localField: 'gallery',
              foreignField: '_id',
              as: 'gallery'
            }
          },
          {
            $unwind: {
              path: '$gallery',
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $group: {
              _id: '$gallery._id',
              gallery: {$addToSet: '$gallery'}
            }
          },
          {$sort: {'gallery.date': -1}}
        ])
        .then(galleries => {
          let photosArr = [];
          let videoArr = [];

          galleries.forEach(item => {
            if (!item.gallery.length) return;

            photosArr = photosArr.concat(item.gallery[0].photos);
            videoArr = videoArr.concat(item.gallery[0].video);
          });

          allData.gallery = {
            photos: photosArr,
            video: videoArr
          };

          return allData;
        });
    })
    .then(allData => {
      res.send(allData);
    })
    .catch(e => {
      console.log(e);
    });
}

export function getTeam(req, res) {
  const teamName = req.body.teamName;

  Team.model
    .aggregate([
      {
        $match: {
          slug: teamName,
          state: 'published'
        }
      },
      {
        $lookup: {
          from: 'slideshows',
          localField: 'slideshow',
          foreignField: '_id',
          as: 'slideshow'
        }
      },
      // { $unwind: {
      //     path: '$slideshow',
      //     preserveNullAndEmptyArrays: true,
      // } },
      {
        $lookup: {
          from: 'teampeople',
          localField: '_id',
          foreignField: 'team',
          as: 'people'
        }
      },
      {
        $lookup: {
          from: 'events',
          localField: '_id',
          foreignField: 'teams',
          as: 'events'
        }
      },
      {
        $unwind: {
          path: '$gallery',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'galleries',
          localField: 'gallery',
          foreignField: '_id',
          as: 'gallery'
        }
      },
      {
        $unwind: {
          path: '$gallery',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $group: {
          _id: '$_id',
          name: {$first: '$name'},
          content: {$first: '$content'},
          logo: {$first: '$logo'},
          slideshow: {$first: '$slideshow'},
          gallery: {$addToSet: '$gallery'},
          people: {$first: '$people'},
          events: {$first: '$events'}
        }
      },
      {
        $project: {
          name: 1,
          content: 1,
          'logo.url': 1,
          slideshow: 1,
          gallery: 1,
          'people.name': 1,
          'people.avatar.url': 1,
          'people.desc': 1,
          'people.role': 1,
          'events.title': 1,
          'events.slug': 1,
          'events.date': 1,
          'events.location': 1,
          'events.cover': 1
        }
      }
    ])
    .then(data => {
      const allData = data[0];
      let photosArr = [];
      let videoArr = [];

      allData.gallery.forEach(item => {
        photosArr = photosArr.concat(item.photos);
        videoArr = videoArr.concat(item.video);
      });

      allData.gallery = {
        photos: photosArr,
        video: videoArr
      };

      return allData;
    })
    .then(allData => {
      const slideshow = allData.slideshow;

      if (!slideshow.length) return allData;

      return getSlideshowData(slideshow[0].slug).then(slideshowData => {
        allData.slideshow = slideshowData;
        return allData;
      });
    })
    .then(allData => {
      res.send(allData);
    })
    .catch(err => {
      console.log(err);
    });
}

export function getRegatta(req, res) {
  Regatta.model
    .aggregate([
      {$match: {state: 'published'}},
      {
        $lookup: {
          from: 'events',
          localField: 'event',
          foreignField: '_id',
          as: 'event'
        }
      },
      {
        $unwind: {
          path: '$event',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'eventseries',
          localField: 'series',
          foreignField: '_id',
          as: 'series'
        }
      },
      {
        $unwind: {
          path: '$series',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'events',
          localField: 'series._id',
          foreignField: 'series.name',
          as: 'events'
        }
      },
      {
        $project: {
          slug: 1,
          name: 1,
          event: 1,
          text: 1,
          events: {
            $filter: {
              input: '$events',
              as: 'event',
              cond: {$eq: ['$$event.state', 'published']}
            }
          }
        }
      }
    ])
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log(err);
    });
}

export function getCustomPage(req, res) {
  const customPageName = req.body.customPageName;

  CustomPage.model
    .aggregate([
      {
        $match: {
          url: customPageName,
          state: 'published'
        }
      },
      {
        $lookup: {
          from: 'slideshows',
          localField: 'slideshow',
          foreignField: '_id',
          as: 'slideshow'
        }
      },
      // { $unwind: {
      //     path: '$slideshow',
      //     preserveNullAndEmptyArrays: true,
      // } },
      {
        $lookup: {
          from: 'custompagepeople',
          localField: '_id',
          foreignField: 'page',
          as: 'people'
        }
      },
      {
        $lookup: {
          from: 'events',
          localField: '_id',
          foreignField: 'customPages',
          as: 'events'
        }
      },
      {
        $lookup: {
          from: 'news',
          localField: '_id',
          foreignField: 'filters.customPages',
          as: 'news'
        }
      },
      {
        $unwind: {
          path: '$gallery',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'galleries',
          localField: 'gallery',
          foreignField: '_id',
          as: 'gallery'
        }
      },
      {
        $unwind: {
          path: '$gallery',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $group: {
          _id: '$_id',
          name: {$first: '$name'},
          content: {$first: '$content'},
          logo: {$first: '$logo'},
          slideshow: {$first: '$slideshow'},
          gallery: {$addToSet: '$gallery'},
          people: {$first: '$people'},
          events: {$first: '$events'},
          news: {$first: '$news'}
        }
      },
      {
        $project: {
          name: 1,
          content: 1,
          'logo.url': 1,
          slideshow: 1,
          gallery: 1,
          'people.name': 1,
          'people.avatar.url': 1,
          'people.desc': 1,
          'people.role': 1,
          'events.title': 1,
          'events.slug': 1,
          'events.date': 1,
          'events.location': 1,
          'events.cover': 1,
          'news.slug': 1,
          'news.date': 1,
          'news.title': 1,
          'news.content': 1,
          'news.cover': 1
        }
      }
    ])
    .then(data => {
      const allData = data[0];
      let photosArr = [];
      let videoArr = [];

      allData.gallery.forEach(item => {
        photosArr = photosArr.concat(item.photos);
        videoArr = videoArr.concat(item.video);
      });

      allData.gallery = {
        photos: photosArr,
        video: videoArr
      };

      return allData;
    })
    .then(allData => {
      const slideshow = allData.slideshow;

      if (!slideshow.length) return allData;

      return getSlideshowData(slideshow[0].slug).then(slideshowData => {
        allData.slideshow = slideshowData;
        return allData;
      });
    })
    .then(allData => {
      res.send(allData);
    })
    .catch(err => {
      console.log(err);
    });
}
