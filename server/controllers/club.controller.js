// import User from '../../models/User';

const keystone = require('keystone');

const ClubLeadership = keystone.list('ClubLeadership');
const ClubMember = keystone.list('ClubMember');
const ClubSymbol = keystone.list('ClubSymbol');
const ClubPartner = keystone.list('ClubPartner');

const getSlideshowData = require('./getSlideshowData');

export function getClubData(req, res) {
  getSlideshowData('klub')
    .then(slideshowData => {
      const allData = {
        slideshow: slideshowData
      };

      return ClubLeadership.model
        .find({}, 'avatar.url desc name position type')
        .exec()
        .then(result => {
          const leadershipData = {type1: [], type2: [], type3: []};
          result.forEach(item => {
            if (!item.type) return;
            leadershipData[`type${item.type}`].push(item);
          });

          allData.leadership = leadershipData;
          return allData;
        });
    })
    .then(allData => {
      return ClubMember.model
        .find({}, 'avatar.url desc name type')
        .exec()
        .then(result => {
          const membersData = {type1: [], type2: [], type3: []};
          result.forEach(item => {
            if (!item.type) return;
            membersData[`type${item.type}`].push(item);
          });

          allData.members = membersData;
          return allData;
        });
    })
    .then(allData => {
      return ClubSymbol.model
        .find({}, 'image.url mainTitle name desc type')
        .exec()
        .then(result => {
          const symbolsData = {
            mainLogo: null,
            pennants: {
              images: [],
              names: [],
              descs: []
            }
          };

          result.forEach(item => {
            if (!item.type) return;

            if (item.type === 1) {
              symbolsData.mainLogo = item;
            } else if (item.type === 2) {
              symbolsData.pennants.images.push(item.image.url);
              symbolsData.pennants.names.push(item.name);
              symbolsData.pennants.descs.push(item.desc);
            }
          });

          allData.symbols = symbolsData;
          return allData;
        });
    })
    .then(allData => {
      return ClubPartner.model
        .find({}, 'type image name desc')
        .exec()
        .then(result => {
          const partnersData = {type1: [], type2: [], type3: []};
          result.forEach(item => {
            if (!item.type) return;
            partnersData[`type${item.type}`].push(item);
          });

          allData.partners = partnersData;
          return allData;
        });
    })
    .then(allData => {
      res.send(allData);
    })
    .catch(err => {
      console.log(err);
    });
}
