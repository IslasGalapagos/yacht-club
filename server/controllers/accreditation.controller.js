const request = require('request');
const keystone = require('keystone');
const moment = require('moment');

const Event = keystone.list('Event');

export function getEventsWithAccreditation(req, res) {
  Event.model
    .find({}, 'title slug')
    .where('state', 'published')
    .where('accreditation', true)
    .where('date.end.string', {$gt: moment().format('YYYY-MM-DD')})
    .exec()
    .then(events => {
      res.send(events);
    });
}

export function addRequest(req, res) {
  if (
    !req.body.event ||
    !req.body.name ||
    !req.body.country ||
    !req.body.email ||
    !req.body['g-recaptcha-response']
  ) {
    return res.json({code: 1});
  }

  const secretKey = '6LcG-g4UAAAAANuEN2gpALAHreWjrJwpSU4fsne8';
  const verificationUrl =
    'https://www.google.com/recaptcha/api/siteverify' +
    '?secret=' +
    secretKey +
    '&response=' +
    req.body['g-recaptcha-response'];

  request(verificationUrl, function(error, response, body) {
    body = JSON.parse(body);

    if (body.success !== undefined && !body.success) {
      return res.json({code: 1});
    }

    Event.model
      .findOne()
      .where('slug', req.body.event)
      .exec()
      .then(result => {
        const newAccreditationRequest = new Accreditation.model({
          event: result._id,
          name: req.body.name,
          country: req.body.country,
          email: req.body.email
        });

        if (req.body.phone) {
          newAccreditationRequest.phone = req.body.phone;
        }

        if (req.body.comment) {
          newAccreditationRequest.comment = req.body.comment;
        }

        newAccreditationRequest.save();

        res.json({code: 0});
      })
      .catch(er => {
        res.sendStatus(500);
      });
  });
}
