const keystone = require('keystone');

const Slideshow = keystone.list('Slideshow');

function getSlideshowData(slideshowName) {
  return Slideshow.model
    .aggregate([
      {
        $match: {
          slug: slideshowName,
          state: 'published'
        }
      },
      {
        $project: {
          slideshow: [
            {
              $cond: [
                '$slide1.image.url',
                {
                  image: '$slide1.image.url',
                  text: {
                    $cond: [
                      '$slide1.textEnable',
                      {
                        text: '$slide1.text',
                        color: '$slide1.textColor',
                        align: '$slide1.textAlign',
                        url: {
                          $cond: ['$slide1.url', '$slide1.url', null]
                        }
                      },
                      null
                    ]
                  }
                },
                null
              ]
            },
            {
              $cond: [
                '$slide2.image.url',
                {
                  image: '$slide2.image.url',
                  text: {
                    $cond: [
                      '$slide2.textEnable',
                      {
                        text: '$slide2.text',
                        color: '$slide2.textColor',
                        align: '$slide2.textAlign',
                        url: {
                          $cond: ['$slide2.url', '$slide2.url', null]
                        }
                      },
                      null
                    ]
                  }
                },
                null
              ]
            },
            {
              $cond: [
                '$slide3.image.url',
                {
                  image: '$slide3.image.url',
                  text: {
                    $cond: [
                      '$slide3.textEnable',
                      {
                        text: '$slide3.text',
                        color: '$slide3.textColor',
                        align: '$slide3.textAlign',
                        url: {
                          $cond: ['$slide3.url', '$slide3.url', null]
                        }
                      },
                      null
                    ]
                  }
                },
                null
              ]
            },
            {
              $cond: [
                '$slide4.image.url',
                {
                  image: '$slide4.image.url',
                  text: {
                    $cond: [
                      '$slide4.textEnable',
                      {
                        text: '$slide4.text',
                        color: '$slide4.textColor',
                        align: '$slide4.textAlign',
                        url: {
                          $cond: ['$slide4.url', '$slide4.url', null]
                        }
                      },
                      null
                    ]
                  }
                },
                null
              ]
            },
            {
              $cond: [
                '$slide5.image.url',
                {
                  image: '$slide5.image.url',
                  text: {
                    $cond: [
                      '$slide5.textEnable',
                      {
                        text: '$slide5.text',
                        color: '$slide5.textColor',
                        align: '$slide5.textAlign',
                        url: {
                          $cond: ['$slide5.url', '$slide5.url', null]
                        }
                      },
                      null
                    ]
                  }
                },
                null
              ]
            },
            {
              $cond: [
                '$slide6.image.url',
                {
                  image: '$slide6.image.url',
                  text: {
                    $cond: [
                      '$slide6.textEnable',
                      {
                        text: '$slide6.text',
                        color: '$slide6.textColor',
                        align: '$slide6.textAlign',
                        url: {
                          $cond: ['$slide6.url', '$slide6.url', null]
                        }
                      },
                      null
                    ]
                  }
                },
                null
              ]
            },
            {
              $cond: [
                '$slide7.image.url',
                {
                  image: '$slide7.image.url',
                  text: {
                    $cond: [
                      '$slide7.textEnable',
                      {
                        text: '$slide7.text',
                        color: '$slide7.textColor',
                        align: '$slide7.textAlign',
                        url: {
                          $cond: ['$slide7.url', '$slide7.url', null]
                        }
                      },
                      null
                    ]
                  }
                },
                null
              ]
            }
          ]
        }
      }
    ])
    .then(result => result)
    .then(data => data[0].slideshow.filter(item => item));
}

exports = module.exports = getSlideshowData;
