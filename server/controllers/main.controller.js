// import User from '../../models/User';

const keystone = require('keystone');

const Gallery = keystone.list('Gallery');

const getSlideshowData = require('./getSlideshowData');

export function getMainData(req, res) {
  getSlideshowData('glavnaya')
    .then(slideshowData => {
      return {
        slideshow: slideshowData
      };
    })
    .then(allData => {
      return Gallery.model
        .find()
        .sort({date: -1})
        .exec()
        .then(galleries => {
          let photosArr = [];
          let videoArr = [];

          galleries.forEach(item => {
            item.photos.forEach(photo => {
              if (photosArr.length < 50) photosArr.push(photo);
            });
            item.video.forEach(video => {
              if (videoArr.length < 50) videoArr.push(video);
            });
          });

          allData.gallery = {
            photos: photosArr,
            video: videoArr
          };

          return allData;
        });
    })
    .then(allData => {
      res.send(allData);
    })
    .catch(err => {
      console.log(err);
    });
}
