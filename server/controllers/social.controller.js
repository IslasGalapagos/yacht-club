const keystone = require('keystone');

const Social = keystone.list('Social');

export function getSocial(req, res) {
  Social.model
    .find()
    .exec()
    .then(result => {
      return result;
    })
    .then(data => {
      res.send(data);
    });
}
