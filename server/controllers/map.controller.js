const keystone = require('keystone');

const AddressClub = keystone.list('AddressClub');

export function getAddress(req, res) {
  AddressClub.model
    .findOne()
    .exec()
    .then(result => {
      return result;
    })
    .then(data => {
      res.send(data.location.geo);
    });
}
