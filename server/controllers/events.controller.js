const keystone = require('keystone');
const moment = require('moment');

const Event = keystone.list('Event');
const Calendar = keystone.list('Calendar');
const News = keystone.list('News');

const getSlideshowData = require('./getSlideshowData');

export function getUIData(req, res) {
  Calendar.model
    .find({}, 'title filters')
    .populate(
      'filters.types \
            filters.categories \
            filters.statuses \
            filters.classes \
            filters.teams'
    )
    .exec()
    .then(result => result)
    .then(UIData => {
      if (!UIData.length) res.status(500).send('Calendar model is empty');

      Event.model
        .distinct('date.start.year', {state: 'published'})
        .then(years => {
          UIData[0].filters.years = years.sort((a, b) => a > b);
          res.send(UIData[0]);
        })
        .catch(e => {
          console.log(e);
        });
    })
    .catch(e => {
      console.log(e);
    });
}

export function getMonthEvents(req, res) {
  if (!req.body.month) res.status(500).send({error: 'Param "month" not found'});
  if (!req.body.year) res.status(500).send({error: 'Param "year" not found'});

  Event.model
    .find()
    .where('state', 'published')
    .where('date.start.year', req.body.year)
    .where('date.start.month', req.body.month)
    .populate(
      ' eventLocation \
            types \
            categories \
            statuses \
            classes \
            teams'
    )
    .exec()
    .then(result => {
      return result;
    })
    .then(data => {
      res.send(data);
    })
    .catch(e => {
      console.log(e);
    });
}

export function getEvent(req, res) {
  Event.model
    .aggregate([
      {
        $match: {
          state: 'published',
          slug: req.body.eventSlug
        }
      },
      {
        $lookup: {
          from: 'slideshows',
          localField: 'slideshow',
          foreignField: '_id',
          as: 'slideshow'
        }
      },
      {
        $lookup: {
          from: 'eventseries',
          localField: 'series.name',
          foreignField: '_id',
          as: 'series.name'
        }
      },
      {
        $unwind: {
          path: '$gallery',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'galleries',
          localField: 'gallery',
          foreignField: '_id',
          as: 'gallery'
        }
      },
      {
        $unwind: {
          path: '$gallery',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $group: {
          _id: '$_id',
          title: {$first: '$title'},
          slug: {$first: '$slug'},
          content: {$first: '$content'},
          slideshow: {$first: '$slideshow'},
          gallery: {$addToSet: '$gallery'},
          cover: {$first: '$cover'},
          logo: {$first: '$logo'},
          siteLink: {$first: '$siteLink'},
          additional: {$first: '$additional'},
          results: {$first: '$results'},
          resultsLink: {$first: '$resultsLink'},
          series: {$first: '$series'},
          date: {$first: '$date'},
          siblings: {$first: '$siblings'},
          accreditation: {
            $first: {
              $cond: [
                '$accreditation',
                {
                  $gt: [
                    {
                      $strcasecmp: [
                        '$date.end.string',
                        moment().format('YYYY-MM-DD')
                      ]
                    },
                    0
                  ]
                },
                false
              ]
            }
          }
        }
      }
    ])
    .then(data => {
      const allData = data[0];

      if (!allData.series.name.length) return allData;

      return Event.model
        .aggregate([
          {
            $match: {
              state: 'published',
              'series.name': allData.series.name[0]._id
            }
          },
          {
            $project: {
              slug: 1,
              phase: '$series.phase',
              year: '$date.start.year',
              date: '$date.start.string'
            }
          },
          {$sort: {date: 1}}
        ])
        .then(events => {
          const otherEvents = events.filter(item => item.phase);

          if (!otherEvents.length) return allData;

          allData.series.otherEvents = otherEvents;
          return allData;
        });
    })
    .then(allData => {
      let photosArr = [];
      let videoArr = [];

      allData.gallery.forEach(item => {
        photosArr = photosArr.concat(item.photos);
        videoArr = videoArr.concat(item.video);
      });

      allData.gallery = {
        photos: photosArr,
        video: videoArr
      };

      return allData;
    })
    .then(allData => {
      const slideshow = allData.slideshow;

      if (!slideshow.length) return allData;

      return getSlideshowData(slideshow[0].slug).then(slideshowData => {
        allData.slideshow = slideshowData;
        return allData;
      });
    })
    .then(allData => {
      return Event.model
        .find({}, 'slug')
        .where('state', 'published')
        .sort({'date.start.string': 1})
        .exec()
        .then(allEvents => {
          let idxOfActive = null;
          allEvents.some((item, index) => {
            if ('' + item.slug === '' + allData.slug) {
              idxOfActive = index;
              return true;
            }
          });

          const prevEvent =
            idxOfActive !== 0 ? allEvents[idxOfActive - 1].slug : '';
          const nextEvent =
            idxOfActive < allEvents.length - 1
              ? allEvents[idxOfActive + 1].slug
              : '';

          allData.siblings.push(prevEvent, nextEvent);

          return allData;
        });
    })
    .then(allData => {
      return News.model
        .find()
        .where('state', 'published')
        .where('filters.event', allData._id)
        .exec()
        .then(newsData => {
          allData.news = newsData;
          return allData;
        });
    })
    .then(allData => {
      if (!allData.series.name.length) return allData;

      return News.model
        .find()
        .where('state', 'published')
        .where('filters.series', allData.series.name[0]._id)
        .exec()
        .then(newsData => {
          allData.news = allData.news.concat(newsData);
          return allData;
        });
    })
    .then(allData => {
      res.send(allData);
    })
    .catch(err => {
      console.log(err);
      res.sendStatus(404);
    });
}

export function getEventsOnSeries(req, res) {
  Event.model
    .find({}, 'date.start.year series.phase')
    // .where('_id').ne(req.body.eventID)
    .where('state', 'published')
    .where('series.name', req.body.seriesID)
    .exec()
    .then(result => result)
    .then(data => {
      res.send(data);
    })
    .catch(e => {
      res.sendStatus(404);
    });
}
