const keystone = require('keystone');

const News = keystone.list('News');
const Gallery = keystone.list('Gallery');

export function getNews(req, res) {
  News.model
    .find()
    .where('state', 'published')
    .populate('filters.classes')
    .populate('album', 'photos.url')
    .populate('filters.event', 'slug')
    .populate('filters.series', 'slug')
    .populate('filters.teams', 'slug name')
    .exec()
    .then(newsData => {
      News.model
        .aggregate([
          {$match: {state: 'published'}},
          {$unwind: '$date.year'},
          {
            $unwind: {
              path: '$filters.classes',
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: 'tagclasses',
              localField: 'filters.classes',
              foreignField: '_id',
              as: 'classes'
            }
          },
          {
            $unwind: {
              path: '$classes',
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $unwind: {
              path: '$filters.teams',
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $lookup: {
              from: 'teams',
              localField: 'filters.teams',
              foreignField: '_id',
              as: 'teams'
            }
          },
          {
            $unwind: {
              path: '$teams',
              preserveNullAndEmptyArrays: true
            }
          },
          {
            $project: {
              'date.year': 1,
              'date.month': 1,
              'classes.slug': 1,
              'classes.name': 1,
              'teams.slug': 1,
              'teams.name': 1
            }
          },
          {$sort: {'date.string': -1}},
          {
            $group: {
              _id: '$date.year',
              months: {$addToSet: '$date.month'},
              classes: {$addToSet: '$classes'},
              teams: {$addToSet: '$teams'}
            }
          }
        ])
        .then(filtersData => {
          res.send({
            news: newsData,
            filters: filtersData
          });
        })
        .catch(e => {
          console.log(e);
        });
    })
    .catch(e => {
      console.log(e);
    });
}

export function getNewsArticle(req, res) {
  News.model
    .findOne({slug: req.body.name})
    .populate('author', 'about')
    .exec()
    .then(result => result)
    .then(data => {
      res.send(data);
    })
    .catch(e => {
      console.log(e);
    });
}

export function getGallery(req, res) {
  Gallery.model
    .find()
    .where('state', 'published')
    .populate('dateEvent', 'date.start.string')
    .exec()
    .then(result => result)
    .then(data => {
      res.send(data);
    })
    .catch(e => {
      console.log(e);
    });
}
