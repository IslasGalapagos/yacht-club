const keystone = require('keystone');
const moment = require('moment');

const SubSection = keystone.list('SubSection');
const Event = keystone.list('Event');
const CustomPage = keystone.list('CustomPage');

const checkSubnav = {
  accreditation: () =>
    Event.model
      .find()
      .where('state', 'published')
      .where('accreditation', true)
      .where('date.end.string', {$gt: moment().format('YYYY-MM-DD')})
      .exec()
      .then(events => {
        return !events.length;
      })
};

export function getNavData(req, res) {
  SubSection.model
    .aggregate([
      {$match: {active: true}},
      {$sort: {sortOrder: 1}},
      {$unwind: '$section'},
      {
        $group: {
          _id: '$section',
          subnav: {$push: '$link'},
          names: {$push: '$name'},
          covers: {
            $push: {
              $cond: ['$cover.url', '$cover.url', null]
            }
          },
          check: {
            $push: {
              $cond: ['$check', true, false]
            }
          }
        }
      }
    ])
    .then(subnavData => {
      const promises = [];

      subnavData.forEach((item, itemIndex) => {
        item.check.forEach((checkItem, checkIndex) => {
          if (!checkItem) return;

          const subnavName = item.subnav[checkIndex].replace('/', '');
          if (!checkSubnav[subnavName]) return;

          promises.push(
            checkSubnav[subnavName]().then(isDisabled => {
              if (isDisabled) item.disabled = checkIndex;
            })
          );
        });

        delete item.check;
      });

      if (!promises.length) return subnavData;

      return Promise.all(promises).then(() => {
        return subnavData;
      });
    })
    .then(subnavData => {
      const newSubnavData = {};

      subnavData.forEach(item => {
        const snObj = (newSubnavData[item._id] = {
          urls: [],
          data: []
        });

        item.subnav.forEach((snItem, snIndx) => {
          snObj.urls.push(snItem);
          snObj.data.push({
            name: item.names[snIndx],
            cover: item.covers[snIndx]
          });
          snObj.disabled = item.disabled || null;
        });
      });

      return newSubnavData;
    })
    .then(subnavData => {
      return CustomPage.model
        .aggregate([
          {$match: {state: 'published'}},
          {
            $project: {
              _id: 0,
              name: 1,
              url: {
                $concat: ['/pages/', '$url']
              },
              section: 1,
              menu: 1,
              cover: 1
            }
          }
        ])
        .then(customPagesData => {
          const cpNavData = {};
          const cpSubnavData = {};

          customPagesData.forEach(item => {
            if (item.menu === 'nav') {
              if (!cpNavData[item.section]) {
                cpNavData[item.section] = {
                  urls: [],
                  data: []
                };
              }

              const navObj = cpNavData[item.section];

              navObj.urls.push(item.url);
              navObj.data.push({
                align: null,
                name: item.name
              });
            }

            if (item.menu === 'subnav') {
              if (!cpSubnavData[item.section])
                cpSubnavData[item.section] = {
                  urls: [],
                  data: []
                };

              const subnavObj = cpSubnavData[item.section];

              subnavObj.urls.push(item.url);
              subnavObj.data.push({
                name: item.name,
                cover: item.cover ? item.cover.url : null,
                disable: null
              });
            }
          });

          return {
            subnav: subnavData,
            customPages: {
              nav: cpNavData,
              subnav: cpSubnavData
            }
          };
        });
    })
    .then(allData => {
      res.send(allData);
    })
    .catch(err => {
      console.log(err);
    });
}
