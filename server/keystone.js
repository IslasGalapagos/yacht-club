require('dotenv').config();

const keystone = require('keystone');

keystone.init({
  name: 'Яхт-клуб',
  brand: 'Яхт-клуб',
  'auto update': true,
  favicon: '../favicon.ico',

  port: process.env.PORT || 3000,
  mongo: process.env.MONGO_URI || process.env.MONGOLAB_URI,
  session: true,
  auth: true,
  'user model': 'User',
  'cookie secret': process.env.COOKIE_SECRET || 'demo',
  'cloudinary config': process.env.CLOUDINARY_URI || ''
});

keystone.set('default region', 'ru');

keystone.import('models/');

keystone.set('routes', require('./build/server').default);

keystone.set('nav', {
  Главная: ['main-slideshows'],
  Клуб: [
    'club-sub-sections',
    'club-slideshows',
    'club-leaderships',
    'club-members',
    'club-symbols',
    'club-partners'
  ],
  Календарь: [
    'calendars',
    'events',
    'event-slideshows',
    'event-locations',
    'event-series'
  ],
  Проекты: [
    'projects-sub-sections',
    'teams',
    'team-slideshows',
    'team-people',
    'regatta'
  ],
  Медиа: ['media-sub-sections', 'news', 'galleries'],
  'Типовая страница': ['custom-pages', 'custom-page-people'],
  Теги: ['tag-types', 'tag-categories', 'tag-statuses', 'tag-classes'],
  Остальное: ['users', 'address-clubs', 'socials', 'accreditations']
});

keystone.start();
