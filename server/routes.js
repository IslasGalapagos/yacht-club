import Express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

import React from 'react';
import Helmet from 'react-helmet';
import ReactDOMServer from 'react-dom/server';
import Loadable from 'react-loadable';
import {getBundles} from 'react-loadable/webpack';
import stats from '../public/react-loadable.json';

import Root from '../client/components/Root';
import {configureStore} from '../client/store';
import routes from '../client/routes';
import callActions from './util/callActions';

import navAPI from './api/nav.routes';
import mapAPI from './api/map.routes';
import mainAPI from './api/main.routes';
import socialAPI from './api/social.routes';
import clubAPI from './api/club.routes';
import eventsAPI from './api/events.routes';
import projectsAPI from './api/projects.routes';
import mediaAPI from './api/media.routes';
import accreditationAPI from './api/accreditation.routes';

export default app => {
  app.use(Express.static(path.resolve(__dirname, '../../public')));
  app.use(bodyParser.json());
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  );

  app.use('/api', navAPI);
  app.use('/api', mapAPI);
  app.use('/api', mainAPI);
  app.use('/api', clubAPI);
  app.use('/api', eventsAPI);
  app.use('/api', projectsAPI);
  app.use('/api', mediaAPI);
  app.use('/api', socialAPI);
  app.use('/api', accreditationAPI);

  app.use((req, res, next) => {
    Loadable.preloadAll().then(() => {
      const context = {};
      const store = configureStore({entryPage: req.url});
      const modules = [];

      callActions(store, routes, req.url).then(() => {
        const initialView = ReactDOMServer.renderToString(
          <Loadable.Capture report={moduleName => modules.push(moduleName)}>
            <Root
              store={store}
              staticRouterData={{location: req.url, context}}
            />
          </Loadable.Capture>
        );

        const additionalBundles = getBundles(stats, modules).reduce(
          (acc, bundle) => {
            const {file} = bundle;
            if (!/^(styles.css|bundle.js)/.test(file)) {
              acc[/\.(css|js)$/.exec(file)[1]].push(file);
            }
            return acc;
          },
          {css: [], js: []}
        );

        if (context.url) {
          res.writeHead(301, {
            Location: context.url
          });
          res.end();
        } else {
          res
            .set('Content-Type', 'text/html')
            .status(200)
            .end(
              renderFullPage(initialView, store.getState(), additionalBundles)
            );
        }
      });
    });
  });
};

function renderFullPage(html, initialState, additionalBundles) {
  const head = Helmet.renderStatic();

  return `
        <!doctype html>
        <html>
            <head>
                ${head.title.toString()}
                ${head.meta.toString()}
                ${head.script.toString()}
                ${additionalBundles['css'].map(
                  file => `<link rel='stylesheet' href='/${file}' />`
                )}
                <link rel='stylesheet' href='/styles.css' />
            </head>
            <body>
                <section role="main" class="react-container">
                    <div id="root">${html}</div>
                    <script>
                        window.__INITIAL_STATE__ = ${JSON.stringify(
                          initialState
                        )};
                    </script>
                </section>
                ${additionalBundles['js'].map(
                  file => `<script src='/${file}'></script>`
                )}
                <script src='/bundle.js'></script>
            </body>
        </html>
    `;
}

function renderError(err) {
  const softTab = '&#32;&#32;&#32;&#32;';
  const errTrace =
    process.env.NODE_ENV !== 'production'
      ? `:<br><br><pre style="color:red">${softTab}${err.stack.replace(
          /\n/g,
          `<br>${softTab}`
        )}</pre>`
      : '';

  return renderFullPage(`Server Error${errTrace}`, {});
}
