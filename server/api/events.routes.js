import { Router } from 'express';
import * as EventsController from '../controllers/events.controller';

const router = new Router();

router.route('/events/ui').post(EventsController.getUIData);
router.route('/events').post(EventsController.getMonthEvents);
router.route('/event').post(EventsController.getEvent);
router.route('/events-on-series').post(EventsController.getEventsOnSeries);

export default router;
