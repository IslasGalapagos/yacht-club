import { Router } from 'express';
import * as SocialController from '../controllers/social.controller';

const router = new Router();

router.route('/social').post(SocialController.getSocial);

export default router;
