import { Router } from 'express';
import * as ClubController from '../controllers/club.controller';

const router = new Router();

router.route('/club').post(ClubController.getClubData);

export default router;
