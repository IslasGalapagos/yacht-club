import { Router } from 'express';
import * as MediaController from '../controllers/media.controller';

const router = new Router();

router.route('/news').post(MediaController.getNews);
router.route('/news-article').post(MediaController.getNewsArticle);
router.route('/gallery').post(MediaController.getGallery);

export default router;
