import { Router } from 'express';
import * as MapController from '../controllers/map.controller';

const router = new Router();

router.route('/club-address').post(MapController.getAddress);

export default router;
