import { Router } from 'express';
import * as NavController from '../controllers/nav.controller';

const router = new Router();

router.route('/nav').post(NavController.getNavData);

export default router;
