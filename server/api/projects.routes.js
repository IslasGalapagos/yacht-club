import { Router } from 'express';
import * as ProjectsController from '../controllers/projects.controller';

const router = new Router();

router.route('/teams').post(ProjectsController.getAllTeams);
router.route('/team').post(ProjectsController.getTeam);
router.route('/regatta').post(ProjectsController.getRegatta);
router.route('/custom-page').post(ProjectsController.getCustomPage);

export default router;
