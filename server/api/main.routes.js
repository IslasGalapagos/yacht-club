import { Router } from 'express';
import * as MainController from '../controllers/main.controller';

const router = new Router();

router.route('/main').post(MainController.getMainData);

export default router;
