import { Router } from 'express';
import * as Accreditation from '../controllers/accreditation.controller';

const router = new Router();

router.route('/accreditation').post(Accreditation.addRequest);
router.route('/accreditation-events').post(Accreditation.getEventsWithAccreditation);

export default router;
