const path = require('path');
const webpack = require('webpack');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const {ReactLoadablePlugin} = require('react-loadable/webpack');

const {NODE_ENV = 'development'} = process.env;
const isProduction = NODE_ENV === 'production';

const plugins = [
  new CleanWebpackPlugin(['public']),
  new MiniCssExtractPlugin({
    filename: 'styles.css'
  }),
  new webpack.ContextReplacementPlugin(
    /node_modules\/moment\/locale/,
    /ru|en-gb/
  ),
  new webpack.ProvidePlugin({
    PropTypes: 'prop-types'
  }),
  new webpack.DefinePlugin({
    APP_ENV: JSON.stringify('Browser')
  }),
  new ReactLoadablePlugin({
    filename: './public/react-loadable.json'
  })
];

if (process.env.DEV_ALL) {
  plugins.push(
    new WebpackShellPlugin({
      onBuildEnd: ['npm run webpack:server']
    })
  );
}

module.exports = {
  target: 'web',

  mode: NODE_ENV,

  devtool: isProduction ? 'cheap-source-map' : 'eval',

  context: path.resolve(__dirname, './client'),

  entry: {
    app: ['./', './styles']
  },

  resolve: {
    extensions: ['.js', '.json', '.less']
  },

  output: {
    path: path.join(__dirname, '/public/'),
    publicPath: '/',
    filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: path.join(__dirname, '/client'),
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: ['@babel/plugin-syntax-dynamic-import']
        }
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          'css-loader',
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true
            }
          }
        ]
      },
      {
        test: /\.woff\d?(\?.+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?.+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?.+)?$/,
        loader: 'url-loader?limit=10000'
      },
      {
        test: /\.svg(\?.+)?$/,
        loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
      },
      {
        test: /\.(jpg|jpeg|gif|png)$/,
        exclude: /node_modules/,
        loader: 'url-loader?limit=1024&name=images/[name].[ext]'
      },
      {
        test: /\.gif$/,
        loader:
          'url-loader?limit=10000&mimetype=image/gif&name=images/[name].[ext]'
      }
    ]
  },

  optimization: {
    minimizer: [new UglifyJsPlugin(), new OptimizeCssAssetsPlugin()]
  },

  plugins,

  watch: Boolean(process.env.DEV_ALL)
};
