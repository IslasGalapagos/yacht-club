require('dotenv').config();

const fs = require('fs');
const path = require('path');
const webpack = require('webpack');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const {NODE_ENV = 'development'} = process.env;
const isProduction = NODE_ENV === 'production';

const nodeModules = {};
fs.readdirSync('node_modules')
  .filter(x => ['.bin'].indexOf(x) === -1)
  .forEach(mod => (nodeModules[mod] = `commonjs ${mod}`));

const SERVER_DIR = path.resolve(__dirname, './server');

const plugins = [
  new CleanWebpackPlugin(['server.build']),
  new webpack.ContextReplacementPlugin(
    /node_modules\/moment\/locale/,
    /ru|en-gb/
  ),
  new webpack.ProvidePlugin({
    React: 'react',
    PropTypes: 'prop-types'
  }),
  new webpack.DefinePlugin({
    APP_ENV: JSON.stringify('Node'),
    DOMAIN: JSON.stringify(process.env.DOMAIN)
  })
];

if (process.env.DEV_ALL) {
  plugins.push(
    new WebpackShellPlugin({
      onBuildEnd: ['npm run server:dev']
    })
  );
}

module.exports = {
  target: 'node',
  node: {
    __dirname: false
  },

  mode: NODE_ENV,

  devtool: isProduction ? 'cheap-source-map' : 'eval',

  context: path.resolve(__dirname, './server'),

  entry: {
    server: ['./routes']
  },

  resolve: {
    extensions: ['.js', '.json', '.less']
  },

  output: {
    path: `${SERVER_DIR}/build`,
    filename: 'server.js',
    libraryTarget: 'commonjs2'
  },

  externals: nodeModules,

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: [
            '@babel/plugin-syntax-dynamic-import',
            'react-loadable/babel'
          ]
        }
      },
      {
        test: /\.(less|svg)$/,
        loader: 'null-loader'
      }
    ]
  },

  plugins,

  optimization: {
    minimizer: [new UglifyJsPlugin()]
  },

  watch: Boolean(process.env.DEV_ALL)
};
